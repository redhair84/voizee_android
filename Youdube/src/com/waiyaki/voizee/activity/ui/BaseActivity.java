/**
 * 
 */
package com.waiyaki.voizee.activity.ui;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.waiyaki.voizee.dao.model.VoizeeVideo;
import com.waiyaki.voizee.main.R;
import com.waiyaki.voizee.main.VoizeeApplication;
import com.waiyaki.voizee.util.AudioRecorder;
import com.waiyaki.voizee.util.PlayerVideoView;
import com.waiyaki.voizee.util.VoizeeConstants;
import com.waiyaki.voizee.util.YoutubeUtils;

/**
 * @author Francisco Javier Morant Moya
 * 
 */
public class BaseActivity extends Activity {

	private AudioManager audioManager;
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
	private AudioRecorder audioRecorder;
	private PlayerVideoView videoView;
	private MediaPlayer mediaPlayer;
	private boolean hasFinished = false;
	protected List<VoizeeVideo> listVideos = null;
	protected ImageButton ytBtn;

	protected SharedPreferences mPrefs;
	private VoizeeApplication shareApp;

	// Loading dialog
	protected static final int DIALOG_LOADING_ID = 0;
	protected ProgressDialog dialogLoading;

	protected void onCreate(Bundle savedInstanceState, int viewId) {

		int SDK_INT = android.os.Build.VERSION.SDK_INT;

		if (SDK_INT > 8) {

            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitAll().build());

            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectLeakedSqlLiteObjects()
                    .penaltyLog()
                    .penaltyDeath()
                    .build());

		}

		// Call to the super
		super.onCreate(savedInstanceState);

		// Set the content view id
		setContentView(viewId);

		// Get the audioManager
		audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

		// get the global app sharing
		setShareApp((VoizeeApplication) this.getApplication());

	}

	@Override
	public void onStart() {
		super.onStart();
		if (!VoizeeConstants.DEVELOPER_MODE)
			EasyTracker.getInstance().activityStart(this); // Add this method.
	}

	@Override
	public void onStop() {
		super.onStop();

		if (!VoizeeConstants.DEVELOPER_MODE)
			EasyTracker.getInstance().activityStop(this); // Add this method.
	}

	public AudioManager getAudioManager() {
		return audioManager;
	}

	public void setAudioManager(AudioManager audioManager) {
		this.audioManager = audioManager;
	}

	public SimpleDateFormat getSdf() {
		return sdf;
	}

	public void setSdf(SimpleDateFormat sdf) {
		this.sdf = sdf;
	}

	public AudioRecorder getAudioRecorder() {
		return audioRecorder;
	}

	public void setAudioRecorder(AudioRecorder audioRecorder) {
		this.audioRecorder = audioRecorder;
	}

	public PlayerVideoView getVideoView() {
		return videoView;
	}

	public void setVideoView(PlayerVideoView videoView) {
		this.videoView = videoView;
	}

	public MediaPlayer getMediaPlayer() {
		return mediaPlayer;
	}

	public void setMediaPlayer(MediaPlayer mediaPlayer) {
		this.mediaPlayer = mediaPlayer;
	}

	public void mute() {

		audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
	}

	public void unmute() {
		// audioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
		int maxVolume = audioManager
				.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, maxVolume / 2,
				0);
	}

	public void unmuteMin() {
		// audioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
		int maxVolume = audioManager
				.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
				(int) (maxVolume * 0.2), 0);
	}

	public VoizeeApplication getShareApp() {
		return shareApp;
	}

	public void setShareApp(VoizeeApplication shareApp) {
		this.shareApp = shareApp;
	}

	protected void showErrorMessage(Exception e, String msg, boolean finish,
			boolean isToasted) {

		// IF any dialog is showing lets remove it
		hasFinished = true;

		if (e != null)
			// Print logs
			e.printStackTrace();

        if(e!=null && VoizeeConstants.DEVELOPER_MODE){
            Log.d("Voizee","Error found it: "+e.getMessage());
        }

		if (isToasted)
			// Show a toast saying thre is an error
			Toast.makeText(this, msg, Toast.LENGTH_LONG).show();

		if (dialogLoading != null && dialogLoading.isShowing())
			dismissDialog();

		// Finish the activity
		if (finish)
			finish();
	}

	protected void showDialog(String message,boolean isCancelable) {

		if (dialogLoading == null || !dialogLoading.isShowing()) {
			// Show searching
			dialogLoading = new ProgressDialog(this);
			dialogLoading.setTitle("Voizee");
			dialogLoading.setMessage(message);
			dialogLoading.setIndeterminate(true);
			dialogLoading.setCancelable(isCancelable);
			dialogLoading.show();

			// It hasn't finished
			hasFinished = false;

			new Thread() {

				public void run() {

					while (!hasFinished) {
						try {
							sleep(1000);
						} catch (InterruptedException e1) {
							e1.printStackTrace();
						}
					}

					// Send an empty message to the handler to remove the dialog
					handlerDialog.sendEmptyMessage(0);

				}

			}.start();
		}
	}

	private Handler handlerDialog = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			// dismiss the progress dialog
			dismissDialog();

		}
	};

	protected void dismissDialog() {
		if (dialogLoading != null && dialogLoading.isShowing()) {


            try {
                dialogLoading.dismiss();
            } catch (Exception e) {
                showErrorMessage(e,"Error dismissing a dialog.",false,false);
            }
		}
	}

	public ProgressDialog getDialogLoading() {
		return dialogLoading;
	}

	public void setDialogLoading(ProgressDialog dialogLoading) {
		this.dialogLoading = dialogLoading;
	}

	protected boolean isSubsetOf(Collection<String> subset,
			Collection<String> superset) {
		for (String string : subset) {
			if (!superset.contains(string)) {
				return false;
			}
		}
		return true;
	}

	protected void addNewViewToVideo(VoizeeVideo videoSelected) {

		if (videoSelected != null && videoSelected.getIdRecording() != null) {
			// Call to add a new view
			try {
				getShareApp().getManagerVideoPlayer().addNewViewToVideo(
						videoSelected.getIdRecording());
			} catch (Exception e) {

				e.printStackTrace();

			}
		}
	}

    protected void initVideo(VoizeeVideo videoSelected) {
        setVideoView((PlayerVideoView) findViewById(R.id.videoViewPlayer));

        if (videoSelected != null && videoSelected.getUrlVideo() != null)
            getVideoView().setVideoURI(Uri.parse(videoSelected.getUrlVideo()));
        else
            showErrorMessage(null, getText(R.string.error_loading_video)
                    .toString(), true, true);

    }

	public boolean isHasFinished() {
		return hasFinished;
	}

	public void setHasFinished(boolean hasFinished) {
		this.hasFinished = hasFinished;
	}

	public Handler getHandlerDialog() {
		return handlerDialog;
	}

	public void setHandlerDialog(Handler handlerDialog) {
		this.handlerDialog = handlerDialog;
	}

	static class ViewHolder {
		TextView text;
		TextView textViews;
		TextView textAuthor;
		TextView timestamp;
        ImageButton shareBtn;
		ImageView icon;
		ProgressBar progress;
		int position;
		String urlThumbnail;
	}

	public class MyCustomAdapter extends ArrayAdapter<String> {

		public MyCustomAdapter(Context context, int textViewResourceId,
				String[] objects) {
			super(context, textViewResourceId, objects);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			ViewHolder holder;
			final VoizeeVideo item = listVideos.get(position);

			// if (convertView == null
			// || (convertView != null && convertView.getTag() == null)) {
			convertView = View.inflate(BaseActivity.this, R.layout.row, null);
			holder = new ViewHolder();
			holder.icon = (ImageView) convertView
					.findViewById(R.id.thumbNailYoutube);

			holder.progress = (ProgressBar) convertView
					.findViewById(R.id.prgBarLoading);

			holder.text = (TextView) convertView
					.findViewById(R.id.textViewTitle);

			holder.text.setText(item.getTitulo());

			holder.urlThumbnail = item.getUrlThumbnail();

			holder.text = (TextView) convertView
					.findViewById(R.id.textViewTitle);

			holder.text.setText(item.getTitulo());

			holder.textAuthor = (TextView) convertView
					.findViewById(R.id.authorVideo);

			holder.textAuthor.setText("@" + item.getAutor());

			holder.textViews = (TextView) convertView
					.findViewById(R.id.playsVideo);

			holder.textViews.setText(item.getViews() + " "
					+ BaseActivity.this.getText(R.string.label_num_views));


            holder.shareBtn = (ImageButton) convertView
                    .findViewById(R.id.shareBtn);



            if (holder.shareBtn != null) {
                holder.shareBtn.setFocusable(false);

                holder.shareBtn.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {


                        try {
                            Intent intent = new Intent(Intent.ACTION_SEND);
                            intent.putExtra(
                                    Intent.EXTRA_TEXT,
                                    getText(R.string.upload_share_video)
                                            + " " + item.getUrlLink());
                            intent.setType("text/plain");

                            startActivity(Intent.createChooser(intent,
                                    getText(R.string.upload_share_action_video)));

                        } catch (final ActivityNotFoundException e) {
                            Log.i("Voizee", "no twitter native", e);
                        }

                    }
                });
            }



			convertView.setTag(holder);

			// Using an AsyncTask to load the slow images in a background thread
			new AsyncTask<ViewHolder, Void, Bitmap>() {
				private ViewHolder v;

				@Override
				protected Bitmap doInBackground(ViewHolder... params) {
					v = params[0];

					return YoutubeUtils.getBitmapFromURL(v.urlThumbnail);
				}

				@Override
				protected void onPostExecute(Bitmap result) {
					super.onPostExecute(result);
					// if (v.position == position) {
					// If this item hasn't been recycled already, hide the
					// progress and set and show the image
					v.progress.setVisibility(View.GONE);
					v.icon.setVisibility(View.VISIBLE);
					if (result != null)
						v.icon.setImageBitmap(result);
					// else
					// v.icon.setBackground(ListBaseFragment.this
					// .getActivity().getResources()
					// .getDrawable(R.drawable.thumbnail));
					// }
				}
			}.execute(holder);

			return convertView;
		}
	}

	public class NoAuthorAdapter extends ArrayAdapter<String> {

		public NoAuthorAdapter(Context context, int textViewResourceId,
				String[] objects) {
			super(context, textViewResourceId, objects);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			ViewHolder holder;
			VoizeeVideo item = listVideos.get(position);

			// if (convertView == null
			// || (convertView != null && convertView.getTag() == null)) {
			convertView = View.inflate(BaseActivity.this, R.layout.row, null);
			holder = new ViewHolder();
			holder.icon = (ImageView) convertView
					.findViewById(R.id.thumbNailYoutube);

			holder.progress = (ProgressBar) convertView
					.findViewById(R.id.prgBarLoading);

			holder.text = (TextView) convertView
					.findViewById(R.id.textViewTitle);

			holder.text.setText(item.getTitulo());

			holder.urlThumbnail = item.getUrlThumbnail();

			holder.text = (TextView) convertView
					.findViewById(R.id.textViewTitle);

			holder.text.setText(item.getTitulo());

			holder.textAuthor = (TextView) convertView
					.findViewById(R.id.authorVideo);

			holder.textAuthor.setVisibility(View.GONE);

			holder.textViews = (TextView) convertView
					.findViewById(R.id.playsVideo);

			holder.textViews.setVisibility(View.GONE);

			convertView.setTag(holder);

			// Using an AsyncTask to load the slow images in a background thread
			new AsyncTask<ViewHolder, Void, Bitmap>() {
				private ViewHolder v;

				@Override
				protected Bitmap doInBackground(ViewHolder... params) {
					v = params[0];

					return YoutubeUtils.getBitmapFromURL(v.urlThumbnail);
				}

				@Override
				protected void onPostExecute(Bitmap result) {
					super.onPostExecute(result);
					// if (v.position == position) {
					// If this item hasn't been recycled already, hide the
					// progress and set and show the image
					v.progress.setVisibility(View.GONE);
					v.icon.setVisibility(View.VISIBLE);
					if (result != null)
						v.icon.setImageBitmap(result);
					// else
					// v.icon.setBackground(ListBaseFragment.this
					// .getActivity().getResources()
					// .getDrawable(R.drawable.thumbnail));
					// }
				}
			}.execute(holder);

			return convertView;
		}
	}

}
