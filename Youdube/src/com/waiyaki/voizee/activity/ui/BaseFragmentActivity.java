/**
 * 
 */
package com.waiyaki.voizee.activity.ui;

import java.text.SimpleDateFormat;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.waiyaki.voizee.main.VoizeeApplication;
import com.waiyaki.voizee.util.AudioRecorder;
import com.waiyaki.voizee.util.PlayerVideoView;
import com.waiyaki.voizee.util.VoizeeConstants;

/**
 * @author Francisco Javier Morant Moya
 * 
 */
public class BaseFragmentActivity extends FragmentActivity {

	private AudioManager audioManager;
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
	private AudioRecorder audioRecorder;
	private PlayerVideoView videoView;
	private MediaPlayer mediaPlayer;
	private boolean hasFinished = true;

	protected SharedPreferences mPrefs;

	private VoizeeApplication shareApp;

	// Loading dialog
	protected static final int DIALOG_LOADING_ID = 0;
	private ProgressDialog dialogLoading;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		// Call to the super
		super.onCreate(savedInstanceState);

		int SDK_INT = android.os.Build.VERSION.SDK_INT;

		if (SDK_INT > 8) {

			StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
					.detectDiskReads().detectDiskWrites().detectNetwork()
					.penaltyLog().build());

		}

		// Get the audioManager
		audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

		// get the global app sharing
		setShareApp((VoizeeApplication) this.getApplication());

	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

	}

	@Override
	public void onStart() {
		super.onStart();
		if (!VoizeeConstants.DEVELOPER_MODE)
			EasyTracker.getInstance().activityStart(this); // Add this method.
	}

	@Override
	public void onStop() {
		super.onStop();

		if (!VoizeeConstants.DEVELOPER_MODE)
			EasyTracker.getInstance().activityStop(this); // Add this method.
	}

	protected void showDialog(String message, boolean cancelable) {

		if (dialogLoading == null || !dialogLoading.isShowing()) {
			// Show searching
			dialogLoading = new ProgressDialog(this);
			dialogLoading.setTitle("Voizee");
			dialogLoading.setMessage(message);
			dialogLoading.setIndeterminate(true);
			dialogLoading.setCancelable(cancelable);
			dialogLoading.show();

			hasFinished = false;

			new Thread() {

				public void run() {

					while (!hasFinished) {
						try {
							sleep(1000);
						} catch (InterruptedException e1) {
							e1.printStackTrace();
						}
					}

					// Send an empty message to the handler to remove the dialog
					handlerDialog.sendEmptyMessage(0);

				}

			}.start();
		}
	}

	protected void showErrorMessage(Exception e, String msg, boolean finish,
			boolean isToasted) {

		// IF any dialog is showing lets remove it
		hasFinished = true;

		if (e != null)
			// Print logs
			e.printStackTrace();

		if (isToasted)
			// Show a toast saying thre is an error
			Toast.makeText(this, msg, Toast.LENGTH_LONG).show();

		if (dialogLoading != null && dialogLoading.isShowing())
			dismissDialog();

		// Finish the activity
		if (finish)
			finish();
	}

	private Handler handlerDialog = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			// dismiss the progress dialog
			dismissDialog();

		}
	};

	public AudioManager getAudioManager() {
		return audioManager;
	}

	public void setAudioManager(AudioManager audioManager) {
		this.audioManager = audioManager;
	}

	public SimpleDateFormat getSdf() {
		return sdf;
	}

	public void setSdf(SimpleDateFormat sdf) {
		this.sdf = sdf;
	}

	public AudioRecorder getAudioRecorder() {
		return audioRecorder;
	}

	public void setAudioRecorder(AudioRecorder audioRecorder) {
		this.audioRecorder = audioRecorder;
	}

	public PlayerVideoView getVideoView() {
		return videoView;
	}

	public void setVideoView(PlayerVideoView videoView) {
		this.videoView = videoView;
	}

	public MediaPlayer getMediaPlayer() {
		return mediaPlayer;
	}

	public void setMediaPlayer(MediaPlayer mediaPlayer) {
		this.mediaPlayer = mediaPlayer;
	}

	public void mute() {

		audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
	}

	public void unmute() {
		// audioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
		int maxVolume = audioManager
				.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, maxVolume / 2,
				0);
	}

	public void unmuteMin() {
		// audioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
		int maxVolume = audioManager
				.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
				(int) (maxVolume * 0.2), 0);
	}

	public VoizeeApplication getShareApp() {
		return shareApp;
	}

	public void setShareApp(VoizeeApplication shareApp) {
		this.shareApp = shareApp;
	}

	protected void dismissDialog() {
		if (dialogLoading != null && dialogLoading.isShowing()) {
			dialogLoading.dismiss();
		}
	}

	public boolean isHasFinished() {
		return hasFinished;
	}

	public void setHasFinished(boolean hasFinished) {
		this.hasFinished = hasFinished;
	}

}
