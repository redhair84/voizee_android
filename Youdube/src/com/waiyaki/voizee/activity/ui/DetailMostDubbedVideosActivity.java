/**
 * 
 */
package com.waiyaki.voizee.activity.ui;

import java.util.LinkedList;

import org.json.JSONException;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.TextView;

import com.markupartist.android.widget.PullToRefreshListView;
import com.markupartist.android.widget.PullToRefreshListView.OnRefreshListener;
import com.waiyaki.voizee.dao.model.VoizeeVideo;
import com.waiyaki.voizee.main.R;
import com.waiyaki.voizee.manager.VoizeeApiManager;
import com.waiyaki.voizee.manager.VoizeeApiManagerImpl;
import com.waiyaki.voizee.util.VoizeeConstants;

/**
 * @author Francisco Javier Morant Moya
 * 
 */
public class DetailMostDubbedVideosActivity extends BaseActivity {

	private EditText locText = null;
	private LinkedList<String> itemsListView;
	private VoizeeApiManager managerVideoPlayer = new VoizeeApiManagerImpl();
	private PullToRefreshListView listView;
	private String youtubeId = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.detail_most_dubbed_videos);

		if (VoizeeConstants.DEVELOPER_MODE)
			Log.d("Voizee", "Create MostDubbedVideosActivity");

		// Get the youtube id pass through the bundle
		youtubeId = (String) this.getIntent().getCharSequenceExtra(
				"youtubeId_MainMostDubbed");

		((TextView) this.findViewById(R.id.headerMostDubbed))
				.setText((String) this.getIntent().getCharSequenceExtra(
						"titleVideo"));

		itemsListView = new LinkedList<String>();

		// Add listview
		setListView((PullToRefreshListView) this.findViewById(R.id.idListView));

		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapter, View view,
					int position, long arg) {
				VoizeeVideo video = listVideos.get(position - 1);

				Intent intentVideoPlayer = new Intent(
						DetailMostDubbedVideosActivity.this,
						PlayerActivity.class);

				// Bundle bundleVideo = new Bundle();
				//
				// bundleVideo.putSerializable(
				// VoizeeConstants.VIDEO_VALUE_REQUESTED,
				// (Serializable) video);
				//
				// intentVideoPlayer.putExtra("bundleVideo", bundleVideo);

				getShareApp().setSelectedVideo(video);

				DetailMostDubbedVideosActivity.this
						.startActivity(intentVideoPlayer);

			}
		});

		// Set a listener to be invoked when the list should be refreshed.
		listView.setOnRefreshListener(new OnRefreshListener() {
			@Override
			public void onRefresh() {
				// Do work to refresh the list here.
				new GetDataTask().execute();
			}
		});

		// Loading the videos from DB

		try {
			listVideos = managerVideoPlayer.getDetailMostDubbedVideos(
					youtubeId, getShareApp().getIdUser());
		} catch (JSONException e) {

			showErrorMessage(
					e,
					DetailMostDubbedVideosActivity.this.getText(
							R.string.error_getting_data).toString(), true, true);
		}

		if (listVideos != null) {
			for (VoizeeVideo video : listVideos)
				itemsListView.add(video.getTitulo());

			String[] array = new String[itemsListView.size()];
			itemsListView.toArray(array);
			setListItemsAdapter(array);
		}

	}

	private class GetDataTask extends AsyncTask<Void, Void, String[]> {

		@Override
		protected String[] doInBackground(Void... params) {
			// Simulates a background job.

			try {
				listVideos = managerVideoPlayer.getDetailMostDubbedVideos(
						youtubeId, getShareApp().getIdUser());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (listVideos != null) {

				for (VoizeeVideo video : listVideos)
					itemsListView.add(video.getTitulo());
			}

			String[] array = new String[itemsListView.size()];
			itemsListView.toArray(array);
			return array;
		}

		@Override
		protected void onPostExecute(String[] result) {
			itemsListView.addFirst("Added after refresh...");

			// Call onRefreshComplete when the list has been refreshed.
			((PullToRefreshListView) getListView()).onRefreshComplete();

			super.onPostExecute(result);
		}
	}

	private void setListItemsAdapter(String[] items2) {
		listView.setAdapter(new MyCustomAdapter(
				DetailMostDubbedVideosActivity.this, R.layout.row, items2));
	}

	public EditText getLocText() {
		return locText;
	}

	public void setLocText(EditText locText) {
		this.locText = locText;
	}

	public LinkedList<String> getItemsListView() {
		return itemsListView;
	}

	public void setItemsListView(LinkedList<String> itemsListView) {
		this.itemsListView = itemsListView;
	}

	public VoizeeApiManager getManagerVideoPlayer() {
		return managerVideoPlayer;
	}

	public void setManagerVideoPlayer(VoizeeApiManager managerVideoPlayer) {
		this.managerVideoPlayer = managerVideoPlayer;
	}

	public PullToRefreshListView getListView() {
		return listView;
	}

	public void setListView(PullToRefreshListView listView) {
		this.listView = listView;
	}

}
