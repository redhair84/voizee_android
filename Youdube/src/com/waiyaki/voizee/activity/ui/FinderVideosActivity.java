/**
 *
 */
package com.waiyaki.voizee.activity.ui;

import java.util.LinkedList;

import org.json.JSONException;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.markupartist.android.widget.PullToRefreshListView;
import com.markupartist.android.widget.PullToRefreshListView.OnRefreshListener;
import com.waiyaki.voizee.dao.model.VoizeeVideo;
import com.waiyaki.voizee.main.R;
import com.waiyaki.voizee.manager.VoizeeApiManager;
import com.waiyaki.voizee.manager.VoizeeApiManagerImpl;
import com.waiyaki.voizee.util.VoizeeConstants;

/**
 * @author Francisco Javier Morant Moya
 */
public class FinderVideosActivity extends ListBaseFragment {

    private EditText locText = null;
    private LinkedList<String> itemsListView;
    private VoizeeApiManager managerVideoPlayer = new VoizeeApiManagerImpl();
    private PullToRefreshListView listView;
    private Button btnSearch;
    private EditText searchValue;
    private ProgressDialog dialogLoading;
    private Handler handler;

    public FinderVideosActivity() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        title = getText(R.string.label_finder_view).toString();

        if (handler == null)
            handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {

                    if (itemsListView.size() > 0) {
                        String[] array = new String[itemsListView.size()];
                        itemsListView.toArray(array);
                        setListItemsAdapter(array);
                    } else {

                        Toast.makeText(FinderVideosActivity.this.getActivity(),
                                getText(R.string.no_results_finder),
                                Toast.LENGTH_LONG).show();
                    }

                    // dismiss the progress dialog
                    dialogLoading.dismiss();

                }
            };
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (VoizeeConstants.DEVELOPER_MODE)
            Log.d("Voizee", "Create FinderVideosActivity View");

        View view = inflater.inflate(R.layout.finder_videos, container, false);

        itemsListView = new LinkedList<String>();


        if (view != null) {

            // Add listview
            setListView((PullToRefreshListView) view.findViewById(R.id.idListView));

            listView.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapter, View view,
                                        int position, long arg) {
                    VoizeeVideo video = getListVideos().get(position - 1);

                    Intent intentVideoPlayer = new Intent(FinderVideosActivity.this
                            .getActivity(), PlayerActivity.class);

                    ((BaseFragmentActivity) FinderVideosActivity.this.getActivity())
                            .getShareApp().setSelectedVideo(video);

                    FinderVideosActivity.this.startActivity(intentVideoPlayer);

                }
            });

            // Set a listener to be invoked when the list should be refreshed.
            listView.setOnRefreshListener(new OnRefreshListener() {
                @Override
                public void onRefresh() {
                    // Do work to refresh the list here.
                    new GetDataTask().execute();
                }
            });

            // Add the list of the videos to the list adapter
            if (getListVideos() != null) {
                for (VoizeeVideo video : getListVideos())
                    itemsListView.add(video.getTitulo());

                String[] array = new String[itemsListView.size()];
                itemsListView.toArray(array);
                setListItemsAdapter(array);
            }

            // Add the button and a listener when it is pressed
            btnSearch = (Button) view.findViewById(R.id.searchBtn);
            btnSearch.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    // hide virtual keyboard
                    InputMethodManager imm = (InputMethodManager) FinderVideosActivity.this
                            .getActivity().getSystemService(
                                    Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(searchValue.getWindowToken(), 0);

                    // Show searching
                    dialogLoading = new ProgressDialog(FinderVideosActivity.this
                            .getActivity());
                    dialogLoading.setTitle("Voizee");
                    dialogLoading.setMessage(FinderVideosActivity.this
                            .getText(R.string.finder_msg_loading));
                    dialogLoading.setIndeterminate(true);
                    dialogLoading.setCancelable(true);
                    dialogLoading.show();

                    new Thread() {

                        public void run() {

                            try {
                                sleep(1000);
                            } catch (InterruptedException e1) {
                                // TODO Auto-generated catch block
                                e1.printStackTrace();
                            }

                            // Clear the list of the videos
                            itemsListView.clear();

                            if (FinderVideosActivity.this.searchValue
                                    .getText() != null) {

                                // Get the new ones
                                try {
                                    setListVideos(managerVideoPlayer
                                            .getSearchedVideos(
                                                    FinderVideosActivity.this.searchValue
                                                            .getText().toString(),
                                                    ((BaseFragmentActivity) FinderVideosActivity.this
                                                            .getActivity())
                                                            .getShareApp().getIdUser()));
                                } catch (JSONException e) {
                                    e.printStackTrace();

                                    if (dialogLoading.isShowing())
                                        dialogLoading.dismiss();
                                }

                                if (getListVideos() != null)
                                    for (VoizeeVideo video : getListVideos())
                                        itemsListView.add(video.getTitulo());
                            }

                            handler.sendEmptyMessage(0);

                        }

                    }.start();

                }
            });

            searchValue = (EditText) view.findViewById(R.id.searchValue);
        }

        return view;
    }

    private class GetDataTask extends AsyncTask<Void, Void, String[]> {

        @Override
        protected String[] doInBackground(Void... params) {
            // Simulates a background job.

            // Clear the list of the videos
            itemsListView.clear();

            if (FinderVideosActivity.this.searchValue.getText() != null) {
                // Get the new ones
                try {
                    setListVideos(managerVideoPlayer.getSearchedVideos(
                            FinderVideosActivity.this.searchValue.getText()
                                    .toString(),
                            ((BaseFragmentActivity) FinderVideosActivity.this
                                    .getActivity()).getShareApp().getIdUser()));
                } catch (JSONException e) {

                    e.printStackTrace();
                }

                for (VoizeeVideo video : getListVideos())
                    itemsListView.add(video.getTitulo());
            }

            String[] array = new String[itemsListView.size()];
            itemsListView.toArray(array);

            return array;
        }

        @Override
        protected void onPostExecute(String[] result) {
            itemsListView.addFirst("Added after refresh...");

            // Call onRefreshComplete when the list has been refreshed.
            ((PullToRefreshListView) getListView()).onRefreshComplete();

            super.onPostExecute(result);
        }
    }

    private void setListItemsAdapter(String[] items2) {
        listView.setAdapter(new MyCustomAdapter(FinderVideosActivity.this
                .getActivity(), R.layout.row, items2));
    }

    public EditText getLocText() {
        return locText;
    }

    public void setLocText(EditText locText) {
        this.locText = locText;
    }

    public LinkedList<String> getItemsListView() {
        return itemsListView;
    }

    public void setItemsListView(LinkedList<String> itemsListView) {
        this.itemsListView = itemsListView;
    }

    public VoizeeApiManager getManagerVideoPlayer() {
        return managerVideoPlayer;
    }

    public void setManagerVideoPlayer(VoizeeApiManager managerVideoPlayer) {
        this.managerVideoPlayer = managerVideoPlayer;
    }

    public PullToRefreshListView getListView() {
        return listView;
    }

    public void setListView(PullToRefreshListView listView) {
        this.listView = listView;
    }

    @Override
    public String getTitleFragment() {

        return title;
    }

    public ProgressDialog getDialogLoading() {
        return dialogLoading;
    }

    public void setDialogLoading(ProgressDialog dialogLoading) {
        this.dialogLoading = dialogLoading;
    }

    public EditText getSearchValue() {
        return searchValue;
    }

    public void setSearchValue(EditText searchValue) {
        this.searchValue = searchValue;
    }

}
