/**
 *
 */
package com.waiyaki.voizee.activity.ui;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.util.LinkedList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.markupartist.android.widget.PullToRefreshListView;
import com.markupartist.android.widget.PullToRefreshListView.OnRefreshListener;
import com.waiyaki.voizee.dao.model.VoizeeVideo;
import com.waiyaki.voizee.main.R;
import com.waiyaki.voizee.manager.VoizeeApiManager;
import com.waiyaki.voizee.manager.VoizeeApiManagerImpl;
import com.waiyaki.voizee.util.YoutubeUtils;

/**
 * @author Francisco Javier Morant Moya
 */
public class FinderYoutubeActivity extends BaseActivity {

    private EditText editTextQuery = null;
    private LinkedList<String> itemsListView;
    private VoizeeApiManager managerVideoPlayer = new VoizeeApiManagerImpl();
    private List<VoizeeVideo> listVideos = null;
    private PullToRefreshListView listView;
    private ProgressDialog dialogLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState, R.layout.buscadoryoutube);

        //Put a constraint to be logged in to record and upload something


        itemsListView = new LinkedList<String>();

        // EditText
        setLocText((EditText) findViewById(R.id.editTextLoc));

        // Add listview
        setListView((PullToRefreshListView) findViewById(R.id.idListView));

        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view,
                                    int position, long arg) {
                VoizeeVideo videoYoutube = listVideos.get(position - 1);

                Intent intentVideoPlayer = new Intent(
                        FinderYoutubeActivity.this.getBaseContext(),
                        RecorderActivity.class);

                Bundle bundleVideoYotube = new Bundle();

                bundleVideoYotube.putSerializable("serializableVideoYotube",
                        (Serializable) videoYoutube);

                intentVideoPlayer
                        .putExtra("urlVideoYoutube", bundleVideoYotube);

                FinderYoutubeActivity.this.startActivity(intentVideoPlayer);
            }
        });

        // Set a listener to be invoked when the list should be refreshed.
        listView.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Do work to refresh the list here.
                new GetDataTask().execute();
            }
        });

        // Add button to search
        ImageButton searchBtn = (ImageButton) findViewById(R.id.buttonFilms);

        // add a click listener to the button
        searchBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                // hide virtual keyboard
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(
                        editTextQuery.getApplicationWindowToken(), 0);

                // Show searching
                dialogLoading = new ProgressDialog(FinderYoutubeActivity.this);
                dialogLoading.setTitle("Voizee");
                dialogLoading.setMessage(FinderYoutubeActivity.this
                        .getText(R.string.finder_msg_loading));
                dialogLoading.setIndeterminate(true);
                dialogLoading.setCancelable(true);
                dialogLoading.show();

                new Thread() {

                    public void run() {

                        try {
                            sleep(1000);
                        } catch (InterruptedException e1) {
                            e1.printStackTrace();
                        }

                        itemsListView.clear();

                        if (editTextQuery != null && editTextQuery.getText() != null
                                && !editTextQuery.getText().toString().equals(""))

                        {

                            try {
                                listVideos = managerVideoPlayer
                                        .searchYoutubeVideos(editTextQuery
                                                .getText().toString()
                                                .toLowerCase(), null);
                            } catch (MalformedURLException e) {
                                e.printStackTrace();
                                dialogLoading.dismiss();
                            }

                            for (VoizeeVideo dtoFilm : listVideos)
                                itemsListView.add(dtoFilm.getTitulo());

                        }

                        handler.sendEmptyMessage(0);

                    }

                }.start();

            }
        });

        // hide virtual keyboard
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editTextQuery.getWindowToken(), 0);

        // Show searching
        dialogLoading = new ProgressDialog(FinderYoutubeActivity.this);
        dialogLoading.setTitle("Voizee");
        dialogLoading.setMessage(FinderYoutubeActivity.this
                .getText(R.string.finder_msg_loading));
        dialogLoading.setIndeterminate(true);
        dialogLoading.setCancelable(true);
        dialogLoading.show();

        new Thread() {

            public void run() {

                try {
                    sleep(1000);
                } catch (InterruptedException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

                itemsListView.clear();

                if (editTextQuery != null && editTextQuery.getText() != null)

                {

                    try {
                        listVideos = managerVideoPlayer.searchYoutubeVideos(
                                editTextQuery.getText().toString()
                                        .toLowerCase(),
                                FinderYoutubeActivity.this.getShareApp()
                                        .getLanguageUser());
                    } catch (MalformedURLException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        dialogLoading.dismiss();
                    }

                    for (VoizeeVideo dtoFilm : listVideos)
                        itemsListView.add(dtoFilm.getTitulo());

                }

                handler.sendEmptyMessage(0);

            }

        }.start();

    }

    private class GetDataTask extends AsyncTask<Void, Void, String[]> {

        @Override
        protected String[] doInBackground(Void... params) {
            // Simulates a background job.
            if (editTextQuery != null && editTextQuery.getText() != null
                    )

            {

                try {
                    listVideos = managerVideoPlayer.searchYoutubeVideos(
                            editTextQuery.getText().toString().toLowerCase(),
                            null);
                } catch (MalformedURLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    dialogLoading.dismiss();
                }

                for (VoizeeVideo dtoFilm : listVideos)
                    itemsListView.add(dtoFilm.getTitulo());

            }

            String[] array = new String[itemsListView.size()];
            itemsListView.toArray(array);
            return array;
        }

        @Override
        protected void onPostExecute(String[] result) {
            itemsListView.addFirst("Added after refresh...");

            // Call onRefreshComplete when the list has been refreshed.
            ((PullToRefreshListView) getListView()).onRefreshComplete();

            super.onPostExecute(result);
        }
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            // hide virtual keyboard
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(
                    editTextQuery.getApplicationWindowToken(), 0);

//			if (msg!=null && msg.getData()!=null && !msg.getData().isEmpty()) {
//
//			} else {
            if (itemsListView.size() > 0) {
                String[] array = new String[itemsListView.size()];
                itemsListView.toArray(array);
                setListItemsAdapter(array);
            } else {
                Toast.makeText(FinderYoutubeActivity.this,
                        getText(R.string.no_results_finder),
                        Toast.LENGTH_LONG).show();
            }

            // dismiss the progress dialog
            dialogLoading.dismiss();
            //}

        }
    };

    private void setListItemsAdapter(String[] items2) {
        listView.setAdapter(new MyCustomAdapter(this, R.layout.row, items2));
    }

    static class ViewHolder {
        TextView text;
        TextView textViews;
        TextView textAuthor;
        TextView timestamp;
        ImageView icon;
        ProgressBar progress;
        int position;
        String urlThumbnail;
    }

    public class MyCustomAdapter extends ArrayAdapter<String> {

        public MyCustomAdapter(Context context, int textViewResourceId,
                               String[] objects) {
            super(context, textViewResourceId, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder;
            VoizeeVideo item = listVideos.get(position);

            // if (convertView == null
            // || (convertView != null && convertView.getTag() == null)) {
            convertView = View.inflate(
                    FinderYoutubeActivity.this.getBaseContext(), R.layout.row,
                    null);
            holder = new ViewHolder();
            holder.icon = (ImageView) convertView
                    .findViewById(R.id.thumbNailYoutube);

            holder.progress = (ProgressBar) convertView
                    .findViewById(R.id.prgBarLoading);

            holder.text = (TextView) convertView
                    .findViewById(R.id.textViewTitle);

            holder.text.setText(item.getTitulo());

            holder.urlThumbnail = item.getUrlThumbnail();

            holder.textAuthor = (TextView) convertView
                    .findViewById(R.id.authorVideo);

            holder.textAuthor.setVisibility(View.GONE);

            holder.textViews = (TextView) convertView
                    .findViewById(R.id.playsVideo);

            holder.textViews.setVisibility(View.GONE);

            
            ImageButton shareBtn = (ImageButton) convertView
                    .findViewById(R.id.shareBtn);

            if (shareBtn != null) {
                shareBtn.setVisibility(View.GONE);
            }

            convertView.setTag(holder);

            // Using an AsyncTask to load the slow images in a background thread
            new AsyncTask<ViewHolder, Void, Bitmap>() {
                private ViewHolder v;

                @Override
                protected Bitmap doInBackground(ViewHolder... params) {
                    v = params[0];

                    return YoutubeUtils.getBitmapFromURL(v.urlThumbnail);
                }

                @Override
                protected void onPostExecute(Bitmap result) {
                    super.onPostExecute(result);
                    // if (v.position == position) {
                    // If this item hasn't been recycled already, hide the
                    // progress and set and show the image
                    v.progress.setVisibility(View.GONE);
                    v.icon.setVisibility(View.VISIBLE);
                    v.icon.setImageBitmap(result);
                    // }
                }
            }.execute(holder);

            return convertView;
        }
    }

    public EditText getLocText() {
        return editTextQuery;
    }

    public void setLocText(EditText locText) {
        this.editTextQuery = locText;
    }

    public LinkedList<String> getItemsListView() {
        return itemsListView;
    }

    public void setItemsListView(LinkedList<String> itemsListView) {
        this.itemsListView = itemsListView;
    }

    public VoizeeApiManager getManagerVideoPlayer() {
        return managerVideoPlayer;
    }

    public void setManagerVideoPlayer(VoizeeApiManager managerVideoPlayer) {
        this.managerVideoPlayer = managerVideoPlayer;
    }

    public List<VoizeeVideo> getListFilms() {
        return listVideos;
    }

    public void setListFilms(List<VoizeeVideo> listFilms) {
        this.listVideos = listFilms;
    }

    public ListView getListView() {
        return listView;
    }

    public void setListView(PullToRefreshListView listView) {
        this.listView = listView;
    }

}
