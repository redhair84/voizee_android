/**
 *
 */
package com.waiyaki.voizee.activity.ui;

import java.util.List;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.waiyaki.voizee.dao.model.VoizeeVideo;
import com.waiyaki.voizee.main.R;
import com.waiyaki.voizee.manager.VoizeeApiManager;
import com.waiyaki.voizee.manager.VoizeeApiManagerImpl;
import com.waiyaki.voizee.util.YoutubeUtils;

/**
 * @author Francisco Javier Morant Moya
 */
public abstract class ListBaseFragment extends Fragment {

    protected List<VoizeeVideo> listVideos = null;
    private VoizeeApiManager managerVideoPlayer = new VoizeeApiManagerImpl();
    protected String title;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        int SDK_INT = android.os.Build.VERSION.SDK_INT;

        if (SDK_INT > 8) {

            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectDiskReads().detectDiskWrites().detectNetwork()
                    .penaltyLog().build());

        }

    }

    static class ViewHolder {
        TextView text;
        TextView textViews;
        TextView textAuthor;
        TextView timestamp;
        ImageButton shareBtn;
        ImageView icon;
        ProgressBar progress;
        int position;
        String urlThumbnail;
    }

    public class MyCustomAdapter extends ArrayAdapter<String> {

        public MyCustomAdapter(Context context, int textViewResourceId,
                               String[] objects) {
            super(context, textViewResourceId, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder;

            convertView = View.inflate(ListBaseFragment.this.getActivity()
                    .getBaseContext(), R.layout.row, null);

            if (listVideos.size() > 0) {
                final VoizeeVideo item = listVideos.get(position);

                // if (convertView == null
                // || (convertView != null && convertView.getTag() == null)) {

                holder = new ViewHolder();
                holder.icon = (ImageView) convertView
                        .findViewById(R.id.thumbNailYoutube);

                holder.progress = (ProgressBar) convertView
                        .findViewById(R.id.prgBarLoading);

                holder.text = (TextView) convertView
                        .findViewById(R.id.textViewTitle);

                holder.text.setText(item.getTitulo());

                holder.urlThumbnail = item.getUrlThumbnail();

                holder.text = (TextView) convertView
                        .findViewById(R.id.textViewTitle);

                holder.text.setText(item.getTitulo());

                holder.textAuthor = (TextView) convertView
                        .findViewById(R.id.authorVideo);

                holder.textAuthor.setText("@" + item.getAutor());

                holder.textViews = (TextView) convertView
                        .findViewById(R.id.playsVideo);

                holder.textViews.setText(item.getViews() + " "
                        + ListBaseFragment.this.getText(R.string.label_num_views));

                holder.shareBtn = (ImageButton) convertView
                        .findViewById(R.id.shareBtn);



                if (holder.shareBtn != null) {
                    holder.shareBtn.setFocusable(false);

                    holder.shareBtn.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {


                            try {
                                Intent intent = new Intent(Intent.ACTION_SEND);
                                intent.putExtra(
                                        Intent.EXTRA_TEXT,
                                        getText(R.string.upload_share_video)
                                                + " " + item.getUrlLink());
                                intent.setType("text/plain");

                                startActivity(Intent.createChooser(intent,
                                        getText(R.string.upload_share_action_video)));

                            } catch (final ActivityNotFoundException e) {
                                Log.i("Voizee", "no twitter native", e);
                            }

                        }
                    });
                }

                convertView.setTag(holder);

                // Using an AsyncTask to load the slow images in a background thread
                new AsyncTask<ViewHolder, Void, Bitmap>() {
                    private ViewHolder v;

                    @Override
                    protected Bitmap doInBackground(ViewHolder... params) {
                        v = params[0];

                        return YoutubeUtils.getBitmapFromURL(v.urlThumbnail);
                    }

                    @Override
                    protected void onPostExecute(Bitmap result) {
                        super.onPostExecute(result);
                        // if (v.position == position) {
                        // If this item hasn't been recycled already, hide the
                        // progress and set and show the image
                        v.progress.setVisibility(View.GONE);
                        v.icon.setVisibility(View.VISIBLE);
                        if (result != null)
                            v.icon.setImageBitmap(result);
                        // else
                        // v.icon.setBackground(ListBaseFragment.this
                        // .getActivity().getResources()
                        // .getDrawable(R.drawable.thumbnail));
                        // }
                    }
                }.execute(holder);
            }

            return convertView;
        }
    }

    public class NoAuthorAdapter extends ArrayAdapter<String> {

        public NoAuthorAdapter(Context context, int textViewResourceId,
                               String[] objects) {
            super(context, textViewResourceId, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder;
            VoizeeVideo item = listVideos.get(position);

            // if (convertView == null
            // || (convertView != null && convertView.getTag() == null)) {
            convertView = View.inflate(ListBaseFragment.this.getActivity()
                    .getBaseContext(), R.layout.row, null);
            holder = new ViewHolder();
            holder.icon = (ImageView) convertView
                    .findViewById(R.id.thumbNailYoutube);

            holder.progress = (ProgressBar) convertView
                    .findViewById(R.id.prgBarLoading);

            holder.text = (TextView) convertView
                    .findViewById(R.id.textViewTitle);

            holder.text.setText(item.getTitulo());

            holder.urlThumbnail = item.getUrlThumbnail();

            holder.text = (TextView) convertView
                    .findViewById(R.id.textViewTitle);

            holder.text.setText(item.getTitulo());

            holder.textAuthor = (TextView) convertView
                    .findViewById(R.id.authorVideo);

            holder.textAuthor.setVisibility(View.GONE);

            holder.textViews = (TextView) convertView
                    .findViewById(R.id.playsVideo);

            holder.textViews.setVisibility(View.GONE);


            holder.shareBtn = (ImageButton) convertView
                    .findViewById(R.id.shareBtn);

            if(holder.shareBtn!=null)
                holder.shareBtn.setVisibility(View.GONE);

            convertView.setTag(holder);

            // Using an AsyncTask to load the slow images in a background thread
            new AsyncTask<ViewHolder, Void, Bitmap>() {
                private ViewHolder v;

                @Override
                protected Bitmap doInBackground(ViewHolder... params) {
                    v = params[0];

                    return YoutubeUtils.getBitmapFromURL(v.urlThumbnail);
                }

                @Override
                protected void onPostExecute(Bitmap result) {
                    super.onPostExecute(result);
                    // if (v.position == position) {
                    // If this item hasn't been recycled already, hide the
                    // progress and set and show the image
                    v.progress.setVisibility(View.GONE);
                    v.icon.setVisibility(View.VISIBLE);
                    if (result != null)
                        v.icon.setImageBitmap(result);
                    // else
                    // v.icon.setBackground(ListBaseFragment.this
                    // .getActivity().getResources()
                    // .getDrawable(R.drawable.thumbnail));
                    // }
                }
            }.execute(holder);

            return convertView;
        }
    }

    public List<VoizeeVideo> getListVideos() {
        return listVideos;
    }

    public void setListVideos(List<VoizeeVideo> listVideos) {
        this.listVideos = listVideos;
    }

    public VoizeeApiManager getManagerVideoPlayer() {
        return managerVideoPlayer;
    }

    public void setManagerVideoPlayer(VoizeeApiManager managerVideoPlayer) {
        this.managerVideoPlayer = managerVideoPlayer;
    }

    public abstract String getTitleFragment();

}
