/**
 *
 */
package com.waiyaki.voizee.activity.ui;

import org.json.JSONException;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.waiyaki.voizee.dao.model.ResultLogin;
import com.waiyaki.voizee.main.R;
import com.waiyaki.voizee.util.StatusLogin;

/**
 * @author Francisco Javier Morant Moya
 */
public class LoginActivity extends BaseActivity {

    private Button loginBtn;
    private Button signUpBtn;
    private EditText emailValue;
    private EditText passValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState, R.layout.login_voizee);

        loginBtn = (Button) this.findViewById(R.id.logInBtn);

        loginBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // Show the transaction dialog
                showDialog(LoginActivity.this.getText(
                        R.string.initial_loading_dialog).toString(),false);

                // Checks when the user want to login
                if (emailValue != null && emailValue.getText()!=null && emailValue.getText().length() < 5)
                    LoginActivity.this.showErrorMessage(
                            null,
                            LoginActivity.this.getText(
                                    R.string.warning_login_user).toString(),
                            false, true);

                else if (passValue != null && passValue.getText()!=null && passValue.getText().length() < 5)

                    LoginActivity.this.showErrorMessage(
                            null,
                            LoginActivity.this.getText(
                                    R.string.warning_login_pass).toString(),
                            false, true);
                else {

                    // When the checks are right
                    try {

                        ResultLogin result = LoginActivity.this
                                .getShareApp()
                                .getManagerLogin()
                                .login(emailValue.getText().toString(),
                                        passValue.getText().toString());

                        if (result.isStatus()) {
                            // Set the id user
                            getShareApp().setIdUser(result.getIdUsuario());

                            // Set the token session

                            getShareApp().setTokenSession(result.getToken());

                            // Set the status
                            getShareApp().getStatusLogin().setCurrentStatus(
                                    StatusLogin.STATUS_LOGIN_VZ);

                            Toast.makeText(
                                    LoginActivity.this,
                                    LoginActivity.this
                                            .getText(R.string.success_login),
                                    Toast.LENGTH_LONG).show();

                            getIntent().putExtra("user_name",
                                    result.getUserName());
                            setResult(RESULT_OK, getIntent());
                            LoginActivity.this.finish();

                        } else {

                            // Loging
                            // didn't
                            // work

                            if (result.getResult() != null && !result.getResult().equals(""))

                                LoginActivity.this.showErrorMessage(null,
                                        result.getResult(), false, true);
                            else

                                LoginActivity.this.showErrorMessage(null,
                                        LoginActivity.this
                                                .getText(R.string.error_login)
                                                .toString(), false, true);


                        }
                    } catch (Exception e) {

                        // Loging
                        // didn't
                        // work

                        LoginActivity.this.showErrorMessage(null,
                                LoginActivity.this
                                        .getText(R.string.error_login)
                                        .toString(), false, true);
                    }
                }
            }
        });

        signUpBtn = (Button) this.findViewById(R.id.signUpBtn);
        signUpBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                LoginActivity.this.startActivity(new Intent(LoginActivity.this
                        .getBaseContext(), RegistrationActivity.class));
            }
        });

        emailValue = (EditText) this.findViewById(R.id.emailValue);

        passValue = (EditText) this.findViewById(R.id.passValue);
    }
}
