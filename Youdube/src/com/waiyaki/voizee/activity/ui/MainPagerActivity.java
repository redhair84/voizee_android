/**
 *
 */
package com.waiyaki.voizee.activity.ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.Session.OpenRequest;
import com.facebook.Session.StatusCallback;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.viewpagerindicator.CirclePageIndicator;
import com.waiyaki.voizee.dao.model.ResultLogin;
import com.waiyaki.voizee.main.R;
import com.waiyaki.voizee.util.StatusLogin;
import com.waiyaki.voizee.util.VoizeeConstants;
import com.waiyaki.voizee.util.YoutubeUtils;

/**
 * @author Francisco Javier Morant Moya
 */
public class MainPagerActivity extends BaseFragmentActivity {

    private ListVideosAdapter mAdapter;
    private ViewPager mPager;
    private List<Fragment> fragments;
    private TextView currentListName;
    private Button btnRecordMain;
    private Button btnLoginFB;
    private Button btnLoginVZ;
    private Button btnLogout;

    private LinearLayout btnsLoginLayout;
    private LinearLayout userLoggedLayout;
    private TextView userTextName;
    private CirclePageIndicator titleIndicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_list_videos);

        if (YoutubeUtils.isOnline(this)) {

            // Create a dialog for loading the data
            mAdapter = new ListVideosAdapter(getSupportFragmentManager());

            mPager = (ViewPager) findViewById(R.id.pager);
            mPager.setAdapter(mAdapter);

            // Bind the title indicator to the adapter
            titleIndicator = (CirclePageIndicator) findViewById(R.id.titles);

            titleIndicator.setViewPager(mPager);

            titleIndicator
                    .setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {

                        @Override
                        public void onPageSelected(int position) {
                            if (VoizeeConstants.DEVELOPER_MODE) {
                                Log.d("Voizee", "Position actual " + position);

                            }

                            if (!(fragments.get(position) instanceof FinderVideosActivity)) {
                                InputMethodManager imm = (InputMethodManager) MainPagerActivity.this
                                        .getSystemService(Context.INPUT_METHOD_SERVICE);
                                if (imm != null) {

                                    imm.hideSoftInputFromWindow(
                                            MainPagerActivity.this.btnLoginFB
                                                    .getApplicationWindowToken(),
                                            0);
                                }
                            } else {

                                if (fragments.get(position).getActivity() != null) {
                                    InputMethodManager imm = (InputMethodManager) fragments
                                            .get(position)
                                            .getActivity()
                                            .getSystemService(
                                                    Context.INPUT_METHOD_SERVICE);
                                    if (imm != null) {
                                        ((FinderVideosActivity) fragments
                                                .get(position))
                                                .getSearchValue().setFocusable(
                                                true);
                                        ((FinderVideosActivity) fragments
                                                .get(position))
                                                .getSearchValue()
                                                .requestFocus();

                                        imm.showSoftInput(
                                                ((FinderVideosActivity) fragments
                                                        .get(position))
                                                        .getSearchValue(),
                                                InputMethodManager.SHOW_IMPLICIT);
                                    }
                                }

                            }

                            currentListName
                                    .setText(((ListBaseFragment) fragments
                                            .get(position)).getTitleFragment());
                        }
                    });

            // Get the textview
            currentListName = (TextView) findViewById(R.id.currentListName);

            // Get button to go to search the videos
            btnRecordMain = (Button) findViewById(R.id.mainRecordBtn);

            btnRecordMain.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    if (getShareApp().getIdUser() == null
                            || getShareApp().getIdUser().equals("")) {
                        showErrorMessage(null, "You have to be logged in to record a dubbing and upload it, to do it please log in here pressing the log in button.", false, true);
                    } else {

                        // Got to search your video
                        MainPagerActivity.this.startActivity(new Intent(
                                MainPagerActivity.this.getBaseContext(),
                                FinderYoutubeActivity.class));
                    }
                }
            });

            // Create the listener on click
            btnLoginVZ = (Button) findViewById(R.id.loginBtnVZ);
            btnLoginVZ.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    MainPagerActivity.this.startActivityForResult(new Intent(
                            MainPagerActivity.this.getBaseContext(),
                            LoginActivity.class),
                            VoizeeConstants.START_ACTIVITY_LOGIN);
                }
            });

            btnLoginFB = (Button) findViewById(R.id.loginBtnFB);
            btnLoginFB.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    // Show the transaction dialog
                    // showDialog(
                    // MainPagerActivity.this.getText(
                    // R.string.initial_loading_dialog).toString(),
                    // false);

                    MainPagerActivity.this.getShareApp().setOpeningFacebook(
                            true);

                    com.facebook.Session.StatusCallback statusCallBack = new Session.StatusCallback() {

                        // callback when session changes state
                        @Override
                        public void call(Session session, SessionState state,
                                         Exception exception) {

                            if (exception != null
                                    && exception.getMessage() != null) {
                                MainPagerActivity.this.showErrorMessage(
                                        exception, exception.getMessage(),
                                        false, true);
                                MainPagerActivity.this.getShareApp()
                                        .setOpeningFacebook(false);
                            }

                            if (session.isOpened()) {
                                if (VoizeeConstants.DEVELOPER_MODE)
                                    Log.d("Voizee", "Session opened");

                                // make request to the /me API
                                Request.executeMeRequestAsync(session,
                                        new Request.GraphUserCallback() {

                                            // callback after Graph API
                                            // response with user object
                                            @Override
                                            public void onCompleted(
                                                    GraphUser user,
                                                    Response response) {

                                                if (user != null) {
                                                    if (VoizeeConstants.DEVELOPER_MODE)
                                                        Log.d("Voizee",
                                                                "User info got it");
                                                    // Login fb in
                                                    // voizee
                                                    // api
                                                    ResultLogin resultLogin = null;

                                                    try {
                                                        resultLogin = MainPagerActivity.this
                                                                .getShareApp()
                                                                .getManagerLogin()
                                                                .loginFB(
                                                                        (String) user
                                                                                .getProperty("email"),
                                                                        user.getUsername(),
                                                                        user.getId(),
                                                                        user.getName());
                                                    } catch (Exception e) {

                                                        MainPagerActivity.this
                                                                .showErrorMessage(
                                                                        e,
                                                                        "Error intentando loguear, intentalo otra vez.",
                                                                        false,
                                                                        false);
                                                    }

                                                    if (resultLogin != null
                                                            && resultLogin
                                                            .isStatus()) {
                                                        if (VoizeeConstants.DEVELOPER_MODE)
                                                            Log.d("Voizee",
                                                                    "Result login in API good");

                                                        MainPagerActivity.this
                                                                .getShareApp()
                                                                .setOpeningFacebook(
                                                                        false);

                                                        // Set the user
                                                        // name
                                                        userTextName.setText(user
                                                                .getName());

                                                        // Set the
                                                        // status to
                                                        // none
                                                        getShareApp()
                                                                .getStatusLogin()
                                                                .setCurrentStatus(
                                                                        StatusLogin.STATUS_LOGIN_FB);

                                                        // Set the
                                                        // idUser of
                                                        // the logged in
                                                        // user
                                                        getShareApp()
                                                                .setIdUser(
                                                                        resultLogin
                                                                                .getIdUsuario());

                                                        // Set the token
                                                        // of
                                                        // the session
                                                        getShareApp()
                                                                .setTokenSession(
                                                                        resultLogin
                                                                                .getToken());

                                                        Message msg = new Message();
                                                        Bundle bdl = new Bundle();

                                                        bdl.putString(
                                                                "operation",
                                                                "refresh");
                                                        msg.setData(bdl);

                                                        handlerLoginRefresh
                                                                .sendMessage(msg);
                                                    } else {
                                                        MainPagerActivity.this
                                                                .getShareApp()
                                                                .setOpeningFacebook(
                                                                        false);

                                                        Message msg = new Message();
                                                        Bundle bdl = new Bundle();

                                                        bdl.putString(
                                                                "operation",
                                                                "error");

                                                        bdl.putString(
                                                                "errorMsg",
                                                                "Error login facebook");
                                                        msg.setData(bdl);

                                                        handlerLoginRefresh
                                                                .sendMessage(msg);
                                                    }
                                                } else {

                                                    MainPagerActivity.this
                                                            .getShareApp()
                                                            .setOpeningFacebook(
                                                                    false);

                                                    Message msg = new Message();
                                                    Bundle bdl = new Bundle();

                                                    bdl.putString("operation",
                                                            "error");
                                                    bdl.putString("errorMsg",
                                                            "User info no got it");
                                                    msg.setData(bdl);

                                                    handlerLoginRefresh
                                                            .sendMessage(msg);
                                                }
                                            }
                                        });
                            }
                        }
                    };

                    openActiveSession(MainPagerActivity.this, true,
                            statusCallBack, Arrays.asList("email"));

                }
            });

            // Get the btns and user layout
            btnsLoginLayout = (LinearLayout) this
                    .findViewById(R.id.btnsLoginLayout);

            userLoggedLayout = (LinearLayout) this
                    .findViewById(R.id.nameLoginLayout);

            // Get the user name once it is logged in
            userTextName = (TextView) this.findViewById(R.id.userTextName);

            // Get the button to log out from the app
            btnLogout = (Button) this.findViewById(R.id.logoutBtn);

            btnLogout.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (getShareApp().getStatusLogin().getCurrentStatus() == StatusLogin.STATUS_LOGIN_FB) {

                        // Close the session in Facebook
                        Session.getActiveSession()
                                .closeAndClearTokenInformation();

                        // Log out from voizee
                        getShareApp().logOutVoizee();

                        Message msg = new Message();
                        Bundle bdl = new Bundle();

                        bdl.putString("operation", "refresh");
                        msg.setData(bdl);

                        handlerLoginRefresh.sendMessage(msg);

                    }

                    if (getShareApp().getStatusLogin().getCurrentStatus() == StatusLogin.STATUS_LOGIN_VZ) {

                        // Set the status to
                        // none
                        getShareApp().getStatusLogin().setCurrentStatus(
                                StatusLogin.STATUS_LOGIN_NONE);

                        // Log out from voizee
                        getShareApp().logOutVoizee();

                        Message msg = new Message();
                        Bundle bdl = new Bundle();

                        bdl.putString("operation", "refresh");
                        msg.setData(bdl);

                        handlerLoginRefresh.sendMessage(msg);

                    }
                }
            });
        } else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setTitle("Voizee");
            dialog.setMessage(MainPagerActivity.this
                    .getText(R.string.no_internet));
            dialog.setPositiveButton(
                    MainPagerActivity.this.getText(R.string.retry_btn),
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            Intent intent = getIntent();
                            finish();
                            startActivity(intent);
                        }
                    });

            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

                @Override
                public void onCancel(DialogInterface dialog) {
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                }
            });
            dialog.show();
        }

        // Create the fragment and save them in an array
        fragments = new ArrayList<Fragment>();

    }

    @Override
    protected void onResume() {

        super.onResume();

        if (getShareApp().isDisconnected()) {
            refreshLoginStatus();
        } else if (isHasFinished()
                && !MainPagerActivity.this.getShareApp().isOpeningFacebook()) {
            new InitialLoadTaskLight().execute(null, null, null);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    public void onStart() {
        super.onStart();

        if (YoutubeUtils.isOnline(this)
                && !MainPagerActivity.this.getShareApp().isOpeningFacebook()) {
            // First initial load
            new InitialLoadTask().execute(null, null, null);
        }
    }

    private class InitialLoadTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if (VoizeeConstants.DEVELOPER_MODE) {
                Log.d("Voizee Timing", "Time Start Getting list videos: "
                        + (new Date()).getTime());
            }

            if (isHasFinished()) {
                // Show the transaction dialog
                showDialog(
                        MainPagerActivity.this.getText(
                                R.string.initial_loading_dialog).toString(),
                        false);

                //
                mAdapter.emptyFragments();
                // Refresh the viewpager
                // getmAdapter().notifyDataSetChanged();

                // Clear the different fragments
                fragments.clear();
            } else {
                this.cancel(true);
            }

        }

        /**
         * The system calls this to perform work in a worker thread and delivers
         * it the parameters given to AsyncTask.execute()
         */

		/*
         * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#doInBackground(Params[])
		 */
        @Override
        protected Void doInBackground(Void... params) {
            ListBaseFragment fragment;

            // Control errors here


            fragment = new PopularesVideosActivity(getShareApp().getIdUser());

            fragment.title = MainPagerActivity.this.getText(
                    R.string.label_most_populars_view).toString();
            fragments.add(fragment);

            fragment = new MainMostDubbedVideosActivity();

            fragment.title = MainPagerActivity.this.getText(
                    R.string.label_most_dubbed_view).toString();
            fragments.add(fragment);

            fragment = new MostViewedVideosActivity();

            fragment.title = MainPagerActivity.this.getText(
                    R.string.label_most_views_view).toString();
            fragments.add(fragment);

            fragment = new LastVideosActivity();

            fragment.title = MainPagerActivity.this.getText(
                    R.string.label_lastest_view).toString();
            fragments.add(fragment);


            if (getShareApp().getIdUser() != null
                    && !getShareApp().getIdUser().equals("")) {

                fragment = new MyVideosActivity(getShareApp().getIdUser());

                fragment.title = MainPagerActivity.this.getText(
                        R.string.label_my_videos_view).toString();
                fragments.add(fragment);

            }

            if (getShareApp().getIdUser() != null
                    && !getShareApp().getIdUser().equals("")) {

                fragment = new MyFavoriteVideosActivity(getShareApp()
                        .getIdUser());

                fragment.title = MainPagerActivity.this.getText(
                        R.string.label_my_favourites_view).toString();
                fragments.add(fragment);

            }


            fragment = new FinderVideosActivity();


            fragment.title = MainPagerActivity.this.getText(
                    R.string.label_finder_view).toString();
            fragments.add(fragment);


            return null;
        }

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#doInBackground(Params[])
		 */

        /**
         * The system calls this to perform work in the UI thread and delivers
         * the result from doInBackground()
         */
        protected void onPostExecute(Void result) {
            if (VoizeeConstants.DEVELOPER_MODE)
                Log.d("Voizee",
                        "Finished the initial loading lets remove everything");

            if (getmAdapter() != null)
                // Refresh the viewpager
                getmAdapter().notifyDataSetChanged();

            // The current step is the first one
            if (mPager != null)
                mPager.setCurrentItem(0, true);

            if (fragments.size() > 0) {
                // Set initial title current list
                currentListName.setText(((ListBaseFragment) fragments.get(0))
                        .getTitleFragment());
            }

            // Set the current item
            titleIndicator.setCurrentItem(0);

            // Remove the dialog
            setHasFinished(true);

            if (VoizeeConstants.DEVELOPER_MODE) {
                Log.d("Voizee Timing", "Time End Getting list videos: "
                        + (new Date()).getTime());
            }

            //Show initial message if it has changed the version number or it is your first time
            if (!getShareApp().isShownInitialDialog()) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(MainPagerActivity.this);
                dialog.setTitle("Voizee");
                dialog.setMessage(MainPagerActivity.this
                        .getText(R.string.initial_hello) + "\n\n\n" + MainPagerActivity.this
                        .getText(R.string.changes_version));
                dialog.setPositiveButton(
                        "OK", null);

                dialog.show();

                //The initial dialog is shown.
                getShareApp().setShownInitialDialog(true);
            }


        }
    }

    private class InitialLoadTaskLight extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Show the transaction dialog
            showDialog(
                    MainPagerActivity.this.getText(
                            R.string.initial_loading_dialog).toString(), false);

            //
            // mAdapter.emptyFragments();
        }

        /**
         * The system calls this to perform work in a worker thread and delivers
         * it the parameters given to AsyncTask.execute()
         */

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#doInBackground(Params[])
		 */
        @Override
        protected Void doInBackground(Void... params) {

            return null;
        }

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#doInBackground(Params[])
		 */

        /**
         * The system calls this to perform work in the UI thread and delivers
         * the result from doInBackground()
         */
        protected void onPostExecute(Void result) {
            if (VoizeeConstants.DEVELOPER_MODE)
                Log.d("Voizee",
                        "Finished the initial loading lets remove everything");

            // Remove the dialog
            setHasFinished(true);

            if (getmAdapter() != null)
                // Refresh the viewpager
                getmAdapter().notifyDataSetChanged();

            // The current step is the first one
            if (mPager != null)
                mPager.setCurrentItem(0, true);

            if (fragments.size() > 0 && currentListName != null) {
                // Set initial title current list
                currentListName.setText(((ListBaseFragment) fragments.get(0))
                        .getTitleFragment());
            }

            if (titleIndicator != null)
                // Set the current item
                titleIndicator.setCurrentItem(0);

        }
    }

    public class ListVideosAdapter extends FragmentPagerAdapter {

        FragmentManager fm;

        public ListVideosAdapter(FragmentManager fm) {
            super(fm);
            this.fm = fm;
        }

        @Override
        public int getCount() {
            // Log.d("Voizee", "Fragments size " + fragments.size());
            return fragments.size();
        }

        @Override
        public Fragment getItem(int position) {

            if (VoizeeConstants.DEVELOPER_MODE)
                Log.d("Voizee", "Get fragment in position " + position);

            return fragments.get(position);

        }

        @Override
        public int getItemPosition(Object object) {

            if (VoizeeConstants.DEVELOPER_MODE)
                Log.d("Voizee", "Get item position object : " + object);

            return POSITION_NONE;

        }

        public void emptyFragments() {
            for (int i = 0; i < fragments.size(); i++)
                fm.beginTransaction().remove(fragments.get(i)).commit();

            // FragmentManager fm = getActivity().getSupportFragmentManager();
//			for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
//				fm.popBackStack();
//			}

        }
    }

    public void refreshLoginStatus() {

        // Gone the buttons
        // layout and show
        // the user text
        // layout

        if (getShareApp().getStatusLogin().getCurrentStatus() == StatusLogin.STATUS_LOGIN_NONE) {
            userLoggedLayout.setVisibility(View.GONE);
            btnsLoginLayout.setVisibility(View.VISIBLE);

            // First initial load
            new InitialLoadTask().execute(null, null, null);
        } else if (getShareApp().getStatusLogin().getCurrentStatus() == StatusLogin.STATUS_LOGIN_FB) {
            userLoggedLayout.setVisibility(View.VISIBLE);
            btnsLoginLayout.setVisibility(View.GONE);

            // Set disconnected to true
            getShareApp().setDisconnected(false);

            // First initial load
            new InitialLoadTask().execute(null, null, null);
        } else if (getShareApp().getStatusLogin().getCurrentStatus() == StatusLogin.STATUS_LOGIN_VZ) {
            userLoggedLayout.setVisibility(View.VISIBLE);
            btnsLoginLayout.setVisibility(View.GONE);

            // Set disconnected to true
            getShareApp().setDisconnected(false);

            // First initial load
            new InitialLoadTask().execute(null, null, null);
        } else {
            userLoggedLayout.setVisibility(View.GONE);
            btnsLoginLayout.setVisibility(View.VISIBLE);

            // Set disconnected to true
            getShareApp().setDisconnected(true);
        }

    }

    private Handler handlerLoginRefresh = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            if (msg != null && msg.getData() != null) {

                String op = (String) msg.getData().get("operation");

                if (op != null && op.equals("refresh"))
                    refreshLoginStatus();

                if (op != null && op.equals("error")) {
                    String errorMsg = (String) msg.getData().get("errorMsg");

                    MainPagerActivity.this.showErrorMessage(null, errorMsg, false,
                            true);
                }

                if (op != null && op.equals("successRegister")) {
                    Toast.makeText(
                            MainPagerActivity.this,
                            MainPagerActivity.this
                                    .getText(R.string.confirm_msg_register),
                            Toast.LENGTH_LONG).show();
                }

            }

        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == VoizeeConstants.START_ACTIVITY_LOGIN) {

            if (data != null) {

                // Set the user name
                userTextName.setText(data.getStringExtra("user_name"));
                // Set the status to
                // none
                getShareApp().getStatusLogin().setCurrentStatus(
                        StatusLogin.STATUS_LOGIN_VZ);

                Message msg = new Message();
                Bundle bdl = new Bundle();

                bdl.putString("operation", "refresh");
                msg.setData(bdl);

                handlerLoginRefresh.sendMessage(msg);
            }
        } else {

            Session.getActiveSession().onActivityResult(this, requestCode,
                    resultCode, data);
        }
    }

    public ListVideosAdapter getmAdapter() {
        return mAdapter;
    }

    public void setmAdapter(ListVideosAdapter mAdapter) {
        this.mAdapter = mAdapter;
    }

    private static Session openActiveSession(Activity activity,
                                             boolean allowLoginUI, StatusCallback callback,
                                             List<String> permissions) {
        OpenRequest openRequest = new OpenRequest(activity).setPermissions(
                permissions).setCallback(callback);
        Session session = new Session.Builder(activity).build();
        if (SessionState.CREATED_TOKEN_LOADED.equals(session.getState())
                || allowLoginUI) {
            Session.setActiveSession(session);
            session.openForRead(openRequest);
            return session;
        }
        return null;
    }
}
