/**
 *
 */
package com.waiyaki.voizee.activity.ui;

import java.util.LinkedList;

import org.json.JSONException;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;

import com.markupartist.android.widget.PullToRefreshListView;
import com.markupartist.android.widget.PullToRefreshListView.OnRefreshListener;
import com.waiyaki.voizee.dao.model.VoizeeVideo;
import com.waiyaki.voizee.main.R;
import com.waiyaki.voizee.main.VoizeeApplication;
import com.waiyaki.voizee.util.VoizeeConstants;

/**
 * @author Francisco Javier Morant Moya
 */
public class MyVideosActivity extends ListBaseFragment {

    private EditText locText = null;
    private LinkedList<String> itemsListView;
    private PullToRefreshListView listView;

    public MyVideosActivity() {
        // Loading the videos from server
        if (VoizeeConstants.DEVELOPER_MODE)
            Log.d("Voizee", "Create LastVideosActivity");

        try {
            setListVideos(getManagerVideoPlayer().getMyVideos(null));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public MyVideosActivity(String idUser) {
        if (VoizeeConstants.DEVELOPER_MODE)
            Log.d("Voizee", "Create MisVideosActivity");

        // Loading the videos from Server

        if (VoizeeConstants.DEVELOPER_MODE && idUser != null) {
            Log.d("Voizee", "id user : " + idUser);
        }

        try {
            listVideos = getManagerVideoPlayer().getMyVideos(idUser);

        } catch (JSONException e) {

            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (VoizeeConstants.DEVELOPER_MODE)
            Log.d("Voizee", "Create MisVideosActivity View");

        View view = inflater.inflate(R.layout.my_videos, container, false);

        itemsListView = new LinkedList<String>();

        if (view != null) {

            // Add listview
            setListView((PullToRefreshListView) view.findViewById(R.id.idListView));

            listView.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapter, View view,
                                        int position, long arg) {
                    VoizeeVideo video = listVideos.get(position - 1);

                    Intent intentVideoPlayer = new Intent(MyVideosActivity.this
                            .getActivity(), PlayerActivity.class);

                    ((BaseFragmentActivity) MyVideosActivity.this.getActivity())
                            .getShareApp().setSelectedVideo(video);

                    MyVideosActivity.this.startActivity(intentVideoPlayer);

                }
            });

            // Set a listener to be invoked when the list should be refreshed.
            listView.setOnRefreshListener(new OnRefreshListener() {
                @Override
                public void onRefresh() {
                    // Do work to refresh the list here.
                    new GetDataTask().execute();
                }
            });

            if (listVideos != null) {

                for (VoizeeVideo video : listVideos)
                    itemsListView.add(video.getTitulo());
            }

            String[] array = new String[itemsListView.size()];
            itemsListView.toArray(array);
            setListItemsAdapter(array);
        }

        return view;
    }

    private void setListItemsAdapter(String[] items2) {
        listView.setAdapter(new MyCustomAdapter(MyVideosActivity.this
                .getActivity(), R.layout.row, items2));
    }

    private class GetDataTask extends AsyncTask<Void, Void, String[]> {

        @Override
        protected String[] doInBackground(Void... params) {
            try {
                listVideos = ((VoizeeApplication) MyVideosActivity.this
                        .getActivity().getApplication())
                        .getManagerVideoPlayer().getMyVideos(
                                ((VoizeeApplication) MyVideosActivity.this
                                        .getActivity().getApplication())
                                        .getIdUser());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (listVideos != null)
                for (VoizeeVideo video : listVideos)
                    itemsListView.add(video.getTitulo());

            String[] array = new String[itemsListView.size()];
            itemsListView.toArray(array);
            return array;
        }

        @Override
        protected void onPostExecute(String[] result) {
            itemsListView.addFirst("Added after refresh...");

            // Call onRefreshComplete when the list has been refreshed.
            ((PullToRefreshListView) getListView()).onRefreshComplete();

            super.onPostExecute(result);
        }
    }

    public EditText getLocText() {
        return locText;
    }

    public void setLocText(EditText locText) {
        this.locText = locText;
    }

    public LinkedList<String> getItemsListView() {
        return itemsListView;
    }

    public void setItemsListView(LinkedList<String> itemsListView) {
        this.itemsListView = itemsListView;
    }

    public PullToRefreshListView getListView() {
        return listView;
    }

    public void setListView(PullToRefreshListView listView) {
        this.listView = listView;
    }

    @Override
    public String getTitleFragment() {
        return title;
    }

}
