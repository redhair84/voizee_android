/**
 *
 */
package com.waiyaki.voizee.activity.ui;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import android.annotation.TargetApi;
import android.os.Build;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.waiyaki.voizee.dao.model.VoizeeVideo;
import com.waiyaki.voizee.main.R;
import com.waiyaki.voizee.util.PlayerStatus;
import com.waiyaki.voizee.util.PlayerVideoView;
import com.waiyaki.voizee.util.VoizeeConstants;
import com.waiyaki.voizee.util.YoutubeUtils;

/**
 * @author Francisco Javier Morant Moya
 */
public class PlayerActivity extends BaseActivity {

    private VoizeeVideo videoSelected;

    EditText editTextAuthor;
    EditText editTextTitulo;

    // Buttons recorder
    private ImageButton playBtn;
    private ImageButton dubbItBtn;

    private LinearLayout barControls;
    private ImageView imageOnda;
    private ImageView imageOndaCapa;
    private LinearLayout layoutImageOnda;

    private SeekBar vSeekBarProgress;
    private SeekBar vSeekBarVol;
    private Handler handlerSeekBar;
    private Thread threadSeekBar;
    private TextView counterVideo;
    private TextView durationVideo;

    private int volumenOriginal = 0;
    private int audioDuration = 0;

    private boolean isLoading = true;
    private boolean hasEnded = false;

    private PlayerStatus playerStatus = new PlayerStatus();

    // Bookmark,like,dislike,report buttons
    private LinearLayout userBtnsLayout;

    private ImageButton bookMarkBtn;
    private ImageButton likeBtn;
    private ImageButton disLikeBtn;
    private ImageButton reportBtn;

    private LinearLayout shareBtnLayout;

    private ImageButton replayBtn;
    private ImageButton twitterBtn;
    private ImageButton fbBtn;

    private static final List<String> PERMISSIONS = Arrays
            .asList("publish_actions");
    private static final String PENDING_PUBLISH_KEY = "pendingPublishReauthorization";
    private boolean pendingPublishReauthorization = false;

    // private Runnable r;
    // private final Handler handler = new Handler();

    @Override
    public void onCreate(Bundle savedInstanceState) {

        // Call to the super to create the video player
        super.onCreate(savedInstanceState, R.layout.videoplayer);

        // Show dialog for transaction
        showDialog(getText(R.string.loading_video_player).toString(), false);


        // Check if it comes an object in the bundle
        if (getIntent().getExtras() != null && getIntent().getExtras().get("bundleVideo") != null) {

            // Get the bundle
            Bundle bndObj = (Bundle) getIntent().getExtras().get("bundleVideo");
            Object bundleVideo = bndObj
                    .getSerializable(VoizeeConstants.VIDEO_VALUE_REQUESTED);

            // It is a server video
            if (bundleVideo != null) {
                videoSelected = (VoizeeVideo) bundleVideo;
                volumenOriginal = videoSelected.getVolume();

                if (VoizeeConstants.DEVELOPER_MODE)
                    Log.d("Voizee", "Volumen original " + volumenOriginal);

                try {
                    videoSelected.setUrlVideo(getShareApp()
                            .getManagerVideoPlayer()
                            .getVideo(videoSelected.getId()).getUrlVideo());
                } catch (Exception e) {

                    // Show error to the user
                    showErrorMessage(e, getText(R.string.error_loading_video)
                            .toString(), true, true);
                }
            } else {

                // Show error to the user
                showErrorMessage(null, getText(R.string.error_loading_video)
                        .toString(), true, true);
            }
        } else if (getShareApp().getSelectedVideo() != null) {
            videoSelected = getShareApp().getSelectedVideo();

            volumenOriginal = videoSelected.getVolume();
            if (VoizeeConstants.DEVELOPER_MODE)
                Log.d("Voizee", "Volumen original " + volumenOriginal);

            try {
                videoSelected.setUrlVideo(getShareApp().getManagerVideoPlayer()
                        .getVideo(videoSelected.getId()).getUrlVideo());
            } catch (Exception e) {

                // Show error to the user
                showErrorMessage(e, getText(R.string.error_loading_video)
                        .toString(), true, true);
            }

        } else if (getIntent().getData() != null) {
            List<String> params = getIntent().getData().getPathSegments();

            for (String param : params) {
                System.out.println("Param: " + param);
            }

            if (params.size() > 0) {
                String videoId = params.get(2);

                if (videoId != null) {

                    try {
                        videoSelected = getShareApp().getManagerVideoPlayer().getVoizeeVideo(videoId);

                        videoSelected.setUrlVideo(getShareApp().getManagerVideoPlayer()
                                .getVideo(videoSelected.getId()).getUrlVideo());

                    } catch (Exception e) {

                        // Show error to the user
                        showErrorMessage(e, getText(R.string.error_loading_video)
                                .toString(), true, true);
                    }

                } else {
                    // Show error to the user
                    showErrorMessage(null, getText(R.string.error_loading_video)
                            .toString(), true, true);
                }
            } else {
                // Show error to the user
                showErrorMessage(null, getText(R.string.error_loading_video)
                        .toString(), true, true);
            }


        } else {

            // Show error to the user
            showErrorMessage(null, getText(R.string.error_loading_video)
                    .toString(), true, true);
        }

        if (videoSelected != null) {
            if (VoizeeConstants.DEVELOPER_MODE)
                Log.d("Voizee", "video url " + videoSelected.getUrlVideo());
        } else {
            // Show error to the user
            showErrorMessage(null, getText(R.string.error_loading_video)
                    .toString(), true, true);
        }

        // Se obtiene el videoView
        initVideo(videoSelected);

        getVideoView().setOnErrorListener(new MediaPlayer.OnErrorListener() {

            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {

                showErrorMessage(null, getText(R.string.error_loading_video)
                        .toString(), true, true);
                return true;
            }
        });

        // SE crea el mediaplayer y se prepara
        setMediaPlayer(new MediaPlayer());

        // At the beginning the status is stopped
        getPlayerStatus().setCurrentStatus(PlayerStatus.STATUS_STOPPED);

        // Set the audio streem type to alarm so that we can play both sounds at
        // the same time
        getMediaPlayer().setAudioStreamType(AudioManager.STREAM_ALARM);

        try {
            if (VoizeeConstants.DEVELOPER_MODE)
                Log.d("Voizee", "audio url " + videoSelected.getUrlAudio());
            getMediaPlayer().setDataSource(this,
                    Uri.parse(videoSelected.getUrlAudio()));
        } catch (Exception e) {

            // Show error to the user
            showErrorMessage(e, getText(R.string.error_loading_video)
                    .toString(), true, true);
        }

        // Media player start to get ready
        try {
            getMediaPlayer().setOnPreparedListener(new OnPreparedListener() {

                @Override
                public void onPrepared(MediaPlayer mp) {

                    // Now the player it is ready
                    isLoading = false;

                    // Set the max duration of the seek bar
                    vSeekBarProgress.setMax(mp.getDuration());

                    // It gets the duration
                    audioDuration = mp.getDuration();

                    // Set the video counter
                    int currentSeconds = audioDuration / 1000;

                    int displaySeconds = currentSeconds % 60;
                    int displayMins = currentSeconds / 60;

                    durationVideo.setText(String.format(" %02d:%02d ",
                            displayMins, displaySeconds));

                    // Removes the dialog
                    setHasFinished(true);

                }
            });

            getMediaPlayer().prepareAsync();
        } catch (Exception e) {

            // Show error to the user
            showErrorMessage(e, getText(R.string.error_loading_video)
                    .toString(), true, true);
        }

        // Se configura el evento de finalizaci�n del audio doblado
        getMediaPlayer().setOnCompletionListener(
                new MediaPlayer.OnCompletionListener() {

                    @Override
                    public void onCompletion(MediaPlayer mp) {

                        if (getPlayerStatus().getCurrentStatus() != PlayerStatus.STATUS_STOPPED) {
                            // Stop the video
                            stopToPlay();

                            // It has finished the video
                            hasEnded = true;

                            // Show and hide bar controls
                            barControls.setVisibility(View.INVISIBLE);

                            ytBtn.setVisibility(View.INVISIBLE);

                            // show share buttons
                            shareBtnLayout.setVisibility(View.VISIBLE);

                            // if (getShareApp().getIdUser() != null
                            // && !getShareApp().getIdUser().equals(""))
                            // // show user button to report, bookmark.....
                            userBtnsLayout.setVisibility(View.VISIBLE);

                            imageOnda.setVisibility(View.VISIBLE);
                            imageOndaCapa.setVisibility(View.VISIBLE);
                            layoutImageOnda.setVisibility(View.VISIBLE);

                        }
                    }
                });

        // // Pause the video or resume
        getVideoView().setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {

                if (!isLoading && !hasEnded) {
                    showTemporaryControlsPlayer(5000);
                }
                return false;
            }

        });

        // Se crea la bar control
        setBarControls((LinearLayout) findViewById(R.id.barControls));

        // Create the audio manager
        setAudioManager((AudioManager) getSystemService(Context.AUDIO_SERVICE));

        int maxVolume = getAudioManager().getStreamMaxVolume(
                AudioManager.STREAM_ALARM);
        if (VoizeeConstants.DEVELOPER_MODE)
            Log.d("Voizee", "Max volume stream alarm : " + maxVolume);


        // Set the max value for the stream alarm
        getAudioManager().setStreamVolume(AudioManager.STREAM_ALARM,
                maxVolume / 2, 0);

        int maxVolumeMusic = getAudioManager().getStreamMaxVolume(
                AudioManager.STREAM_MUSIC);
        if (VoizeeConstants.DEVELOPER_MODE)
            Log.d("Voizee", "Max volume stream music : " + maxVolumeMusic);

        // Set the max value for the stream music
        getAudioManager().setStreamVolume(AudioManager.STREAM_MUSIC,
                (volumenOriginal * maxVolumeMusic) / 100, 0);

        vSeekBarVol = (SeekBar) findViewById(R.id.seekBarVolume);
        vSeekBarVol.setMax(maxVolume);
        vSeekBarVol.setProgress(maxVolume / 2);
        vSeekBarVol
                .setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        getAudioManager().setStreamVolume(
                                AudioManager.STREAM_ALARM, progress, 0);
                    }
                });

        vSeekBarProgress = (SeekBar) findViewById(R.id.seekBarProgressVideo);
        // vSeekBarProgress.setMax(getMediaPlayer().getDuration());
        vSeekBarProgress.setProgress(0);
        vSeekBarProgress.setMax(getMediaPlayer().getDuration());
        vSeekBarProgress
                .setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {

//                        final int progressGotIt = progress;
//
//
//                        // Just seek if it is
//                        if (getPlayerStatus().getCurrentStatus() == PlayerStatus.STATUS_PLAYING
//                                && fromUser) {
//
//                            Log.d("Voizee", "Progress video  "
//                                    + progressGotIt);
//
//                            Log.d("Voizee", "Initial State  Audio Position  "
//                                    + getMediaPlayer().getCurrentPosition() + "Video Position " + getVideoView().getCurrentPosition());
//
//                            //Disable the play button
//
//                            getPlayBtn().setEnabled(false);
//
//                            //PAuse the video
//                            getVideoView().pause();
//                            //Pause the media player
//                            getMediaPlayer().pause();
//
//                            Log.d("Voizee", "After Pause Audio Position  "
//                                    + getMediaPlayer().getCurrentPosition() + "Video Position " + getVideoView().getCurrentPosition());
//
//                            getVideoView().seekTo(progressGotIt);
//                            getMediaPlayer().seekTo(progressGotIt);
//
//
//                            Log.d("Voizee", "After seek Audio Position  "
//                                    + getMediaPlayer().getCurrentPosition() + "Video Position " + getVideoView().getCurrentPosition());
//
//                            //Seek the bar
//                            seekBar.setProgress(progressGotIt);
//
//
//                            syncAudioVideo();
//
//                        }

                    }
                });

        // Run the thread to move the seekbar
        threadSeekBar = new Thread(new Runnable() {
            public void run() {
                try {
                    if (VoizeeConstants.DEVELOPER_MODE)
                        Log.d("Voizee", "Vengo al runnable.");
                    int currentPos = getMediaPlayer().getCurrentPosition();

                    Message msg = new Message();
                    Bundle bdl = new Bundle();

                    bdl.putInt("progress", currentPos);
                    msg.setData(bdl);

                    handlerProgBar.sendMessage(msg);

                    if (currentPos < audioDuration
                            && getPlayerStatus().getCurrentStatus() == PlayerStatus.STATUS_PLAYING) {
                        vSeekBarProgress.postDelayed(this, 1000);
                    }

                } catch (Exception e) {
                    // Show error to the user
                    showErrorMessage(e, getText(R.string.error_loading_video)
                            .toString(), true, false);
                }

            }
        });

        counterVideo = (TextView) this.findViewById(R.id.counterVideo);

        durationVideo = (TextView) this.findViewById(R.id.durationVideo);

        // Logica del play button
        playBtn = (ImageButton) findViewById(R.id.playBtn);

        playBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                pressPlayerBtn();
                // getBarControls().startAnimation(hideBarControls);
                showTemporaryControlsPlayer(5000);
            }
        });

        dubbItBtn = (ImageButton) findViewById(R.id.dubbIt);
        dubbItBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getBarControls().setVisibility(View.VISIBLE);
                vSeekBarVol.setVisibility(View.VISIBLE);
                dubbItBtn.setVisibility(View.VISIBLE);
                ytBtn.setVisibility(View.VISIBLE);

                imageOnda.setVisibility(View.VISIBLE);
                layoutImageOnda.setVisibility(View.VISIBLE);
                imageOndaCapa.setVisibility(View.VISIBLE);

                Intent intentVideoPlayer = new Intent(PlayerActivity.this
                        .getBaseContext(), RecorderActivity.class);

                Bundle bundleVideo = new Bundle();

                bundleVideo.putSerializable("serializableVideoYotube",
                        (Serializable) videoSelected);

                intentVideoPlayer.putExtra("urlVideoYoutube", bundleVideo);

                PlayerActivity.this.startActivity(intentVideoPlayer);
            }
        });

        if (!VoizeeConstants.DEVELOPER_MODE && videoSelected != null)
            // Add a new view to the video for the statistics
            addNewViewToVideo(videoSelected);

        // It create the handler of the seekbar
        setHandlerSeekBar(new Handler());

        // Get the layout where the buttons for like,dislike,report and bookmark
        // are
        userBtnsLayout = (LinearLayout) this.findViewById(R.id.userBtnsLayout);

        // if (getShareApp().getIdUser() != null
        // && !getShareApp().getIdUser().equals(""))
        // userBtnsLayout.setVisibility(View.VISIBLE);
        // else
        // userBtnsLayout.setVisibility(View.GONE);

        // Get the bookmark button
        bookMarkBtn = (ImageButton) this.findViewById(R.id.bookMarkBtn);

        // Select or not selected depending on if the video is already
        // bookmarked

        if (videoSelected != null && videoSelected.isBookmark()) {
            bookMarkBtn.setBackgroundDrawable(this.getResources().getDrawable(
                    R.drawable.fav_on));
        } else

            bookMarkBtn.setBackgroundDrawable(this.getResources().getDrawable(
                    R.drawable.fav));

        // Set the listener
        bookMarkBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    if (PlayerActivity.this.getShareApp()
                            .getIdUser() != null && !PlayerActivity.this.getShareApp()
                            .getIdUser().equals("")) {

                        if (!videoSelected.isBookmark()) {
                            String result = PlayerActivity.this
                                    .getShareApp()
                                    .getManagerVideoPlayer()
                                    .userOperationVideoPlayer(
                                            "bookmark",
                                            PlayerActivity.this.getShareApp()
                                                    .getIdUser(),
                                            videoSelected.getIdRecording(),
                                            PlayerActivity.this.getShareApp()
                                                    .getTokenSession());
                            if (result.equals("true")) {
                                videoSelected.setBookmark(true);
                                bookMarkBtn
                                        .setBackgroundDrawable(PlayerActivity.this
                                                .getResources().getDrawable(
                                                        R.drawable.fav_on));
                                Toast.makeText(
                                        PlayerActivity.this,
                                        PlayerActivity.this.getText(
                                                R.string.bookmark_success)
                                                .toString(), Toast.LENGTH_LONG)
                                        .show();
                            } else if (result.equals("false")) {
                                showErrorMessage(
                                        null,
                                        PlayerActivity.this.getText(
                                                R.string.bookmark_fail).toString(),
                                        false, true);
                            } else if (result.equals("desconectado")) {

                                // log out voizee
                                getShareApp().logOutVoizee();

                                // Set the app as disconnected
                                getShareApp().setDisconnected(true);

                                // Show error message telling that is disconnected
                                showErrorMessage(
                                        null,
                                        PlayerActivity.this.getText(
                                                R.string.error_session_expired)
                                                .toString(), true, true);
                            }

                        } else {
                            String result = PlayerActivity.this
                                    .getShareApp()
                                    .getManagerVideoPlayer()
                                    .userOperationVideoPlayer(
                                            "remove_bookmark",
                                            PlayerActivity.this.getShareApp()
                                                    .getIdUser(),
                                            videoSelected.getIdRecording(),
                                            PlayerActivity.this.getShareApp()
                                                    .getTokenSession());
                            if (result.equals("true")) {
                                videoSelected.setBookmark(false);
                                bookMarkBtn
                                        .setBackgroundDrawable(PlayerActivity.this
                                                .getResources().getDrawable(
                                                        R.drawable.fav));
                                Toast.makeText(
                                        PlayerActivity.this,
                                        PlayerActivity.this.getText(
                                                R.string.remove_bookmark_success)
                                                .toString(), Toast.LENGTH_LONG)
                                        .show();
                            } else if (result.equals("false")) {
                                showErrorMessage(
                                        null,
                                        PlayerActivity.this.getText(
                                                R.string.remove_bookmark_error)
                                                .toString(), false, true);
                            } else if (result.equals("desconectado")) {
                                // log out voizee
                                getShareApp().logOutVoizee();

                                // Set the app as disconnected
                                getShareApp().setDisconnected(true);

                                // Show error message telling that is disconnected
                                showErrorMessage(
                                        null,
                                        PlayerActivity.this.getText(
                                                R.string.error_session_expired)
                                                .toString(), true, true);
                            }
                        }
                    } else {
                        // Show error message telling that is disconnected
                        showErrorMessage(null,
                                getText(R.string.must_login)
                                        .toString(), false, true);
                    }
                } catch (JSONException e) {

                    showErrorMessage(e,
                            PlayerActivity.this.getText(R.string.bookmark_fail)
                                    .toString(), false, true);
                }

            }
        });

        // Get the bookmark button
        likeBtn = (ImageButton) this.findViewById(R.id.likeBtn);

        if (videoSelected != null && videoSelected.isLike()) {
            likeBtn.setBackgroundDrawable(this.getResources().getDrawable(
                    R.drawable.like_on));
        } else

            likeBtn.setBackgroundDrawable(this.getResources().getDrawable(
                    R.drawable.like));

        // Set the listener
        likeBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    if (PlayerActivity.this.getShareApp()
                            .getIdUser() != null && !PlayerActivity.this.getShareApp()
                            .getIdUser().equals("")) {
                        if (!videoSelected.isLike()) {
                            String result = PlayerActivity.this
                                    .getShareApp()
                                    .getManagerVideoPlayer()
                                    .userOperationVideoPlayer(
                                            "like",
                                            PlayerActivity.this.getShareApp()
                                                    .getIdUser(),
                                            videoSelected.getIdRecording(),
                                            PlayerActivity.this.getShareApp()
                                                    .getTokenSession());
                            if (result.equals("true")) {
                                likeBtn.setBackgroundDrawable(PlayerActivity.this
                                        .getResources().getDrawable(
                                                R.drawable.like_on));
                                videoSelected.setLike(true);

                                Toast.makeText(
                                        PlayerActivity.this,
                                        PlayerActivity.this
                                                .getText(R.string.like_success),
                                        Toast.LENGTH_LONG).show();
                            } else if (result.equals("false")) {
                                showErrorMessage(
                                        null,
                                        PlayerActivity.this.getText(
                                                R.string.like_fail).toString(),
                                        false, true);
                            } else if (result.equals("desconectado")) {
                                // log out voizee
                                getShareApp().logOutVoizee();

                                // Set the app as disconnected
                                getShareApp().setDisconnected(true);

                                // Show error message telling that is disconnected
                                showErrorMessage(
                                        null,
                                        PlayerActivity.this.getText(
                                                R.string.error_session_expired)
                                                .toString(), true, true);
                            }
                        } else {
                            String result = PlayerActivity.this
                                    .getShareApp()
                                    .getManagerVideoPlayer()
                                    .userOperationVideoPlayer("remove_like",
                                            getShareApp().getIdUser(),
                                            videoSelected.getIdRecording(),
                                            getShareApp().getTokenSession());

                            if (result.equals("true")) {
                                likeBtn.setBackgroundDrawable(PlayerActivity.this
                                        .getResources()
                                        .getDrawable(R.drawable.like));
                                videoSelected.setLike(false);

                                Toast.makeText(
                                        PlayerActivity.this,
                                        PlayerActivity.this
                                                .getText(R.string.remove_like_success),
                                        Toast.LENGTH_LONG).show();
                            } else if (result.equals("false")) {
                                showErrorMessage(null,
                                        getText(R.string.remove_like_fail)
                                                .toString(), false, true);
                            } else if (result.equals("desconectado")) {
                                // log out voizee
                                getShareApp().logOutVoizee();

                                // Set the app as disconnected
                                getShareApp().setDisconnected(true);

                                // Show error message telling that is disconnected
                                showErrorMessage(null,
                                        getText(R.string.error_session_expired)
                                                .toString(), true, true);
                            }
                        }
                    } else {
                        // Show error message telling that is disconnected
                        showErrorMessage(null,
                                getText(R.string.must_login)
                                        .toString(), false, true);
                    }
                } catch (JSONException e) {

                    showErrorMessage(e, getText(R.string.error_liking_video)
                            .toString(), false, true);
                }

            }
        });

        // Get the bookmark button
        disLikeBtn = (ImageButton) this.findViewById(R.id.disLikeBtn);

        if (videoSelected != null && videoSelected.isDislike()) {
            disLikeBtn.setBackgroundDrawable(this.getResources().getDrawable(
                    R.drawable.dislike_on));
        } else

            disLikeBtn.setBackgroundDrawable(this.getResources().getDrawable(
                    R.drawable.dislike));

        // Set the listener
        disLikeBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {

                    if (PlayerActivity.this.getShareApp()
                            .getIdUser() != null && !PlayerActivity.this.getShareApp()
                            .getIdUser().equals("")) {

                        if (!videoSelected.isDislike()) {
                            String result = PlayerActivity.this
                                    .getShareApp()
                                    .getManagerVideoPlayer()
                                    .userOperationVideoPlayer("dislike",
                                            getShareApp().getIdUser(),
                                            videoSelected.getIdRecording(),
                                            getShareApp().getTokenSession());
                            if (result.equals("true")) {
                                videoSelected.setDislike(true);
                                disLikeBtn
                                        .setBackgroundDrawable(PlayerActivity.this
                                                .getResources().getDrawable(
                                                        R.drawable.dislike_on));
                                Toast.makeText(PlayerActivity.this,
                                        getText(R.string.dislike_success),
                                        Toast.LENGTH_LONG).show();
                            } else if (result.equals("false")) {
                                showErrorMessage(null,
                                        getText(R.string.dislike_fail).toString(),
                                        false, true);
                            } else if (result.equals("desconectado")) {
                                // log out voizee
                                getShareApp().logOutVoizee();

                                // Set the app as disconnected
                                getShareApp().setDisconnected(true);

                                // Show error message telling that is disconnected
                                showErrorMessage(null,
                                        getText(R.string.error_session_expired)
                                                .toString(), true, true);
                            }
                        } else {
                            String result = getShareApp().getManagerVideoPlayer()
                                    .userOperationVideoPlayer("remove_dislike",
                                            getShareApp().getIdUser(),
                                            videoSelected.getIdRecording(),
                                            getShareApp().getTokenSession());
                            if (result.equals("true")) {
                                videoSelected.setDislike(false);
                                disLikeBtn
                                        .setBackgroundDrawable(PlayerActivity.this
                                                .getResources().getDrawable(
                                                        R.drawable.dislike));

                                Toast.makeText(
                                        PlayerActivity.this,
                                        PlayerActivity.this
                                                .getText(R.string.remove_dislike_success),
                                        Toast.LENGTH_LONG).show();
                            } else if (result.equals("false")) {
                                showErrorMessage(
                                        null,
                                        PlayerActivity.this.getText(
                                                R.string.remove_dislike_fail)
                                                .toString(), false, true);
                            } else if (result.equals("desconectado")) {
                                // log out voizee
                                getShareApp().logOutVoizee();

                                // Set the app as disconnected
                                getShareApp().setDisconnected(true);

                                // Show error message telling that is disconnected
                                showErrorMessage(
                                        null,
                                        PlayerActivity.this.getText(
                                                R.string.error_session_expired)
                                                .toString(), true, true);
                            }
                        }
                    } else {
                        // Show error message telling that is disconnected
                        showErrorMessage(null,
                                getText(R.string.must_login)
                                        .toString(), false, true);
                    }
                } catch (JSONException e) {

                    showErrorMessage(
                            e,
                            PlayerActivity.this.getText(
                                    R.string.error_disliking_video).toString(),
                            false, true);
                }

            }
        });

        // Get the bookmark button
        reportBtn = (ImageButton) this.findViewById(R.id.reportBtn);

        if (videoSelected != null && videoSelected.isReport()) {
            reportBtn.setBackgroundDrawable(this.getResources().getDrawable(
                    R.drawable.denuncia_on));
        } else

            reportBtn.setBackgroundDrawable(this.getResources().getDrawable(
                    R.drawable.denuncia));

        // Set the listener
        reportBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {

                    if (!videoSelected.isReport()) {
                        String result = PlayerActivity.this
                                .getShareApp()
                                .getManagerVideoPlayer()
                                .userOperationVideoPlayer(
                                        "report",
                                        PlayerActivity.this.getShareApp()
                                                .getIdUser(),
                                        videoSelected.getIdRecording(),
                                        PlayerActivity.this.getShareApp()
                                                .getTokenSession());

                        if (result.equals("true")) {
                            videoSelected.setReport(true);
                            reportBtn.setBackgroundDrawable(PlayerActivity.this
                                    .getResources().getDrawable(
                                            R.drawable.denuncia_on));

                            Toast.makeText(
                                    PlayerActivity.this,
                                    PlayerActivity.this
                                            .getText(R.string.report_success),
                                    Toast.LENGTH_LONG).show();

                        } else if (result.equals("false")) {
                            showErrorMessage(
                                    null,
                                    PlayerActivity.this.getText(
                                            R.string.report_fail).toString(),
                                    false, true);
                        } else if (result.equals("desconectado")) {
                            // log out voizee
                            getShareApp().logOutVoizee();

                            // Set the app as disconnected
                            getShareApp().setDisconnected(true);

                            // Show error message telling that is disconnected
                            showErrorMessage(
                                    null,
                                    PlayerActivity.this.getText(
                                            R.string.error_session_expired)
                                            .toString(), true, true);
                        }
                    } else {
                        String result = PlayerActivity.this
                                .getShareApp()
                                .getManagerVideoPlayer()
                                .userOperationVideoPlayer(
                                        "remove_report",
                                        PlayerActivity.this.getShareApp()
                                                .getIdUser(),
                                        videoSelected.getIdRecording(),
                                        PlayerActivity.this.getShareApp()
                                                .getTokenSession());

                        if (result.equals("true")) {
                            videoSelected.setReport(false);

                            reportBtn.setBackgroundDrawable(PlayerActivity.this
                                    .getResources().getDrawable(
                                            R.drawable.denuncia));

                            Toast.makeText(
                                    PlayerActivity.this,
                                    PlayerActivity.this.getText(
                                            R.string.remove_report_success)
                                            .toString(), Toast.LENGTH_LONG)
                                    .show();
                        } else if (result.equals("false")) {
                            showErrorMessage(
                                    null,
                                    PlayerActivity.this.getText(
                                            R.string.remove_report_fail)
                                            .toString(), false, true);
                        } else if (result.equals("desconectado")) {
                            // log out voizee
                            getShareApp().logOutVoizee();

                            // Set the app as disconnected
                            getShareApp().setDisconnected(true);

                            // Show error message telling that is disconnected
                            showErrorMessage(
                                    null,
                                    PlayerActivity.this.getText(
                                            R.string.error_session_expired)
                                            .toString(), true, true);
                        }
                    }
                } catch (JSONException e) {

                    showErrorMessage(
                            e,
                            PlayerActivity.this.getText(
                                    R.string.error_reporting_video).toString(),
                            false, true);

                }

            }
        });

        shareBtnLayout = (LinearLayout) findViewById(R.id.shareContainer);

        replayBtn = (ImageButton) findViewById(R.id.replayBtn);

        replayBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Add a new view to the video for the statistics
                addNewViewToVideo(videoSelected);

                // It has finished the video
                hasEnded = false;

                shareBtnLayout.setVisibility(View.GONE);

                // Start to play the video
                pressPlayerBtn();
                // getBarControls().startAnimation(hideBarControls);
                showTemporaryControlsPlayer(2000);

            }
        });

        twitterBtn = (ImageButton) findViewById(R.id.shareTwitterBtn);
        twitterBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                try {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.putExtra(
                            Intent.EXTRA_TEXT,
                            PlayerActivity.this
                                    .getText(R.string.share_twitter_video)
                                    + videoSelected.getUrlLink());
                    intent.setType("text/plain");
                    final PackageManager pm = PlayerActivity.this
                            .getPackageManager();
                    final List activityList = pm.queryIntentActivities(intent,
                            0);
                    int len = activityList.size();
                    for (int i = 0; i < len; i++) {
                        final ResolveInfo app = (ResolveInfo) activityList
                                .get(i);
                        if ("com.twitter.android.PostActivity"
                                .equals(app.activityInfo.name)) {
                            final ActivityInfo activity = app.activityInfo;
                            final ComponentName name = new ComponentName(
                                    activity.applicationInfo.packageName,
                                    activity.name);
                            intent = new Intent(Intent.ACTION_SEND);
                            intent.addCategory(Intent.CATEGORY_LAUNCHER);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                    | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                            intent.setComponent(name);
                            intent.putExtra(
                                    Intent.EXTRA_TEXT,
                                    PlayerActivity.this
                                            .getText(R.string.share_twitter_video)
                                            + " " + videoSelected.getUrlLink());
                            PlayerActivity.this.startActivity(intent);
                            break;
                        }
                    }
                } catch (final ActivityNotFoundException e) {
                    // Show error to the user
                    showErrorMessage(e, "no twitter native"
                            .toString(), false, false);
                }
            }
        });

        fbBtn = (ImageButton) findViewById(R.id.shareFbBtn);

        fbBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Session session = Session.getActiveSession();

                if (session != null) {

                    // Check for publish permissions
                    List<String> permissions = session.getPermissions();
                    if (!isSubsetOf(PERMISSIONS, permissions)) {
                        pendingPublishReauthorization = true;
                        Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(
                                PlayerActivity.this, PERMISSIONS);
                        session.requestNewPublishPermissions(newPermissionsRequest);
                        return;
                    }

                    Bundle postParams = new Bundle();
                    postParams.putString("name", "Voizee");
                    postParams.putString("caption", videoSelected.getTitulo());
                    postParams.putString("description", PlayerActivity.this
                            .getText(R.string.share_fb_video).toString());
                    postParams.putString("link", videoSelected.getUrlLink());
                    postParams.putString("picture",
                            "http://voizee.tv/imagenes/logo.png");

                    Request.Callback callback = new Request.Callback() {
                        public void onCompleted(Response response) {

                            FacebookRequestError error = response.getError();
                            if (error != null) {
                                Toast.makeText(
                                        PlayerActivity.this
                                                .getApplicationContext(),
                                        error.getErrorMessage(),
                                        Toast.LENGTH_SHORT).show();
                                dialogLoading.dismiss();
                            } else {
                                Toast.makeText(
                                        PlayerActivity.this
                                                .getApplicationContext(),
                                        PlayerActivity.this
                                                .getText(R.string.share_fb_video_success),
                                        Toast.LENGTH_LONG).show();
                                dialogLoading.dismiss();
                            }
                        }
                    };

                    Request request = new Request(session, "me/feed",
                            postParams, HttpMethod.POST, callback);

                    RequestAsyncTask task = new RequestAsyncTask(request);
                    task.execute();
                    fbBtn.setEnabled(false);

                    dialogLoading = new ProgressDialog(PlayerActivity.this);
                    dialogLoading.setTitle("Voizee");
                    dialogLoading.setMessage(PlayerActivity.this
                            .getText(R.string.sharing_content));
                    dialogLoading.setIndeterminate(true);
                    dialogLoading.setCancelable(true);
                    dialogLoading.show();

                } else {
                    Toast.makeText(
                            PlayerActivity.this,
                            PlayerActivity.this
                                    .getText(R.string.warning_login_fb),
                            Toast.LENGTH_LONG).show();
                }
            }
        });

        ytBtn = (ImageButton) findViewById(R.id.originalYtbBtn);

        ytBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent browse = new Intent(Intent.ACTION_VIEW, Uri
                        .parse(VoizeeConstants.URL_VIDEO_YOUTUBE
                                + videoSelected.getId()));

                startActivity(browse);
            }
        });

        try {
            // Se incluye la imagen de onda
            setImageOnda((ImageView) findViewById(R.id.imageOnda));
            getImageOnda().setImageBitmap(
                    YoutubeUtils.getBitmapFromURL(videoSelected.getUrlOnda()));

            imageOndaCapa = (ImageView) findViewById(R.id.imageCapaOnda);
            layoutImageOnda = (LinearLayout) findViewById(R.id.layoutImageOnda);


        } catch (Exception e) {

            // Show error to the user
            showErrorMessage(e, getText(R.string.error_loading_video)
                    .toString(), true, true);
        }


        if (VoizeeConstants.DEVELOPER_MODE)
            Log.d("Voizee", "I finish to load player activity");

    }

    @Override
    public void onStop() {
        super.onStop();

        // Unmute the stream music
        unmute();

        if (getPlayerStatus().getCurrentStatus() == PlayerStatus.STATUS_PLAYING) {
            // Stop the video
            getVideoView().stopPlayback();

            // Stops the media player
            getMediaPlayer().stop();

            if (threadSeekBar.isAlive())
                threadSeekBar.stop();
        }
    }

    public ImageButton getPlayBtn() {
        return playBtn;
    }

    public void setPlayBtn(ImageButton playBtn) {
        this.playBtn = playBtn;
    }

    public LinearLayout getBarControls() {
        return barControls;
    }

    public void setBarControls(LinearLayout barControls) {
        this.barControls = barControls;
    }

    private void pressPlayerBtn() {

        // Play from stop or pause
        if (getPlayerStatus().getCurrentStatus() == PlayerStatus.STATUS_STOPPED) {

            if (imageOndaCapa != null)
                //The width of the wave is  set to 0
                imageOndaCapa.getLayoutParams().width = 0;

            syncAudioVideo();

            // getVideoView().seekTo(0);
            vSeekBarProgress.setProgress(0);

            //
            counterVideo.setText(String.format(" %02d:%02d ", 0, 0));

            // Set the player status to play
            getPlayerStatus().setCurrentStatus(PlayerStatus.STATUS_PLAYING);

            if (!threadSeekBar.isAlive())
                // Run the thread to move the seekbar
                threadSeekBar.run();

            // Set the background to play
            playBtn.setBackgroundDrawable(this.getResources().getDrawable(
                    R.drawable.pause_btn));

        } else if (getPlayerStatus().getCurrentStatus() == PlayerStatus.STATUS_PLAYING) {

            // Pause the video and media player
            getVideoView().pause();
            getMediaPlayer().pause();

            // Set the background to play
            playBtn.setBackgroundDrawable(this.getResources().getDrawable(
                    R.drawable.play_btn));

            // Set the player status to play
            getPlayerStatus().setCurrentStatus(PlayerStatus.STATUS_PAUSED);
        } else if (getPlayerStatus().getCurrentStatus() == PlayerStatus.STATUS_PAUSED)
        // Pause from playing
        {

            // Resume the video view in another way
            //getVideoView().seekTo(getMediaPlayer().getCurrentPosition());
            getVideoView().start();


            final Handler handlerAudio = new Handler();
            handlerAudio.postDelayed(new Runnable() {
                @Override
                public void run() {


                    // Start the audio player
                    getMediaPlayer().start();
                    getMediaPlayer().seekTo(getVideoView().getCurrentPosition());

                    Log.d("Voizee", "Audio Position  "
                            + getMediaPlayer().getCurrentPosition() + "Video Position " + getVideoView().getCurrentPosition());

                }

            }, 1000);


            // Set the player status to play
            getPlayerStatus().setCurrentStatus(PlayerStatus.STATUS_PLAYING);

            if (!threadSeekBar.isAlive())
                // Run the thread to move the seekbar
                threadSeekBar.run();

            // Set the background to play
            playBtn.setBackgroundDrawable(this.getResources().getDrawable(
                    R.drawable.pause_btn));
        }

    }

    private void stopToPlay() {

        // if (getVideoView().isPlaying()) {
        // Empieza el video
        //getVideoView().seekTo(0);
        if (getPlayerStatus().getCurrentStatus() != PlayerStatus.STATUS_STOPPED)
            getVideoView().stopPlayback();

        // Reset the videoview
        initVideo(videoSelected);

        // Stop thread
        if (threadSeekBar != null && threadSeekBar.isAlive())
            threadSeekBar.stop();

        getMediaPlayer().seekTo(0);
        if (getPlayerStatus().getCurrentStatus() != PlayerStatus.STATUS_STOPPED)
            getMediaPlayer().stop();

        // Set the player status to play
        getPlayerStatus().setCurrentStatus(PlayerStatus.STATUS_STOPPED);

        // Set the background to play
        playBtn.setBackgroundDrawable(this.getResources().getDrawable(
                R.drawable.play_btn));

        // SE prepara el media player
        try {

            getMediaPlayer().prepareAsync();
        } catch (Exception e1) {

            showErrorMessage(e1, getText(R.string.error_loading_video)
                    .toString(), true, true);
        }

    }

    private void showTemporaryControlsPlayer(int seconds) {

        // if (getPlayerStatus().getCurrentStatus() ==
        // PlayerStatus.STATUS_PLAYING) {

        getBarControls().setVisibility(View.VISIBLE);
        vSeekBarProgress.setVisibility(View.VISIBLE);
        vSeekBarVol.setVisibility(View.VISIBLE);
        ytBtn.setVisibility(View.VISIBLE);

        dubbItBtn.setVisibility(View.GONE);
        // if (getShareApp().getIdUser() != null
        // && !getShareApp().getIdUser().equals(""))
        // show user button to report, bookmark.....
        userBtnsLayout.setVisibility(View.VISIBLE);

        counterVideo.setVisibility(View.VISIBLE);
        durationVideo.setVisibility(View.VISIBLE);
        imageOnda.setVisibility(View.VISIBLE);
        layoutImageOnda.setVisibility(View.VISIBLE);
        imageOndaCapa.setVisibility(View.VISIBLE);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                getBarControls().setVisibility(View.INVISIBLE);
                vSeekBarProgress.setVisibility(View.INVISIBLE);
                vSeekBarVol.setVisibility(View.INVISIBLE);
                dubbItBtn.setVisibility(View.GONE);
                userBtnsLayout.setVisibility(View.INVISIBLE);
                counterVideo.setVisibility(View.INVISIBLE);
                durationVideo.setVisibility(View.INVISIBLE);
                ytBtn.setVisibility(View.INVISIBLE);
                imageOnda.setVisibility(View.INVISIBLE);
                layoutImageOnda.setVisibility(View.INVISIBLE);
                imageOndaCapa.setVisibility(View.INVISIBLE);

            }
        }, seconds);
        // }
    }

    private void syncAudioVideo() {
        //Show a progress dialog
        showDialog(getText(R.string.sync_video).toString(), false);

        //Start the video
        getVideoView().start();

        //Waiting to fill the buffer
        final Handler handlerAudio = new Handler();
        final Date initialTime = new Date();
        handlerAudio.postDelayed(new Runnable() {
            @Override
            public void run() {


                //Dismiss the dialog
                setHasFinished(true);

                //Start the audio again
                getMediaPlayer().start();

                getMediaPlayer().seekTo(getVideoView().getCurrentPosition());

                //Enable again the play button
                getPlayBtn().setEnabled(true);

                Log.d("Voizee", "After start again Audio Position  "
                        + getMediaPlayer().getCurrentPosition() + "Video Position " + getVideoView().getCurrentPosition());

            }
        }, 1000);
    }

    private Handler handlerProgBar = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            if (VoizeeConstants.DEVELOPER_MODE)
                Log.d("Voizee", "Cambio la seekbar");

            if (msg != null && msg.getData() != null) {

                int progress = msg.getData().getInt("progress");

                // if (countProgress % 3 == 0) {
                vSeekBarProgress.setProgress(progress);
                // countProgress = 1;
                // } else {
                // countProgress++;
                // }

                // Set the video counter
                int currentSeconds = progress / 1000;

                int displaySeconds = currentSeconds % 60;
                int displayMins = currentSeconds / 60;

                counterVideo.setText(String.format(" %02d:%02d ", displayMins,
                        displaySeconds));

                int totalDurationSeconds = (audioDuration / 1000);

                float porcentage = (currentSeconds / (float) totalDurationSeconds);

                if (imageOndaCapa != null && layoutImageOnda != null) {
                    imageOndaCapa.getLayoutParams().width = (int) (layoutImageOnda.getMeasuredWidth()
                            * porcentage);

                    if (VoizeeConstants.DEVELOPER_MODE)
                        Log.d("Voizee", "Width capa "
                                + imageOndaCapa.getLayoutParams().width
                                + " currentSeconds: " + currentSeconds
                                + " audioDuration: " + totalDurationSeconds
                                + " formula: " + (533 * porcentage));

                }

            }

        }
    };

    public PlayerStatus getPlayerStatus() {
        return playerStatus;
    }

    public void setPlayerStatus(PlayerStatus playerStatus) {
        this.playerStatus = playerStatus;
    }

    public Handler getHandlerSeekBar() {
        return handlerSeekBar;
    }

    public void setHandlerSeekBar(Handler handlerSeekBar) {
        this.handlerSeekBar = handlerSeekBar;
    }

    public LinearLayout getUserBtnsLayout() {
        return userBtnsLayout;
    }

    public void setUserBtnsLayout(LinearLayout userBtnsLayout) {
        this.userBtnsLayout = userBtnsLayout;
    }

    public ImageButton getLikeBtn() {
        return likeBtn;
    }

    public void setLikeBtn(ImageButton likeBtn) {
        this.likeBtn = likeBtn;
    }

    public ImageButton getBookMarkBtn() {
        return bookMarkBtn;
    }

    public void setBookMarkBtn(ImageButton bookMarkBtn) {
        this.bookMarkBtn = bookMarkBtn;
    }

    public ImageButton getReportBtn() {
        return reportBtn;
    }

    public void setReportBtn(ImageButton reportBtn) {
        this.reportBtn = reportBtn;
    }

    public ImageButton getDisLikeBtn() {
        return disLikeBtn;
    }

    public void setDisLikeBtn(ImageButton disLikeBtn) {
        this.disLikeBtn = disLikeBtn;
    }

    public ImageView getImageOnda() {
        return imageOnda;
    }

    public void setImageOnda(ImageView imageOnda) {
        this.imageOnda = imageOnda;
    }

}
