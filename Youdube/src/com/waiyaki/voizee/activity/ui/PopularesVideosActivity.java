/**
 *
 */
package com.waiyaki.voizee.activity.ui;

import java.util.LinkedList;

import org.json.JSONException;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;

import com.markupartist.android.widget.PullToRefreshListView;
import com.markupartist.android.widget.PullToRefreshListView.OnRefreshListener;
import com.waiyaki.voizee.dao.model.VoizeeVideo;
import com.waiyaki.voizee.main.R;
import com.waiyaki.voizee.manager.VoizeeApiManager;
import com.waiyaki.voizee.manager.VoizeeApiManagerImpl;
import com.waiyaki.voizee.util.VoizeeConstants;

/**
 * @author Francisco Javier Morant Moya
 */
public class PopularesVideosActivity extends ListBaseFragment {

    private EditText locText = null;
    private LinkedList<String> itemsListView;
    private VoizeeApiManager managerVideoPlayer = new VoizeeApiManagerImpl();
    private PullToRefreshListView listView;

    public PopularesVideosActivity() {
        // Loading the videos from server
        if (VoizeeConstants.DEVELOPER_MODE)
            Log.d("Voizee", "Create LastVideosActivity");

        try {
            setListVideos(getManagerVideoPlayer().getPopularsVideos(null));
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public PopularesVideosActivity(String idUser) {
        if (VoizeeConstants.DEVELOPER_MODE)
            Log.d("Voizee", "Create PopularesVideosActivity");

        // Loading the videos from Server

        try {

            listVideos = managerVideoPlayer.getPopularsVideos(idUser);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        title = getText(R.string.label_most_populars_view).toString();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (VoizeeConstants.DEVELOPER_MODE)
            Log.d("Voizee", "Create PopularesVideosActivity View");

        View view = inflater
                .inflate(R.layout.popularesvideos, container, false);

        itemsListView = new LinkedList<String>();

        if (view != null) {

            // Add listview
            setListView((PullToRefreshListView) view.findViewById(R.id.idListView));

            listView.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapter, View view,
                                        int position, long arg) {
                    VoizeeVideo video = listVideos.get(position - 1);

                    Intent intentVideoPlayer = new Intent(
                            PopularesVideosActivity.this.getActivity(),
                            PlayerActivity.class);

                    ((BaseFragmentActivity) PopularesVideosActivity.this
                            .getActivity()).getShareApp().setSelectedVideo(video);

                    PopularesVideosActivity.this.startActivity(intentVideoPlayer);

                }
            });

            // Set a listener to be invoked when the list should be refreshed.
            listView.setOnRefreshListener(new OnRefreshListener() {
                @Override
                public void onRefresh() {
                    // Do work to refresh the list here.
                    new GetDataTask().execute();
                }
            });

            if (listVideos != null) {
                for (VoizeeVideo video : listVideos)
                    itemsListView.add(video.getTitulo());

                String[] array = new String[itemsListView.size()];
                itemsListView.toArray(array);
                setListItemsAdapter(array);
            }
        }

        return view;
    }

    private class GetDataTask extends AsyncTask<Void, Void, String[]> {

        @Override
        protected String[] doInBackground(Void... params) {
            // Simulates a background job.

            try {
                listVideos = managerVideoPlayer
                        .getPopularsVideos(((BaseFragmentActivity) PopularesVideosActivity.this
                                .getActivity()).getShareApp().getIdUser());
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            if (listVideos != null)
                for (VoizeeVideo video : listVideos)
                    itemsListView.add(video.getTitulo());

            String[] array = new String[itemsListView.size()];
            itemsListView.toArray(array);
            return array;
        }

        @Override
        protected void onPostExecute(String[] result) {
            itemsListView.addFirst("Added after refresh...");

            // Call onRefreshComplete when the list has been refreshed.
            ((PullToRefreshListView) getListView()).onRefreshComplete();

            super.onPostExecute(result);
        }
    }

    private void setListItemsAdapter(String[] items2) {
        listView.setAdapter(new MyCustomAdapter(PopularesVideosActivity.this
                .getActivity(), R.layout.row, items2));
    }

    public EditText getLocText() {
        return locText;
    }

    public void setLocText(EditText locText) {
        this.locText = locText;
    }

    public LinkedList<String> getItemsListView() {
        return itemsListView;
    }

    public void setItemsListView(LinkedList<String> itemsListView) {
        this.itemsListView = itemsListView;
    }

    public VoizeeApiManager getManagerVideoPlayer() {
        return managerVideoPlayer;
    }

    public void setManagerVideoPlayer(VoizeeApiManager managerVideoPlayer) {
        this.managerVideoPlayer = managerVideoPlayer;
    }

    public PullToRefreshListView getListView() {
        return listView;
    }

    public void setListView(PullToRefreshListView listView) {
        this.listView = listView;
    }

    @Override
    public String getTitleFragment() {
        return title;
    }
}
