/**
 *
 */
package com.waiyaki.voizee.activity.ui;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.SeekBar;
import android.widget.Toast;

import com.waiyaki.voizee.dao.model.VoizeeVideo;
import com.waiyaki.voizee.main.R;
import com.waiyaki.voizee.util.AudioRecorder;
import com.waiyaki.voizee.util.PlayerVideoView;
import com.waiyaki.voizee.util.VoizeeConstants;

/**
 * @author Francisco Javier Morant Moya
 */
public class RecorderActivity extends BaseActivity {

    private VoizeeVideo voizeeVideoRequest;

    // Buttons recorder
    private ImageButton recordBtn;
    private ImageButton playBtn;
    private ImageButton stopBtn;
    // private ImageButton pauseBtn;
    private LinearLayout barControls;
    private SeekBar vSeekBar;
    private Animation hideBarControls;
    private Animation showBarControls;

    private boolean isPaused = false;
    private boolean isRecording = false;
    private boolean isFinishedVideo = false;

    // Image view count down
    private ImageView imageCountDown;
    private int countDown = 4;
    private Handler handlerCountDown;
    private boolean isError = false;
    private boolean isAudioOriginalOn = false;

    public void onCreate(Bundle savedInstanceState) {

        // Call to the super video recorder
        super.onCreate(savedInstanceState, R.layout.videorecorder);


        if (getIntent() != null && getIntent().getExtras() != null) {

            // Get the serialized object from the previous screen
            Bundle bndObj = (Bundle) getIntent().getExtras().get("urlVideoYoutube");
            voizeeVideoRequest = (VoizeeVideo) bndObj
                    .getSerializable("serializableVideoYotube");
        } else
            showErrorMessage(null, getText(R.string.error_loading_video)
                    .toString(), true, true);

        // Set the url of the youtube video
        try {
            voizeeVideoRequest.setUrlVideo(getShareApp()
                    .getManagerVideoPlayer()
                    .getVideo(voizeeVideoRequest.getId()).getUrlVideo());
        } catch (Exception e) {

            showErrorMessage(e, getText(R.string.error_loading_video)
                    .toString(), true, true);
        }

        // Set the path of the audio
        setAudioRecorder(new AudioRecorder(
                "/data/data/com.waiyaki.voizee.main/"
                        + getSdf().format(new Date()) + ".mp4"));

        // Set the url in the object to request
        voizeeVideoRequest.setUrlAudio(getAudioRecorder().getPath());

        // Set up the video view
        initVideo(voizeeVideoRequest);

        // Check if there has been any problem even after the initview
        if (getVideoView() == null)
            showErrorMessage(null, getText(R.string.error_loading_video)
                    .toString(), true, true);

        if (getVideoView() != null) {

            // When the video finish
            getVideoView().setOnCompletionListener(
                    new MediaPlayer.OnCompletionListener() {

                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            // Set to finished the boolean variable
                            isFinishedVideo = true;

                            if (!isRecording)
                                stopToPlay();
                            else
                                stopToRecord();
                        }

                    });

            // // Pause the video or resume
            getVideoView().setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View arg0, MotionEvent arg1) {
                    getBarControls().setVisibility(View.VISIBLE);
                    vSeekBar.setVisibility(View.VISIBLE);

                    ytBtn.setVisibility(View.VISIBLE);

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            getBarControls().setVisibility(View.INVISIBLE);
                            vSeekBar.setVisibility(View.INVISIBLE);
                            ytBtn.setVisibility(View.INVISIBLE);
                        }
                    }, 5000);
                    return false;
                }

            });

            getVideoView().setOnErrorListener(
                    new MediaPlayer.OnErrorListener() {

                        @Override
                        public boolean onError(MediaPlayer mp, int what,
                                               int extra) {
                            isFinishedVideo = true;

                            showErrorMessage(null,
                                    getText(R.string.error_loading_video)
                                            .toString(), true, true);

                            isError = true;

                            if (getAudioRecorder().isRecording())
                                stopToRecord();
                            else
                                stopToPlay();

                            return true;
                        }
                    });
        }

        // Set the bar controls
        setBarControls((LinearLayout) findViewById(R.id.barControls));

        // Logica boton de record
        recordBtn = (ImageButton) findViewById(R.id.recordBtn);

        recordBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                // Dialogo para mostrar si se quiere el audio original o no.

                if (!getAudioRecorder().isRecording()) {

                    final AlertDialog.Builder dialogChoose = new AlertDialog.Builder(
                            RecorderActivity.this);
                    dialogChoose.setTitle("Voizee");
                    dialogChoose
                            .setMessage(getText(R.string.remove_audio_recorder));

                    // Positive answer
                    dialogChoose.setNegativeButton(
                            getText(R.string.remove_audio_recorder_no),
                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface arg0,
                                                    int arg1) {

                                    AlertDialog.Builder dialog = new AlertDialog.Builder(
                                            RecorderActivity.this);
                                    dialog.setTitle("Voizee");
                                    dialog.setMessage(getText(R.string.recorder_suggestion));
                                    dialog.setPositiveButton(
                                            "OK",
                                            new DialogInterface.OnClickListener() {

                                                @Override
                                                public void onClick(
                                                        DialogInterface arg0,
                                                        int arg1) {
                                                    // Unmute the audio of the
                                                    // stream music
                                                    unmute();

                                                    // Set the current volumen
                                                    // after the unmute
                                                    vSeekBar.setProgress(getAudioManager()
                                                            .getStreamVolume(
                                                                    AudioManager.STREAM_MUSIC));

                                                    // Make visible the
                                                    // countdown
                                                    imageCountDown
                                                            .setVisibility(View.VISIBLE);

                                                    // First count down
                                                    imageCountDown
                                                            .setImageDrawable(getBaseContext()
                                                                    .getResources()
                                                                    .getDrawable(
                                                                            R.drawable.cuenta_atras_32x));
                                                    // At 5 secs after remove
                                                    // the onair
                                                    handlerCountDown
                                                            .postDelayed(
                                                                    runCountDown,
                                                                    1000);

                                                    // Hides the bar control
                                                    // createAnimationBarControl();
                                                    getBarControls()
                                                            .setVisibility(
                                                                    View.INVISIBLE);
                                                    vSeekBar.setVisibility(View.INVISIBLE);
                                                    ytBtn.setVisibility(View.INVISIBLE);

                                                    // Set original on
                                                    isAudioOriginalOn = true;
                                                }
                                            });
                                    dialog.show();
                                }
                            });

                    // Set the button for configure network as a provider
                    dialogChoose.setPositiveButton(
                            getText(R.string.remove_audio_recorder_si),
                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface arg0,
                                                    int arg1) {

                                    AlertDialog.Builder dialog = new AlertDialog.Builder(
                                            RecorderActivity.this);
                                    dialog.setTitle("Voizee");
                                    dialog.setMessage(getText(R.string.recorder_suggestion));
                                    dialog.setPositiveButton(
                                            "OK",
                                            new DialogInterface.OnClickListener() {

                                                @Override
                                                public void onClick(
                                                        DialogInterface arg0,
                                                        int arg1) {
                                                    // Mute the original audio
                                                    mute();

                                                    // Seek progress
                                                    vSeekBar.setProgress(getAudioManager()
                                                            .getStreamVolume(
                                                                    AudioManager.STREAM_MUSIC));
                                                    // Set the onair sign
                                                    // getVideoView().setBackground(
                                                    // getBaseContext().getResources()
                                                    // .getDrawable(
                                                    // R.drawable.onair));

                                                    // SE hace visible el
                                                    // countdown
                                                    imageCountDown
                                                            .setVisibility(View.VISIBLE);

                                                    // Se inicializa
                                                    imageCountDown
                                                            .setImageDrawable(getBaseContext()
                                                                    .getResources()
                                                                    .getDrawable(
                                                                            R.drawable.cuenta_atras_32x));
                                                    // At 5 secs after remove
                                                    // the onair
                                                    handlerCountDown
                                                            .postDelayed(
                                                                    runCountDown,
                                                                    1000);
                                                    // createAnimationBarControl();
                                                    // getBarControls().startAnimation(
                                                    // hideBarControls);
                                                    getBarControls()
                                                            .setVisibility(
                                                                    View.INVISIBLE);
                                                    vSeekBar.setVisibility(View.INVISIBLE);
                                                    ytBtn.setVisibility(View.INVISIBLE);

                                                    // Set original on
                                                    isAudioOriginalOn = false;
                                                }
                                            });

                                    dialog.show();

                                }
                            });

                    // Show the dialog to choose the provider
                    dialogChoose.show();
                } else {
                    // Start to record
                    try {
                        startToRecord();
                    } catch (IOException e) {

                        showErrorMessage(e,
                                getText(R.string.error_recording_video)
                                        .toString(), true, true);
                    }
                }
            }
        });

        // Logica del play button
        playBtn = (ImageButton) findViewById(R.id.playBtn);

        playBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                startToPlay();
                // createAnimationBarControl();
                // getBarControls().startAnimation(hideBarControls);
                getBarControls().setVisibility(View.INVISIBLE);
                vSeekBar.setVisibility(View.INVISIBLE);
                ytBtn.setVisibility(View.INVISIBLE);
            }
        });

        stopBtn = (ImageButton) findViewById(R.id.stopBtn);
        stopBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // createAnimationBarControl();
                getBarControls().setVisibility(View.VISIBLE);
                vSeekBar.setVisibility(View.VISIBLE);
                ytBtn.setVisibility(View.VISIBLE);
                if (getAudioRecorder().isRecording())
                    stopToRecord();
                else
                    stopToPlay();

            }
        });

        // pauseBtn = (ImageButton) findViewById(R.id.pauseBtn);
        // pauseBtn.setOnClickListener(new View.OnClickListener() {
        //
        // @Override
        // public void onClick(View v) {
        // // createAnimationBarControl();
        // // getBarControls().startAnimation(showBarControls);
        // getBarControls().setVisibility(View.VISIBLE);
        // vSeekBar.setVisibility(View.VISIBLE);
        // // TODO Auto-generated method stub
        // if (getAudioRecorder().isRecording())
        // pauseToRecord();
        // else
        // pauseToPlayer();
        // }
        // });

        // Create the audio manager
        setAudioManager((AudioManager) getSystemService(Context.AUDIO_SERVICE));

        int maxVolume = getAudioManager().getStreamMaxVolume(
                AudioManager.STREAM_MUSIC);
//        int curVolume = getAudioManager().getStreamVolume(
//                AudioManager.STREAM_MUSIC);

        vSeekBar = (SeekBar) findViewById(R.id.volumenRecorder);
        vSeekBar.setMax(maxVolume);
        vSeekBar.setProgress(maxVolume / 2);
        vSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
                // TODO Auto-generated method stub
                getAudioManager().setStreamVolume(AudioManager.STREAM_MUSIC,
                        arg1, 0);
            }

            @Override
            public void onStartTrackingTouch(SeekBar arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onStopTrackingTouch(SeekBar arg0) {
                // TODO Auto-generated method stub

            }
        });

        // Get the image view countdown element
        imageCountDown = (ImageView) findViewById(R.id.imgCountDown);
        handlerCountDown = new Handler();

        ytBtn = (ImageButton) findViewById(R.id.originalYtbBtn);

        ytBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent browse = new Intent(Intent.ACTION_VIEW, Uri
                        .parse(VoizeeConstants.URL_VIDEO_YOUTUBE
                                + voizeeVideoRequest.getId()));

                startActivity(browse);
            }
        });
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onPostCreate(savedInstanceState);

        if (getVideoView() == null)
            showErrorMessage(null, getText(R.string.error_loading_video)
                    .toString(), true, true);
    }

    private void startToRecord() throws IOException {

        isRecording = true;
        isError = false;
        isFinishedVideo = false;

        // Set the onair sign
        // getVideoView().setBackground(
        // getBaseContext().getResources().getDrawable(R.drawable.onair));

        // If it has not been paused
//        if (!getVideoView().isPlaying() && !isPaused) {
//            initVideo(voizeeVideoRequest);
//            getVideoView().start();
//        } else {
            // After pauseing the video
            //getVideoView().seekTo(getVideoView().getCurrentPosition());
           getVideoView().start();
        //}


        final Handler handlerAudio = new Handler();
        handlerAudio.postDelayed(new Runnable() {
            @Override
            public void run() {


                try {
                    // If there is already a record append it
                    if (getAudioRecorder().getPaths().size() > 0)

                        getAudioRecorder().startAppend();
                    else
                        getAudioRecorder().start();

                } catch (Exception e) {

                    showErrorMessage(e,
                            getText(R.string.error_recording_video)
                                    .toString(), true, true);
                }


            }
        }, 1000);

        // recordBtn.setImageDrawable(getBaseContext().getResources().getDrawable(
        // R.drawable.recon));
        recordBtn.setVisibility(View.GONE);

        // stopBtn.setImageDrawable(getBaseContext().getResources().getDrawable(
        // R.drawable.stop));
        stopBtn.setVisibility(View.VISIBLE);

        // Se pone el video activado
        // playBtn.setImageDrawable(getBaseContext().getResources().getDrawable(
        // R.drawable.playon));
        playBtn.setVisibility(View.GONE);

        // Se pone el video activado
        // pauseBtn.setImageDrawable(getBaseContext().getResources().getDrawable(
        // R.drawable.pause));
        // pauseBtn.setEnabled(true);
        isPaused = false;

    }

    private Runnable runCountDown = new Runnable() {
        @Override
        public void run() {
            if (countDown > -1) {
                if (countDown == 3)
                    imageCountDown.setImageDrawable(getBaseContext()
                            .getResources().getDrawable(
                                    R.drawable.cuenta_atras_32x));
                if (countDown == 2)
                    imageCountDown.setImageDrawable(getBaseContext()
                            .getResources().getDrawable(
                                    R.drawable.cuenta_atras_22x));
                if (countDown == 1)
                    imageCountDown.setImageDrawable(getBaseContext()
                            .getResources().getDrawable(
                                    R.drawable.cuenta_atras_12x));
                if (countDown == 0)
                    imageCountDown.setImageDrawable(getBaseContext()
                            .getResources().getDrawable(
                                    R.drawable.cuenta_atras_onair2x));
                countDown--;
                handlerCountDown.postDelayed(runCountDown, 1000);
            } else {

                // Start to record
                try {
                    startToRecord();
                } catch (IOException e) {
                    showErrorMessage(e, getText(R.string.error_recording_video)
                            .toString(), true, true);
                }
                imageCountDown.setVisibility(View.INVISIBLE);
                getVideoView().setBackgroundColor(
                        getBaseContext().getResources().getColor(
                                R.color.transparent));
            }
        }
    };

    private void stopToRecord() {

        isPaused = false;
        isRecording = false;
        countDown = 4;

        // SEt off air
        // Set the onair sign
        // getVideoView().setBackground(
        // getBaseContext().getResources().getDrawable(
        // R.drawable.onair_off));

        // Pause because the image it has to be stopped
        this.pauseToRecord();

        if (!isFinishedVideo) {

            final AlertDialog.Builder dialogStopRecording = new AlertDialog.Builder(
                    RecorderActivity.this);
            dialogStopRecording.setTitle("Voizee");
            dialogStopRecording.setMessage(getText(R.string.recorder_stop));
            dialogStopRecording.setPositiveButton(
                    getText(R.string.recorder_stop_si),
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {

                            // Once it is stopped definitely the video when
                            // compose
                            // the audio
                            composeAudio();

                            if (!isError) {

                                final AlertDialog.Builder dialogSuggestRecording = new AlertDialog.Builder(
                                        RecorderActivity.this);
                                dialogSuggestRecording.setTitle("Voizee");
                                dialogSuggestRecording
                                        .setMessage(getText(R.string.recorder_suggestion_watch));

                                dialogSuggestRecording
                                        .setPositiveButton(
                                                getText(R.string.recorder_suggestion_watch_si),
                                                new DialogInterface.OnClickListener() {

                                                    @Override
                                                    public void onClick(
                                                            DialogInterface arg0,
                                                            int arg1) {
                                                        // Go to the player to
                                                        // play a
                                                        // local
                                                        // video

                                                        startWatchingRecordedVideo();
                                                    }
                                                });

                                dialogSuggestRecording
                                        .setNegativeButton(
                                                getText(R.string.recorder_suggestion_watch_no),
                                                new DialogInterface.OnClickListener() {

                                                    @Override
                                                    public void onClick(
                                                            DialogInterface arg0,
                                                            int arg1) {

                                                        stopBtn.setVisibility(View.GONE);

                                                        // Se pone el video
                                                        // activado

                                                        playBtn.setVisibility(View.VISIBLE);

                                                        recordBtn
                                                                .setVisibility(View.VISIBLE);

                                                        // Set the visible to
                                                        // the able
                                                        // to see something
                                                        barControls
                                                                .setVisibility(View.VISIBLE);

                                                        // Show share dialog
                                                        Intent intentShareDialog = new Intent(
                                                                RecorderActivity.this,
                                                                UploadDialogActivity.class);

                                                        Bundle bundleVideo = new Bundle();

                                                        bundleVideo
                                                                .putSerializable(
                                                                        "videoToUpload",
                                                                        (Serializable) voizeeVideoRequest);

                                                        intentShareDialog
                                                                .putExtra(
                                                                        "bundleToUploadVideo",
                                                                        bundleVideo);

                                                        startActivityForResult(
                                                                intentShareDialog,
                                                                VoizeeConstants.START_SHARE_VIDEO_UPLOADED);
                                                    }
                                                });

                                dialogSuggestRecording.show();

                            }
                        }
                    });

            dialogStopRecording.setNegativeButton(
                    getText(R.string.recorder_stop_no),
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            try {
                                RecorderActivity.this.startToRecord();
                            } catch (IOException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();

                                Toast.makeText(
                                        RecorderActivity.this,
                                        getText(R.string.error_recording_video),
                                        Toast.LENGTH_LONG).show();
                            }
                        }
                    });

            // Show the dialog to choose the provider
            dialogStopRecording.show();
        } else {

            // Once it is stopped definitely the video when
            // compose
            // the audio
            composeAudio();

            if (!isError) {

                final AlertDialog.Builder dialogSuggestRecording = new AlertDialog.Builder(
                        RecorderActivity.this);
                dialogSuggestRecording.setTitle("Voizee");
                dialogSuggestRecording
                        .setMessage(getText(R.string.recorder_suggestion_watch));

                dialogSuggestRecording.setPositiveButton(
                        getText(R.string.recorder_suggestion_watch_si),
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                // Go to the player to play a
                                // local
                                // video

                                startWatchingRecordedVideo();
                            }
                        });

                dialogSuggestRecording.setNegativeButton(
                        getText(R.string.recorder_suggestion_watch_no),
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {

                                stopBtn.setVisibility(View.GONE);

                                // Se pone el video activado
                                playBtn.setVisibility(View.VISIBLE);

                                recordBtn.setVisibility(View.VISIBLE);

                                // Set the visible to the able
                                // to see something
                                barControls.setVisibility(View.VISIBLE);

                                // Show share dialog
                                Intent intentShareDialog = new Intent(
                                        RecorderActivity.this,
                                        UploadDialogActivity.class);

                                Bundle bundleVideo = new Bundle();

                                bundleVideo.putSerializable("videoToUpload",
                                        (Serializable) voizeeVideoRequest);

                                intentShareDialog.putExtra(
                                        "bundleToUploadVideo", bundleVideo);

                                RecorderActivity.this
                                        .startActivityForResult(
                                                intentShareDialog,
                                                VoizeeConstants.START_SHARE_VIDEO_UPLOADED);
                            }
                        });

                dialogSuggestRecording.show();

            }
        }
    }

    private void startToPlay() {

        isRecording = false;

//        if (!getVideoView().isPlaying() && !isPaused) {
//            initVideo(voizeeVideoRequest);
//            getVideoView().start();
//        } else {
            //getVideoView().seekTo(getVideoView().getCurrentPosition());
            getVideoView().start();
        //}

        // Se unmutea el video
        unmute();

        // Se pone el video activado
        playBtn.setVisibility(View.GONE);

        recordBtn.setVisibility(View.GONE);

        stopBtn.setVisibility(View.VISIBLE);

        isPaused = false;
    }

    private void stopToPlay() {

        isRecording = false;

        if (getVideoView().isPlaying()) {
            // Empieza el video
            getVideoView().pause();
            getVideoView().seekTo(0);
        }

        // Se pone el video activado
        // playBtn.setImageDrawable(getBaseContext().getResources().getDrawable(
        // R.drawable.play));
        playBtn.setVisibility(View.VISIBLE);

        // recordBtn.setImageDrawable(getBaseContext().getResources().getDrawable(
        // R.drawable.rec));
        recordBtn.setVisibility(View.VISIBLE);

        // stopBtn.setImageDrawable(getBaseContext().getResources().getDrawable(
        // R.drawable.stopon));
        stopBtn.setVisibility(View.GONE);

        // Se pone el video activado
        // pauseBtn.setImageDrawable(getBaseContext().getResources().getDrawable(
        // R.drawable.pauseon));
        // pauseBtn.setEnabled(false);

        isPaused = false;
    }

    private void pauseToRecord() {

        isRecording = false;

        // Se pone el video activado
        // pauseBtn.setImageDrawable(getBaseContext().getResources().getDrawable(
        // R.drawable.pauseon));
        // pauseBtn.setEnabled(false);

        if (getAudioRecorder().isRecording()) {
            // Se pone el video activado
            // playBtn.setImageDrawable(getBaseContext().getResources()
            // .getDrawable(R.drawable.playon));
            playBtn.setVisibility(View.GONE);
        } else {
            // Se pone el video activado
            // playBtn.setImageDrawable(getBaseContext().getResources()
            // .getDrawable(R.drawable.play));
            playBtn.setVisibility(View.VISIBLE);
        }

        if (getAudioRecorder().isRecording()) {
            // recordBtn.setImageDrawable(getBaseContext().getResources()
            // .getDrawable(R.drawable.rec));
            recordBtn.setVisibility(View.VISIBLE);
        } else {
            // recordBtn.setImageDrawable(getBaseContext().getResources()
            // .getDrawable(R.drawable.recon));
            recordBtn.setVisibility(View.GONE);
        }

        // stopBtn.setImageDrawable(getBaseContext().getResources().getDrawable(
        // R.drawable.stop));
        stopBtn.setVisibility(View.VISIBLE);

        getVideoView().pause();
        getVideoView().setBackgroundColor(
                getBaseContext().getResources().getColor(R.color.transparent));
        if (getAudioRecorder().isRecording()) {
            getAudioRecorder().pause();
        }

        isPaused = true;
    }

    private void composeAudio() {
        // Se muestra el dialogo de envio de
        // datos
        // Show the initial dialog
        // Show dialog to choice the correct
        // route

        // Stops defenitely the recorder
        // if (getAudioRecorder().isRecording())

        boolean result = getAudioRecorder().createMP4File();

        File file = new File(voizeeVideoRequest.getUrlAudio());

        if (!result || !file.exists())

            showErrorMessage(null, getText(R.string.error_recording_video)
                    .toString(), true, true);
        else {

            // Stops the video
            getVideoView().stopPlayback();

            // Dependings on if the headset are plugged or not we wend one think
            // or another
            if (getAudioManager().isWiredHeadsetOn() && isAudioOriginalOn) {
                // Create a new dialog to ask the user for details
                // Call to create the dialog activity
                voizeeVideoRequest.setVolume(getAudioManager().getStreamVolume(
                        AudioManager.STREAM_MUSIC));
            } else
                voizeeVideoRequest.setVolume(0);

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == VoizeeConstants.START_SUGGESTED_VIDEO_RECORDED) {

            // Reset the audio composer
            getAudioRecorder().clearTempFiles();

            stopBtn.setVisibility(View.GONE);

            // Se pone el video activado
            playBtn.setVisibility(View.VISIBLE);
            recordBtn.setVisibility(View.VISIBLE);

            // Set the visible to the able
            // to see something
            barControls.setVisibility(View.VISIBLE);

            // Show share dialog
            Intent intentShareDialog = new Intent(RecorderActivity.this,
                    UploadDialogActivity.class);

            Bundle bundleVideo = new Bundle();

            bundleVideo.putSerializable("videoToUpload",
                    (Serializable) voizeeVideoRequest);

            intentShareDialog.putExtra("bundleToUploadVideo", bundleVideo);

            RecorderActivity.this.startActivityForResult(intentShareDialog,
                    VoizeeConstants.START_SHARE_VIDEO_UPLOADED);

        }

        if (requestCode == VoizeeConstants.START_SHARE_VIDEO_UPLOADED) {

            Toast.makeText(this, getText(R.string.another_record),
                    Toast.LENGTH_LONG).show();
            finish();

        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {

            if (getAudioRecorder().isRecording()) {
                this.pauseToRecord();

                final AlertDialog.Builder removeRecordingDialog = new AlertDialog.Builder(
                        RecorderActivity.this);
                removeRecordingDialog.setTitle("Voizee");
                removeRecordingDialog
                        .setMessage(getText(R.string.recorder_suggestion_exit));
                removeRecordingDialog.setPositiveButton(
                        getText(R.string.recorder_stop_si),
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {

                                RecorderActivity.this.finish();
                            }
                        });

                removeRecordingDialog.setNegativeButton(
                        getText(R.string.recorder_stop_no),
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {

                                if (isRecording) {
                                    try {
                                        RecorderActivity.this.startToRecord();
                                    } catch (IOException e) {

                                        showErrorMessage(
                                                e,
                                                getText(
                                                        R.string.error_recording_video)
                                                        .toString(), true, true);
                                    }
                                }
                            }
                        });

                // Show the dialog to choose the provider
                removeRecordingDialog.show();
            }
        }

        return super.onKeyDown(keyCode, event);
    }

    public LinearLayout getBarControls() {
        return barControls;
    }

    public void setBarControls(LinearLayout barControls) {
        this.barControls = barControls;
    }

    public Animation getHideBarControls() {
        return hideBarControls;
    }

    public void setHideBarControls(Animation hideBarControls) {
        this.hideBarControls = hideBarControls;
    }

    public Animation getShowBarControls() {
        return showBarControls;
    }

    public void setShowBarControls(Animation showBarControls) {
        this.showBarControls = showBarControls;
    }

    public boolean isPaused() {
        return isPaused;
    }

    public void setPaused(boolean isPaused) {
        this.isPaused = isPaused;
    }

    private void startWatchingRecordedVideo() {
        Intent intentSuggestedVideo = new Intent(RecorderActivity.this,
                PlayerActivity.class);

        // Bundle bundleVideo = new Bundle();
        //
        // bundleVideo.putSerializable(VoizeeConstants.VIDEO_VALUE_REQUESTED,
        // (Serializable) voizeeVideoRequest);
        //
        // intentSuggestedVideo.putExtra("bundleVideo", bundleVideo);

        getShareApp().setSelectedVideo(voizeeVideoRequest);

        RecorderActivity.this.startActivityForResult(intentSuggestedVideo,
                VoizeeConstants.START_SUGGESTED_VIDEO_RECORDED);
    }
}
