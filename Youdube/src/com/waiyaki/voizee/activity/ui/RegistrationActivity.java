/**
 *
 */
package com.waiyaki.voizee.activity.ui;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import com.waiyaki.voizee.util.StatusLogin;
import com.waiyaki.voizee.util.VoizeeConstants;
import org.json.JSONException;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.waiyaki.voizee.dao.model.ResultLogin;
import com.waiyaki.voizee.main.R;

import java.io.File;

/**
 * @author Francisco Javier Morant Moya
 */
public class RegistrationActivity extends BaseActivity {

    private Button registerBtn;

    private EditText emailValue;
    private EditText nameValue;
    private EditText passValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.registration_voizee);

        registerBtn = (Button) this.findViewById(R.id.signUpBtn);

        registerBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Checks when the user
                // want to login
                if (nameValue != null && nameValue.getText()!=null && nameValue.getText().length() < 5) {

                    showErrorMessage(null,
                            getText(R.string.warning_register_user).toString(),
                            false, true);
                } else if (passValue != null
                        && passValue.getText()!=null && passValue.getText().length() < 5)

                    showErrorMessage(null,
                            getText(R.string.warning_register_pass).toString(),
                            false, true);
                else if (emailValue != null
                        && emailValue.getText()!=null && emailValue.getText().toString().isEmpty()
                        && emailValue.getText().toString().indexOf("@") < 0)

                    showErrorMessage(
                            null,
                            getText(R.string.warning_register_email).toString(),
                            false, true);
                else {

                    new RegisterTask().execute(null, null, null);


                }
            }
        });

        emailValue = (EditText) this.findViewById(R.id.emailValueReg);
        nameValue = (EditText) this.findViewById(R.id.nameValueReg);
        passValue = (EditText) this.findViewById(R.id.passValueReg);

    }


    private class RegisterTask extends AsyncTask<Void, Void, ResultLogin> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            // Show a dialog registering
            showDialog(getText(R.string.registering_user).toString(),false);
        }

        /**
         * The system calls this to perform work in a worker thread and delivers
         * it the parameters given to AsyncTask.execute()
         */

		/*
         * (non-Javadoc)
		 *
		 * @see android.os.AsyncTask#doInBackground(Params[])
		 */
        @Override
        protected ResultLogin doInBackground(Void... params) {

            // When the checks
            // are right
            ResultLogin result;

            try {

                result = RegistrationActivity.this
                        .getShareApp()
                        .getManagerLogin()
                        .register(emailValue.getText().toString(),
                                nameValue.getText().toString(),
                                passValue.getText().toString());

            } catch (Exception e) {

                ResultLogin failedRegistration = new ResultLogin();
                failedRegistration.setStatus(false);
                failedRegistration.setResult(RegistrationActivity.this.getText(R.string.error_registering_user).toString());

                return failedRegistration;
            }

            return result;
        }

		/*
         * (non-Javadoc)
		 *
		 * @see android.os.AsyncTask#doInBackground(Params[])
		 */

        /**
         * The system calls this to perform work in the UI thread and delivers
         * the result from doInBackground()
         */
        protected void onPostExecute(ResultLogin resultLogin) {

            if (resultLogin != null) {
                if (resultLogin.isStatus()) {

                    // Dismiss the dialog
                    setHasFinished(true);

                    // Show a toast, that you are already member
                    Toast.makeText(RegistrationActivity.this,
                            getText(R.string.register_confirm_user),
                            Toast.LENGTH_LONG).show();

                    RegistrationActivity.this.finish();

                } else {
                    if (resultLogin.getResult() != null && !resultLogin.getResult().equals(""))
                        showErrorMessage(null, resultLogin.getResult(), false,
                                true);
                    else
                        showErrorMessage(null, getText(R.string.error_registering_user).toString(), false,
                                true);
                }
            } else {
                showErrorMessage(null, getText(R.string.error_registering_user).toString(), false,
                        true);
            }
        }
    }
}
