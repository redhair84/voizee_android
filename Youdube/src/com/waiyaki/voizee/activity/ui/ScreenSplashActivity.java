package com.waiyaki.voizee.activity.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import com.waiyaki.voizee.main.R;
import com.waiyaki.voizee.util.VoizeeConstants;

public class ScreenSplashActivity extends BaseActivity {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);

		TextView labelDeveloperMode = (TextView) findViewById(R.id.labelDeveloperMode);

		if (labelDeveloperMode != null && VoizeeConstants.DEVELOPER_MODE)
			labelDeveloperMode.setVisibility(View.VISIBLE);

		TextView labelVersion = (TextView) findViewById(R.id.labelVersion);

		labelVersion.setText("Version "+VoizeeConstants.VERSION_NUMBER);

		final Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				// Finish the current activity and start the new one
				finish();
				// Start Activity
				ScreenSplashActivity.this.startActivity(new Intent(
						ScreenSplashActivity.this.getBaseContext(),
						MainPagerActivity.class));
			}
		}, 2000);
	}
}