/**
 *
 */
package com.waiyaki.voizee.activity.ui;

import java.io.File;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;

import com.waiyaki.voizee.dao.model.VoizeeVideo;
import com.waiyaki.voizee.main.R;
import com.waiyaki.voizee.util.CategoryOnItemSelectedListener;
import com.waiyaki.voizee.util.StatusLogin;
import com.waiyaki.voizee.util.VoizeeConstants;

/**
 * @author Francisco Javier Morant Moya
 */
public class UploadDialogActivity extends BaseActivity {

    private EditText editTextTitulo;
    private Button sendBtn;
    private ArrayAdapter<CharSequence> mAdapterCategories;

    // Spinners of categories
    public static final int CATEGORY_POSITION = 0;

    private int mPosCategory;
    private String mSelectionCategory;

    private VoizeeVideo voizeeVideoRequest;
    private Spinner spinnerCategory;
    private CheckBox isPrivateCheckBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.dialogformvideo);

        if (getIntent() != null && getIntent().getExtras() != null) {
            // Get the bundle
            Bundle bndObj = (Bundle) getIntent().getExtras().get(
                    "bundleToUploadVideo");
            voizeeVideoRequest = (VoizeeVideo) bndObj
                    .getSerializable("videoToUpload");

            sendBtn = (Button) this.findViewById(R.id.btnSendNew);

            sendBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // Envio de formulario

                    if (getEditTextTitulo() != null && getEditTextTitulo().getText() != null && getEditTextTitulo().getText().toString() != null
                            && !getEditTextTitulo().getText().toString()
                            .isEmpty()) {

                        new UploadLoadTask().execute(null, null, null);
                    } else {

                        showErrorMessage(null,
                                getText(R.string.warning_upload_video)
                                        .toString(), false, true);
                    }

                }
            });

            setEditTextTitulo((EditText) this.findViewById(R.id.editTextTitulo));

            // Set the spinner categories
            spinnerCategory = (Spinner) this
                    .findViewById(R.id.spinnerCategories);

            mAdapterCategories = ArrayAdapter.createFromResource(this,
                    R.array.Categorias,
                    android.R.layout.simple_spinner_dropdown_item);

            spinnerCategory.setAdapter(mAdapterCategories);

            // Declare the listeners for the providers

            CategoryOnItemSelectedListener spinnerListenerCategories = new CategoryOnItemSelectedListener(
                    this, this.mAdapterCategories);

			/*
             * Attach the listener to the Spinner.
			 */

            spinnerCategory
                    .setOnItemSelectedListener(spinnerListenerCategories);

            isPrivateCheckBox = (CheckBox) this.findViewById(R.id.isPrivate);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == VoizeeConstants.START_SHARE_VIDEO_UPLOADED) {
            finish();
        }
    }

    public ArrayAdapter<CharSequence> getmAdapterCategories() {
        return mAdapterCategories;
    }

    public void setmAdapterCategories(
            ArrayAdapter<CharSequence> mAdapterCategories) {
        this.mAdapterCategories = mAdapterCategories;
    }

    public Spinner getSpinnerCategory() {
        return spinnerCategory;
    }

    public void setSpinnerCategory(Spinner spinnerCategory) {
        this.spinnerCategory = spinnerCategory;
    }

    public int getmPosCategory() {
        return mPosCategory;
    }

    public void setmPosCategory(int mPosCategory) {
        this.mPosCategory = mPosCategory;
    }

    public String getmSelectionCategory() {
        return mSelectionCategory;
    }

    public void setmSelectionCategory(String mSelectionCategory) {
        this.mSelectionCategory = mSelectionCategory;
    }

    public EditText getEditTextTitulo() {
        return editTextTitulo;
    }

    public void setEditTextTitulo(EditText editTextTitulo) {
        this.editTextTitulo = editTextTitulo;
    }

    private class UploadLoadTask extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Show the transaction dialog
            showDialog(getText(R.string.uploading_video).toString(), false);
        }

        /**
         * The system calls this to perform work in a worker thread and delivers
         * it the parameters given to AsyncTask.execute()
         */

		/*
         * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#doInBackground(Params[])
		 */
        @Override
        protected String doInBackground(Void... params) {

            if (getEditTextTitulo() != null && getEditTextTitulo().getText() != null)
                // Set the title
                voizeeVideoRequest.setTitulo(getEditTextTitulo().getText()
                        .toString());
            else
                voizeeVideoRequest.setTitulo("Default");

            // If the user is logged in it is passed the id user
            if (getShareApp().getStatusLogin().getCurrentStatus() !=
                    StatusLogin.STATUS_LOGIN_NONE)
                voizeeVideoRequest.setIdUsuario(getShareApp().getIdUser());

            // IT is passed the category
            if (getmPosCategory() != mAdapterCategories.getCount() - 1)
                voizeeVideoRequest.setCategoria(String
                        .valueOf((getmPosCategory() + 1)));
            else
                voizeeVideoRequest.setCategoria(String.valueOf(9));

            // Call to upload the video to voizee servers
            String urlYoudubeVideo = UploadDialogActivity.this
                    .getShareApp()
                    .getManagerVideoPlayer()
                    .uploadVideo(
                            voizeeVideoRequest.getUrlAudio(),
                            voizeeVideoRequest,
                            UploadDialogActivity.this.getShareApp()
                                    .getTokenSession(), isPrivateCheckBox.isChecked());

            return urlYoudubeVideo;
        }

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#doInBackground(Params[])
		 */

        /**
         * The system calls this to perform work in the UI thread and delivers
         * the result from doInBackground()
         */
        protected void onPostExecute(final String urlYoudubeVideo) {
            // Send video to share it
            if (urlYoudubeVideo != null) {

                // Remove the sending dialog
                setHasFinished(true);

                // Delete the recorded file
                File file = new File(voizeeVideoRequest.getUrlAudio());
                boolean deleted = file.delete();

                if (deleted && VoizeeConstants.DEVELOPER_MODE)
                    Log.d("Voizee", "Audio file deleted");

                final AlertDialog.Builder dialogSuggestRecording = new AlertDialog.Builder(
                        UploadDialogActivity.this);
                dialogSuggestRecording.setTitle("Voizee");
                dialogSuggestRecording
                        .setMessage(getText(R.string.upload_success_video));

                dialogSuggestRecording.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                // Go to the player to play a
                                // local
                                // video


                                try {
                                    Intent intent = new Intent(Intent.ACTION_SEND);
                                    intent.putExtra(
                                            Intent.EXTRA_TEXT,
                                            getText(R.string.upload_share_video)
                                                    + " " + urlYoudubeVideo);
                                    intent.setType("text/plain");

                                    startActivity(Intent.createChooser(intent,
                                            getText(R.string.upload_share_action_video)));

                                } catch (final ActivityNotFoundException e) {
                                    Log.i("Voizee", "no twitter native", e);
                                }

                            }
                        });

                dialogSuggestRecording.show();

            } else {

                showErrorMessage(null, getText(R.string.error_uploading_video)
                        .toString(), false, true);

            }

        }
    }

}
