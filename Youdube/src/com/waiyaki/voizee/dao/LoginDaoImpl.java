/**
 * 
 */
package com.waiyaki.voizee.dao;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.waiyaki.voizee.dao.model.BookMark;
import com.waiyaki.voizee.dao.model.ResultLogin;
import com.waiyaki.voizee.parser.JSONParser;
import com.waiyaki.voizee.util.VoizeeConstants;

/**
 * @author Francisco Javier Morant Moya
 * 
 */
public class LoginDaoImpl implements LoginDao {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.waiyaki.videolate.dao.LoginDao#loginFB(java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public ResultLogin loginFB(String correo, String username,
			String id_facebook, String nombre) throws JSONException {
		// // Creating JSON Parser instance
		JSONParser jParser = new JSONParser();
		//
		Map<String, String> mapParams = new HashMap<String, String>();

		mapParams.put("correo", correo);
		mapParams.put("username", username);
		mapParams.put("id_facebook", id_facebook);
		mapParams.put("nombre", nombre);

		// // getting JSON string from URL
		JSONObject json;
		try {
			json = jParser.getJSONFromPost(null, null, mapParams,
					VoizeeConstants.URL_HOST_SERVER_VOIZEE
							+ VoizeeConstants.URL_API_SERVER_VOIZEE
							+ "login_fb");
		} catch (IOException e) {
			ResultLogin resultLogin = new ResultLogin();
			resultLogin.setStatus(false);
			resultLogin.setResult(e.getMessage());

			return resultLogin;
		}

		ResultLogin resultLogin = new ResultLogin();
		resultLogin.setStatus(json.getBoolean("status"));
		resultLogin.setResult(json.getString("result"));

		// Setting the user id
		if (resultLogin.isStatus()) {
			JSONObject usuario = ((JSONObject) ((JSONObject) json.get("result"))
					.get("usuario"));

			// Get the user
			if (usuario.getString("id_user") != null)
				resultLogin.setIdUsuario(usuario.getString("id_user"));
			else
				resultLogin.setIdUsuario("");

			// Get the token of the session
			if (usuario.getString("token") != null)
				resultLogin.setToken(usuario.getString("token"));
			else
				resultLogin.setToken("");

		} else
			resultLogin.setIdUsuario("");

		return resultLogin;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.waiyaki.videolate.dao.LoginDao#login(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public ResultLogin loginVoizee(String usuario, String password)
			throws JSONException {
		// // Creating JSON Parser instance
		JSONParser jParser = new JSONParser();
		//
		Map<String, String> mapParams = new HashMap<String, String>();

		mapParams.put("usuario", usuario);
		mapParams.put("password", password);

		// // getting JSON string from URL
		JSONObject json;

		try {
			json = jParser.getJSONFromPost(null, null, mapParams,
					VoizeeConstants.URL_HOST_SERVER_VOIZEE
							+ VoizeeConstants.URL_API_SERVER_VOIZEE + "login");
		} catch (Exception e) {
			ResultLogin resultLogin = new ResultLogin();
			resultLogin.setStatus(false);
			resultLogin.setResult(e.getMessage());

			return resultLogin;
		}

		ResultLogin resultLogin = new ResultLogin();
		resultLogin.setStatus(json.getBoolean("status"));
		resultLogin.setResult(json.getString("result"));

		// Setting the user id
		if (resultLogin.isStatus()) {
			String idUsuario = ((JSONObject) ((JSONObject) json.get("result"))
					.get("usuario")).getString("id_user");
			if (idUsuario != null)
				resultLogin.setIdUsuario(idUsuario);
			else
				resultLogin.setIdUsuario("");

			String userName = ((JSONObject) ((JSONObject) json.get("result"))
					.get("usuario")).getString("username");
			if (userName != null)
				resultLogin.setUserName(userName);
			else
				resultLogin.setUserName("");

            // Get the token of the session
            String token = ((JSONObject) ((JSONObject) json.get("result"))
                    .get("usuario")).getString("token");

            if (token!= null)
                resultLogin.setToken(token);
            else
                resultLogin.setToken("");
		} else
			resultLogin.setIdUsuario("");

		return resultLogin;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.waiyaki.videolate.dao.LoginDao#register(java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public ResultLogin register(String correo, String usuario, String password)
			throws JSONException {
		// // Creating JSON Parser instance
		JSONParser jParser = new JSONParser();
		//
		Map<String, String> mapParams = new HashMap<String, String>();

		mapParams.put("correo", correo);
		mapParams.put("usuario", usuario);
		mapParams.put("password", password);

		// // getting JSON string from URL
		JSONObject json;

		try {
			json = jParser.getJSONFromPost(null, null, mapParams,
					VoizeeConstants.URL_HOST_SERVER_VOIZEE + VoizeeConstants.URL_API_SERVER_VOIZEE+"register");
		} catch (IOException e) {
			ResultLogin resultLogin = new ResultLogin();
			resultLogin.setStatus(false);
			resultLogin.setResult(e.getMessage());

			return resultLogin;
		}

		ResultLogin resultLogin = new ResultLogin();
		resultLogin.setStatus(json.getBoolean("status"));
		resultLogin.setResult(json.getString("result"));

		return resultLogin;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.waiyaki.videolate.dao.LoginDao#getBookMarks(java.lang.String)
	 */
	@Override
	public List<BookMark> getBookMarks(String id_usuario) {
		// TODO Auto-generated method stub
		return null;
	}

}
