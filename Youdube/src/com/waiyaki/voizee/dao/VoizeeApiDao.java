/**
 * 
 */
package com.waiyaki.voizee.dao;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import org.json.JSONException;

import com.google.gdata.util.ServiceException;
import com.waiyaki.voizee.dao.model.VoizeeVideo;

/**
 * @author Francisco Javier Morant Moya
 * 
 */
public interface VoizeeApiDao {

	public List<VoizeeVideo> searchYoutubeVideos(String videoQuery,
			String language) throws MalformedURLException;

	public VoizeeVideo getVideo(String id) throws MalformedURLException,
			IOException, ServiceException;

	public String uploadVideo(String path, VoizeeVideo youtubeVideo,
			String token,boolean isPrivate);

    public VoizeeVideo getVoizeeVideo(String idVoizee) throws JSONException;

	public List<VoizeeVideo> getPopularVideos(String idUser)
			throws JSONException;

	public List<VoizeeVideo> getLastVideos(String idUser) throws JSONException;

	public List<VoizeeVideo> getMainMostDubbedVideos() throws JSONException;

	public List<VoizeeVideo> getDetailMostDubbedVideos(String youtubeId,
			String idUser) throws JSONException;

	public List<VoizeeVideo> getMostViewedVideos(String idUser)
			throws JSONException;

	public List<VoizeeVideo> getMyVideos(String idUser) throws JSONException;

	public List<VoizeeVideo> getMyFavouritesVideos(String idUser)
			throws JSONException;

	public List<VoizeeVideo> getSearchedVideos(String searchValueEntered,
			String idUser) throws JSONException;

	public boolean addNewViewToVideo(String idVoizee) throws JSONException;

	public String userOperationVideoPlayer(String operation, String idUser,
			String idVideo, String token) throws JSONException;

}
