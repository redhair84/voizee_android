package com.waiyaki.voizee.dao;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.google.gdata.client.youtube.YouTubeQuery;
import com.google.gdata.client.youtube.YouTubeQuery.OrderBy;
import com.google.gdata.client.youtube.YouTubeService;
import com.google.gdata.data.media.mediarss.MediaThumbnail;
import com.google.gdata.data.youtube.VideoEntry;
import com.google.gdata.data.youtube.VideoFeed;
import com.google.gdata.data.youtube.YouTubeMediaGroup;
import com.google.gdata.util.ServiceException;
import com.waiyaki.voizee.dao.model.VoizeeVideo;
import com.waiyaki.voizee.parser.JSONParser;
import com.waiyaki.voizee.util.VoizeeConstants;

/**
 * @author Francisco Javier Morant Moya
 */
public class VoizeeApiDaoImpl implements VoizeeApiDao {

    /*
     * (non-Javadoc)
     *
     * @see
     * com.waiyaki.videolate.dao.VideoPlayerDao#getListVideos(java.lang.String)
     */
    @Override
    public List<VoizeeVideo> searchYoutubeVideos(String videoQuery,
                                                 String language) throws MalformedURLException {

        //
        List<VoizeeVideo> youtubeVideos;

        youtubeVideos = new ArrayList<VoizeeVideo>();
        //

        YouTubeService service = new YouTubeService(
                "VideoLate",
                "AI39si7kqXDdI18LRSNR_JZOFKNqSLMzUC0yQ-Do9z2VpaAv-giphBwz8tJfdRuw_K2ma2vuKCwG62qrP8oyLR9tyA3B3ehe_Q");

        YouTubeQuery query = new YouTubeQuery(new URL(
                "http://gdata.youtube.com/feeds/mobile/videos"));

        // order results by the number of views (most viewed first)
        query.setOrderBy(YouTubeQuery.OrderBy.VIEW_COUNT);

        // search for puppies and include restricted content in the search
        // results
        query.setFullTextQuery(videoQuery);
        query.setFormats(1, 6);
        query.setSafeSearch(YouTubeQuery.SafeSearch.NONE);
        query.setMaxResults(50);
        query.setOrderBy(OrderBy.RELEVANCE);

        if (language != null) {
            query.setLanguageRestrict(language);
        }

        // Get the video entries from the youtube API
        VideoFeed videoFeed = null;
        try {
            videoFeed = service.query(query, VideoFeed.class);

        } catch (Exception e) {
            if (VoizeeConstants.DEVELOPER_MODE) {
                Log.e("Voizee", e.getMessage());
            }
        }

        if (videoFeed != null) {
            for (VideoEntry entry : videoFeed.getEntries()) {

                VoizeeVideo objVideo = new VoizeeVideo();
                objVideo.setTitulo(entry.getTitle().getPlainText());
                objVideo.setDescripcion(entry.getMediaGroup().getDescription()
                        .getPlainTextContent());
                objVideo.setId(entry.getMediaGroup().getVideoId());

                YouTubeMediaGroup mediaGroup = entry.getMediaGroup();

                for (com.google.gdata.data.media.mediarss.MediaContent content : mediaGroup
                        .getContents())
                    objVideo.setUrlVideo(content.getUrl());

                for (MediaThumbnail mediaThumbnail : mediaGroup.getThumbnails()) {
                    objVideo.setUrlThumbnail(mediaThumbnail.getUrl());
                }

                // Se a�ade a la lista
                youtubeVideos.add(objVideo);
            }
        }

        return youtubeVideos;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.waiyaki.videolate.dao.VideoPlayerDao#getVideo(java.lang.String)
     */
    @Override
    public VoizeeVideo getVideo(String id) throws MalformedURLException,
            IOException, ServiceException {

        YouTubeService service = new YouTubeService(
                "Voizee",
                "AI39si7kqXDdI18LRSNR_JZOFKNqSLMzUC0yQ-Do9z2VpaAv-giphBwz8tJfdRuw_K2ma2vuKCwG62qrP8oyLR9tyA3B3ehe_Q");
        String videoEntryUrl = "http://gdata.youtube.com/feeds/mobile/videos/"
                + id;

        VideoEntry videoEntry = service.getEntry(new URL(videoEntryUrl),
                VideoEntry.class);

        VoizeeVideo objVideo = new VoizeeVideo();
        objVideo.setTitulo(videoEntry.getTitle().getPlainText());
        objVideo.setDescripcion(videoEntry.getMediaGroup().getDescription()
                .getPlainTextContent());
        objVideo.setId(videoEntry.getMediaGroup().getVideoId());

        YouTubeMediaGroup mediaGroup = videoEntry.getMediaGroup();

        for (com.google.gdata.data.media.mediarss.MediaContent content : mediaGroup
                .getContents()) {
            objVideo.setUrlVideo(content.getUrl());
        }

        for (MediaThumbnail mediaThumbnail : mediaGroup.getThumbnails()) {
            objVideo.setUrlThumbnail(mediaThumbnail.getUrl());
        }

        return objVideo;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.waiyaki.videolate.dao.VideoPlayerDao#uploadVideo(java.io.InputStream)
     */
    @Override
    public String uploadVideo(String path, VoizeeVideo youtubeVideo,
                              String token,boolean isPrivate) {

        String result;

        Map<String, String> mapValues = new HashMap<String, String>();
        mapValues.put("youtube_id", youtubeVideo.getId());
        mapValues.put("titulo", youtubeVideo.getTitulo());
        mapValues.put("id_usuario", youtubeVideo.getIdUsuario());
        mapValues.put("autor", youtubeVideo.getAutor());
        mapValues.put("categoria", youtubeVideo.getCategoria());
        mapValues.put("volumen", String.valueOf(youtubeVideo.getVolume()));
        mapValues.put("thumbnail", youtubeVideo.getUrlThumbnail());
        mapValues.put("token", token);

        if(isPrivate)
            mapValues.put("private","1");
        else
            mapValues.put("private","0");

        JSONObject json;
        try {
            json = JSONParser.getJSONFromPost("audio", path, mapValues,
                    VoizeeConstants.URL_HOST_SERVER_VOIZEE
                            + VoizeeConstants.URL_API_SERVER_VOIZEE + "upload");
            if (json != null && json.getString("status").equals("true"))
                result = json.getJSONObject("result").getString("url");
            else
                return null;
        } catch (IOException e) {
            return null;
        } catch (JSONException e) {
            return null;
        }

        return result;
    }


    public VoizeeVideo getVoizeeVideo(String idVoizee) throws JSONException {

        if (VoizeeConstants.DEVELOPER_MODE)
            Log.d("Voizee", "I come to get my voizee video");

        VoizeeVideo newVoizeeVideo = new VoizeeVideo();


        // // Creating JSON Parser instance
        JSONParser jParser = new JSONParser();

        String urlServer;

        urlServer = VoizeeConstants.URL_HOST_SERVER_VOIZEE
                + VoizeeConstants.URL_API_SERVER_VOIZEE + "dub_url?youdube_id=" + idVoizee;
        //
        // // getting JSON string from URL
        JSONObject json = jParser.getJSONFromUrl(urlServer);

        if (json != null) {

            JSONObject result = json.getJSONObject("result");


            newVoizeeVideo.setAutor(result
                    .getString("autor").toString());

            newVoizeeVideo.setViews(result
                    .getString("views").toString());
            newVoizeeVideo.setId(result.getString("youtube_id").toString());
            newVoizeeVideo.setIdRecording(result
                    .getString("id_recording").toString());
            newVoizeeVideo.setTitulo(result
                    .getString("title").toString());
            newVoizeeVideo.setUrlThumbnail(result
                    .getString("thumbnail").toString());
            newVoizeeVideo.setVolume(Integer.parseInt(result
                    .getString("volumen").toString()));
            newVoizeeVideo.setUrlVideo("http://www.youtube.com/watch?v="
                    + result.getString("youtube_id")
                    .toString());
            newVoizeeVideo.setUrlLink(VoizeeConstants.URL_HOST_SERVER_VOIZEE
                    + "/ini/video/"
                    + result.getString("url")
                    .toString());
            newVoizeeVideo.setUrlAudio(result
                    .getString("audio_file").toString());

            newVoizeeVideo.setUrlOnda(VoizeeConstants.URL_HOST_SERVER_VOIZEE
                    + "/ondas/560x40_"
                    + result.getString("dibujo_onda")
                    .toString());

            newVoizeeVideo.setBookmark(result
                    .getBoolean("is_bookmark"));
            newVoizeeVideo.setLike(result.getBoolean(
                    "is_like"));
            newVoizeeVideo.setDislike(result
                    .getBoolean("is_dislike"));
            newVoizeeVideo.setReport(result.getBoolean(
                    "is_report"));

        }

        if (VoizeeConstants.DEVELOPER_MODE)
            Log.d("Voizee", "I finish to get my voizee video");

        return newVoizeeVideo;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.waiyaki.videolate.dao.VideoPlayerDao#getPopularVideos(java.lang.String
     * )
     */
    @Override
    public List<VoizeeVideo> getPopularVideos(String idUser)
            throws JSONException {
        // // Creating JSON Parser instance
        JSONParser jParser = new JSONParser();

        String urlServer;

        if (idUser != null)
            urlServer = VoizeeConstants.URL_HOST_SERVER_VOIZEE
                    + VoizeeConstants.URL_API_SERVER_VOIZEE
                    + "populars?id_usuario=" + idUser;
        else
            urlServer = VoizeeConstants.URL_HOST_SERVER_VOIZEE
                    + VoizeeConstants.URL_API_SERVER_VOIZEE + "populars";
        //
        // // getting JSON string from URL
        JSONObject json = jParser.getJSONFromUrl(urlServer);
        //
        List<VoizeeVideo> youtubeVideos = new ArrayList<VoizeeVideo>();

        if (json != null) {

            JSONObject result = json.getJSONObject("result");

            // using JSONArray to grab the menuitems from under popop
            JSONArray videosArray = result.getJSONArray("videos");

            // lets loop through the JSONArray and get all the items
            for (int i = 0; i < videosArray.length(); i++) {
                VoizeeVideo youtubeVideo = new VoizeeVideo();

                youtubeVideo.setAutor(videosArray.getJSONObject(i)
                        .getString("autor").toString());
                youtubeVideo.setViews(videosArray.getJSONObject(i)
                        .getString("views").toString());
                youtubeVideo.setId(videosArray.getJSONObject(i)
                        .getString("youtube_id").toString());
                youtubeVideo.setIdRecording(videosArray.getJSONObject(i)
                        .getString("id_recording").toString());
                youtubeVideo.setTitulo(videosArray.getJSONObject(i)
                        .getString("title").toString());
                youtubeVideo.setUrlThumbnail(videosArray.getJSONObject(i)
                        .getString("thumbnail").toString());
                youtubeVideo.setVolume(Integer.parseInt(videosArray
                        .getJSONObject(i).getString("volumen").toString()));
                youtubeVideo.setUrlVideo("http://www.youtube.com/watch?v="
                        + videosArray.getJSONObject(i).getString("youtube_id")
                        .toString());
                youtubeVideo.setUrlLink(VoizeeConstants.URL_HOST_SERVER_VOIZEE
                        + "/ini/video/"
                        + videosArray.getJSONObject(i).getString("url")
                        .toString());
                youtubeVideo.setUrlAudio(videosArray.getJSONObject(i)
                        .getString("audio_file").toString());

                youtubeVideo.setUrlOnda(VoizeeConstants.URL_HOST_SERVER_VOIZEE
                        + "/ondas/560x40_"
                        + videosArray.getJSONObject(i).getString("dibujo_onda")
                        .toString());

                youtubeVideo.setBookmark(videosArray.getJSONObject(i)
                        .getBoolean("is_bookmark"));
                youtubeVideo.setLike(videosArray.getJSONObject(i).getBoolean(
                        "is_like"));
                youtubeVideo.setDislike(videosArray.getJSONObject(i)
                        .getBoolean("is_dislike"));
                youtubeVideo.setReport(videosArray.getJSONObject(i).getBoolean(
                        "is_report"));

                // printing the values to the logcat
                // Log.v(videosArray.getJSONObject(i).getString("onclick").toString());

                youtubeVideos.add(youtubeVideo);
            }
        }

        return youtubeVideos;
    }

	/*
     * (non-Javadoc)
	 * 
	 * @see com.waiyaki.videolate.dao.VideoPlayerDao#getSuggestedVideo()
	 */
    // @Override
    // public List<YouTubeVideoRequest> getSuggestedVideos() throws
    // JSONException,
    // MalformedURLException, IOException, ServiceException {
    //
    // // // Creating JSON Parser instance
    // JSONParser jParser = new JSONParser();
    // //
    // // // getting JSON string from URL
    // JSONObject json = jParser
    // .getJSONFromUrl("http://youdube.it/api.php?op=suggestions");
    // //
    // List<YouTubeVideoRequest> youtubeVideos = youtubeVideos = new
    // ArrayList<YouTubeVideoRequest>();
    //
    // JSONObject result = json.getJSONObject("result");
    //
    // // using JSONArray to grab the menuitems from under popop
    // JSONArray videosArray = result.getJSONArray("videos");
    //
    // // lets loop through the JSONArray and get all the items
    // for (int i = 0; i < videosArray.length(); i++) {
    // YouTubeVideoRequest youtubeVideo = new YouTubeVideoRequest();
    //
    // youtubeVideo.setId(videosArray.getJSONObject(i)
    // .getString("youtube_id").toString());
    // youtubeVideo.setTitulo(videosArray.getJSONObject(i)
    // .getString("title").toString());
    // youtubeVideo.setUrlThumbnail(videosArray.getJSONObject(i)
    // .getString("thumbnail").toString());
    //
    // // printing the values to the logcat
    // // Log.v(videosArray.getJSONObject(i).getString("onclick").toString());
    //
    // youtubeVideos.add(youtubeVideo);
    // }
    //
    // return youtubeVideos;
    // }

    // /*
    // * (non-Javadoc)
    // *
    // * @see com.waiyaki.videolate.dao.VideoPlayerDao#getSuggestedVideo()
    // */
    // @Override
    // public YouTubeVideoRequest getVideoYoutube(String videoYoutubeId)
    // throws JSONException, MalformedURLException, IOException,
    // ServiceException {
    //
    // // // Creating JSON Parser instance
    // JSONParser jParser = new JSONParser();
    // //
    // // // getting JSON string from URL
    // JSONObject json = jParser
    // .getJSONFromUrl("http://youdube.it/api.php?op=dub_url&youdube_id="
    // + videoYoutubeId);
    // //
    // List<YouTubeVideoRequest> youtubeVideos = youtubeVideos = new
    // ArrayList<YouTubeVideoRequest>();
    //
    // JSONObject result = json.getJSONObject("result");
    //
    // // using JSONArray to grab the menuitems from under popop
    //
    // // lets loop through the JSONArray and get all the items
    //
    // YouTubeVideoRequest youtubeVideo = new YouTubeVideoRequest();
    //
    // youtubeVideo.setId(result.getString("youtube_id").toString());
    // youtubeVideo.setTitulo(result.getString("title").toString());
    // youtubeVideo.setUrlAudio(result.getString("audio_file").toString());
    //
    // return youtubeVideo;
    // }

    /*
     * (non-Javadoc)
     *
     * @see com.waiyaki.videolate.dao.VideoPlayerDao#getLastVideos()
     */
    @Override
    public List<VoizeeVideo> getLastVideos(String idUser) throws JSONException {
        // // Creating JSON Parser instance
        JSONParser jParser = new JSONParser();

        String urlServer;

        if (idUser != null)
            urlServer = VoizeeConstants.URL_HOST_SERVER_VOIZEE
                    + VoizeeConstants.URL_API_SERVER_VOIZEE
                    + "latests?id_usuario=" + idUser;
        else
            urlServer = VoizeeConstants.URL_HOST_SERVER_VOIZEE
                    + VoizeeConstants.URL_API_SERVER_VOIZEE + "latests";

        //
        // // getting JSON string from URL
        JSONObject json = jParser.getJSONFromUrl(urlServer);
        //
        List<VoizeeVideo> youtubeVideos = new ArrayList<VoizeeVideo>();

        if (json != null) {

            JSONObject result = json.getJSONObject("result");

            // using JSONArray to grab the menuitems from under popop
            JSONArray videosArray = result.getJSONArray("videos");

            // lets loop through the JSONArray and get all the items
            for (int i = 0; i < videosArray.length(); i++) {
                VoizeeVideo youtubeVideo = new VoizeeVideo();

                youtubeVideo.setAutor(videosArray.getJSONObject(i)
                        .getString("autor").toString());
                youtubeVideo.setViews(videosArray.getJSONObject(i)
                        .getString("views").toString());
                youtubeVideo.setId(videosArray.getJSONObject(i)
                        .getString("youtube_id").toString());
                youtubeVideo.setIdRecording(videosArray.getJSONObject(i)
                        .getString("id_recording").toString());
                youtubeVideo.setTitulo(videosArray.getJSONObject(i)
                        .getString("title").toString());
                youtubeVideo.setUrlThumbnail(videosArray.getJSONObject(i)
                        .getString("thumbnail").toString());
                youtubeVideo.setVolume(Integer.parseInt(videosArray
                        .getJSONObject(i).getString("volumen").toString()));
                youtubeVideo.setUrlVideo("http://www.youtube.com/watch?v="
                        + videosArray.getJSONObject(i).getString("youtube_id")
                        .toString());
                youtubeVideo.setUrlLink(VoizeeConstants.URL_HOST_SERVER_VOIZEE
                        + "/ini/video/"
                        + videosArray.getJSONObject(i).getString("url")
                        .toString());
                youtubeVideo.setUrlAudio(videosArray.getJSONObject(i)
                        .getString("audio_file").toString());

                youtubeVideo.setUrlOnda(VoizeeConstants.URL_HOST_SERVER_VOIZEE
                        + "/ondas/560x40_"
                        + videosArray.getJSONObject(i).getString("dibujo_onda")
                        .toString());

                youtubeVideo.setBookmark(videosArray.getJSONObject(i)
                        .getBoolean("is_bookmark"));
                youtubeVideo.setLike(videosArray.getJSONObject(i).getBoolean(
                        "is_like"));
                youtubeVideo.setDislike(videosArray.getJSONObject(i)
                        .getBoolean("is_dislike"));
                youtubeVideo.setReport(videosArray.getJSONObject(i).getBoolean(
                        "is_report"));

                youtubeVideos.add(youtubeVideo);
            }
        }

        return youtubeVideos;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.waiyaki.videolate.dao.VideoPlayerDao#getLastVideos()
     */
    @Override
    public List<VoizeeVideo> getMainMostDubbedVideos() throws JSONException {
        // // Creating JSON Parser instance
        JSONParser jParser = new JSONParser();
        //
        // // getting JSON string from URL
        JSONObject json = jParser
                .getJSONFromUrl(VoizeeConstants.URL_HOST_SERVER_VOIZEE
                        + VoizeeConstants.URL_API_SERVER_VOIZEE + "most_dubbed");

        // If the user is logged in it has to pass the user id

        List<VoizeeVideo> youtubeVideos = new ArrayList<VoizeeVideo>();

        if (json != null) {

            JSONObject result = json.getJSONObject("result");

            // using JSONArray to grab the menuitems from under popop
            JSONArray videosArray = result.getJSONArray("videos");

            // lets loop through the JSONArray and get all the items
            for (int i = 0; i < videosArray.length(); i++) {
                VoizeeVideo youtubeVideo = new VoizeeVideo();

                youtubeVideo.setId(videosArray.getJSONObject(i)
                        .getString("youtube_id").toString());
                youtubeVideo.setTitulo(videosArray.getJSONObject(i)
                        .getString("title").toString());

                youtubeVideo.setUrlThumbnail(videosArray.getJSONObject(i)
                        .getString("thumbnail").toString());

                youtubeVideos.add(youtubeVideo);
            }
        }

        return youtubeVideos;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.waiyaki.videolate.dao.VideoPlayerDao#getLastVideos()
     */
    @Override
    public List<VoizeeVideo> getDetailMostDubbedVideos(String youtubeId,
                                                       String idUser) throws JSONException {
        // // Creating JSON Parser instance
        JSONParser jParser = new JSONParser();

        String urlServer;

        if (idUser != null)
            urlServer = VoizeeConstants.URL_HOST_SERVER_VOIZEE
                    + VoizeeConstants.URL_API_SERVER_VOIZEE
                    + "recordings_of?youtube_id=" + youtubeId + "&id_usuario="
                    + idUser;
        else
            urlServer = VoizeeConstants.URL_HOST_SERVER_VOIZEE
                    + VoizeeConstants.URL_API_SERVER_VOIZEE
                    + "recordings_of?youtube_id=" + youtubeId;

        //
        // // getting JSON string from URL
        JSONObject json = jParser.getJSONFromUrl(urlServer);

        // If the user is logged in it has to pass the user id

        List<VoizeeVideo> youtubeVideos = new ArrayList<VoizeeVideo>();

        if (json != null) {

            JSONObject result = json.getJSONObject("result");

            // using JSONArray to grab the menuitems from under popop
            JSONArray videosArray = result.getJSONArray("videos");

            // lets loop through the JSONArray and get all the items
            for (int i = 0; i < videosArray.length(); i++) {
                VoizeeVideo voizeeVideo = new VoizeeVideo();

                voizeeVideo.setAutor(videosArray.getJSONObject(i)
                        .getString("autor").toString());
                voizeeVideo.setViews(videosArray.getJSONObject(i)
                        .getString("views").toString());
                voizeeVideo.setId(videosArray.getJSONObject(i)
                        .getString("youtube_id").toString());
                voizeeVideo.setIdRecording(videosArray.getJSONObject(i)
                        .getString("id_recording").toString());
                voizeeVideo.setTitulo(videosArray.getJSONObject(i)
                        .getString("title").toString());
                voizeeVideo.setUrlThumbnail(videosArray.getJSONObject(i)
                        .getString("thumbnail").toString());
                voizeeVideo.setVolume(Integer.parseInt(videosArray
                        .getJSONObject(i).getString("volumen").toString()));
                voizeeVideo.setUrlVideo("http://www.youtube.com/watch?v="
                        + videosArray.getJSONObject(i).getString("youtube_id")
                        .toString());
                voizeeVideo.setUrlAudio(videosArray.getJSONObject(i)
                        .getString("audio_file").toString());

                voizeeVideo.setUrlOnda(VoizeeConstants.URL_HOST_SERVER_VOIZEE
                        + "/ondas/560x40_"
                        + videosArray.getJSONObject(i).getString("dibujo_onda")
                        .toString());

                voizeeVideo.setUrlLink(VoizeeConstants.URL_HOST_SERVER_VOIZEE
                        + "/ini/video/"
                        + videosArray.getJSONObject(i).getString("url")
                        .toString());

                voizeeVideo.setLike(videosArray.getJSONObject(i).getBoolean(
                        "is_like"));

                voizeeVideo.setDislike(videosArray.getJSONObject(i).getBoolean(
                        "is_dislike"));

                voizeeVideo.setBookmark(videosArray.getJSONObject(i)
                        .getBoolean("is_bookmark"));

                voizeeVideo.setReport(videosArray.getJSONObject(i).getBoolean(
                        "is_report"));

                youtubeVideos.add(voizeeVideo);
            }
        }

        return youtubeVideos;
    }

	/*
     * (non-Javadoc)
	 * 
	 * @see com.waiyaki.videolate.dao.VideoPlayerDao#getMostViewedVideos()
	 */

    @Override
    public List<VoizeeVideo> getMostViewedVideos(String idUser)
            throws JSONException {
        // // Creating JSON Parser instance
        JSONParser jParser = new JSONParser();

        String urlServer;

        if (idUser != null)
            urlServer = VoizeeConstants.URL_HOST_SERVER_VOIZEE
                    + VoizeeConstants.URL_API_SERVER_VOIZEE
                    + "most_viewed?id_usuario=" + idUser;
        else
            urlServer = VoizeeConstants.URL_HOST_SERVER_VOIZEE
                    + VoizeeConstants.URL_API_SERVER_VOIZEE + "most_viewed";

        //
        // // getting JSON string from URL
        JSONObject json = jParser.getJSONFromUrl(urlServer);
        //
        List<VoizeeVideo> youtubeVideos = new ArrayList<VoizeeVideo>();

        if (json != null) {

            JSONObject result = json.getJSONObject("result");

            // using JSONArray to grab the menuitems from under popop
            JSONArray videosArray = result.getJSONArray("videos");

            // lets loop through the JSONArray and get all the items
            for (int i = 0; i < videosArray.length(); i++) {
                VoizeeVideo youtubeVideo = new VoizeeVideo();

                youtubeVideo.setAutor(videosArray.getJSONObject(i)
                        .getString("autor").toString());
                youtubeVideo.setViews(videosArray.getJSONObject(i)
                        .getString("views").toString());
                youtubeVideo.setId(videosArray.getJSONObject(i)
                        .getString("youtube_id").toString());
                youtubeVideo.setIdRecording(videosArray.getJSONObject(i)
                        .getString("id_recording").toString());
                youtubeVideo.setTitulo(videosArray.getJSONObject(i)
                        .getString("title").toString());
                youtubeVideo.setUrlThumbnail(videosArray.getJSONObject(i)
                        .getString("thumbnail").toString());
                youtubeVideo.setVolume(Integer.parseInt(videosArray
                        .getJSONObject(i).getString("volumen").toString()));
                youtubeVideo.setUrlVideo("http://www.youtube.com/watch?v="
                        + videosArray.getJSONObject(i).getString("youtube_id")
                        .toString());

                youtubeVideo.setUrlLink(VoizeeConstants.URL_HOST_SERVER_VOIZEE
                        + "/ini/video/"
                        + videosArray.getJSONObject(i).getString("url")
                        .toString());
                youtubeVideo.setUrlAudio(videosArray.getJSONObject(i)
                        .getString("audio_file").toString());

                youtubeVideo.setUrlOnda(VoizeeConstants.URL_HOST_SERVER_VOIZEE
                        + "/ondas/560x40_"
                        + videosArray.getJSONObject(i).getString("dibujo_onda")
                        .toString());

                youtubeVideo.setBookmark(videosArray.getJSONObject(i)
                        .getBoolean("is_bookmark"));
                youtubeVideo.setLike(videosArray.getJSONObject(i).getBoolean(
                        "is_like"));
                youtubeVideo.setDislike(videosArray.getJSONObject(i)
                        .getBoolean("is_dislike"));
                youtubeVideo.setReport(videosArray.getJSONObject(i).getBoolean(
                        "is_report"));
                // printing the values to the logcat
                // Log.v(videosArray.getJSONObject(i).getString("onclick").toString());

                youtubeVideos.add(youtubeVideo);
            }
        }

        return youtubeVideos;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.waiyaki.voizee.dao.VideoPlayerDao#getMyVideos()
     */
    @Override
    public List<VoizeeVideo> getMyVideos(String idUser) throws JSONException {

        // // Creating JSON Parser instance
        JSONParser jParser = new JSONParser();
        //
        // // getting JSON string from URL
        JSONObject json = jParser
                .getJSONFromUrl(VoizeeConstants.URL_HOST_SERVER_VOIZEE
                        + VoizeeConstants.URL_API_SERVER_VOIZEE
                        + "videos_propios?id_usuario=" + idUser);
        //
        List<VoizeeVideo> youtubeVideos = new ArrayList<VoizeeVideo>();

        if (json != null) {

            JSONObject result = json.getJSONObject("result");

            // using JSONArray to grab the menuitems from under popop
            JSONArray videosArray = result.getJSONArray("videos");

            // lets loop through the JSONArray and get all the items
            for (int i = 0; i < videosArray.length(); i++) {
                VoizeeVideo youtubeVideo = new VoizeeVideo();

                youtubeVideo.setAutor(videosArray.getJSONObject(i)
                        .getString("autor").toString());
                youtubeVideo.setViews(videosArray.getJSONObject(i)
                        .getString("views").toString());
                youtubeVideo.setId(videosArray.getJSONObject(i)
                        .getString("youtube_id").toString());
                youtubeVideo.setIdRecording(videosArray.getJSONObject(i)
                        .getString("id_recording").toString());
                youtubeVideo.setTitulo(videosArray.getJSONObject(i)
                        .getString("title").toString());
                youtubeVideo.setUrlThumbnail(videosArray.getJSONObject(i)
                        .getString("thumbnail").toString());
                youtubeVideo.setVolume(Integer.parseInt(videosArray
                        .getJSONObject(i).getString("volumen").toString()));
                youtubeVideo.setUrlVideo("http://www.youtube.com/watch?v="
                        + videosArray.getJSONObject(i).getString("youtube_id")
                        .toString());
                youtubeVideo.setUrlAudio(videosArray.getJSONObject(i)
                        .getString("audio_file").toString());

                youtubeVideo.setUrlOnda(VoizeeConstants.URL_HOST_SERVER_VOIZEE
                        + "/ondas/560x40_"
                        + videosArray.getJSONObject(i).getString("dibujo_onda")
                        .toString());

                youtubeVideo.setUrlLink(VoizeeConstants.URL_HOST_SERVER_VOIZEE
                        + "/ini/video/"
                        + videosArray.getJSONObject(i).getString("url")
                        .toString());

                youtubeVideo.setBookmark(videosArray.getJSONObject(i)
                        .getBoolean("is_bookmark"));
                youtubeVideo.setLike(videosArray.getJSONObject(i).getBoolean(
                        "is_like"));
                youtubeVideo.setDislike(videosArray.getJSONObject(i)
                        .getBoolean("is_dislike"));
                youtubeVideo.setReport(videosArray.getJSONObject(i).getBoolean(
                        "is_report"));

                youtubeVideos.add(youtubeVideo);
            }
        }

        return youtubeVideos;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.waiyaki.voizee.dao.VideoPlayerDao#getMyFavouritesVideos(java.lang
     * .String)
     */
    @Override
    public List<VoizeeVideo> getMyFavouritesVideos(String idUser)
            throws JSONException {
        // // Creating JSON Parser instance
        JSONParser jParser = new JSONParser();
        //
        // // getting JSON string from URL
        JSONObject json = jParser
                .getJSONFromUrl(VoizeeConstants.URL_HOST_SERVER_VOIZEE
                        + VoizeeConstants.URL_API_SERVER_VOIZEE
                        + "bookmarks?id_usuario=" + idUser);
        //
        List<VoizeeVideo> youtubeVideos = new ArrayList<VoizeeVideo>();

        if (json != null) {

            JSONObject result = json.getJSONObject("result");

            // using JSONArray to grab the menuitems from under popop
            JSONArray videosArray = result.getJSONArray("videos");

            // lets loop through the JSONArray and get all the items
            for (int i = 0; i < videosArray.length(); i++) {
                VoizeeVideo youtubeVideo = new VoizeeVideo();

                youtubeVideo.setAutor(videosArray.getJSONObject(i)
                        .getString("autor").toString());
                youtubeVideo.setViews(videosArray.getJSONObject(i)
                        .getString("views").toString());
                youtubeVideo.setId(videosArray.getJSONObject(i)
                        .getString("youtube_id").toString());
                youtubeVideo.setIdRecording(videosArray.getJSONObject(i)
                        .getString("id_recording").toString());
                youtubeVideo.setTitulo(videosArray.getJSONObject(i)
                        .getString("title").toString());
                youtubeVideo.setUrlThumbnail(videosArray.getJSONObject(i)
                        .getString("thumbnail").toString());
                youtubeVideo.setVolume(Integer.parseInt(videosArray
                        .getJSONObject(i).getString("volumen").toString()));
                youtubeVideo.setUrlVideo("http://www.youtube.com/watch?v="
                        + videosArray.getJSONObject(i).getString("youtube_id")
                        .toString());
                youtubeVideo.setUrlAudio(videosArray.getJSONObject(i)
                        .getString("audio_file").toString());

                youtubeVideo.setUrlOnda(VoizeeConstants.URL_HOST_SERVER_VOIZEE
                        + "/ondas/560x40_"
                        + videosArray.getJSONObject(i).getString("dibujo_onda")
                        .toString());

                youtubeVideo.setUrlLink(VoizeeConstants.URL_HOST_SERVER_VOIZEE
                        + "/ini/video/"
                        + videosArray.getJSONObject(i).getString("url")
                        .toString());

                youtubeVideo.setBookmark(videosArray.getJSONObject(i)
                        .getBoolean("is_bookmark"));
                youtubeVideo.setLike(videosArray.getJSONObject(i).getBoolean(
                        "is_like"));
                youtubeVideo.setDislike(videosArray.getJSONObject(i)
                        .getBoolean("is_dislike"));
                youtubeVideo.setReport(videosArray.getJSONObject(i).getBoolean(
                        "is_report"));

                youtubeVideos.add(youtubeVideo);
            }
        }

        return youtubeVideos;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.waiyaki.voizee.dao.VideoPlayerDao#getSearchedVideos(java.lang.String)
     */
    @Override
    public List<VoizeeVideo> getSearchedVideos(String searchValueEntered,
                                               String idUser) throws JSONException {
        // // Creating JSON Parser instance
        JSONParser jParser = new JSONParser();

        String urlServer;

        if (idUser != null)
            urlServer = VoizeeConstants.URL_HOST_SERVER_VOIZEE
                    + VoizeeConstants.URL_API_SERVER_VOIZEE + "search?string="
                    + searchValueEntered + "&id_usuario=" + idUser;
        else
            urlServer = VoizeeConstants.URL_HOST_SERVER_VOIZEE
                    + VoizeeConstants.URL_API_SERVER_VOIZEE + "search?string="
                    + searchValueEntered;

        //
        // // getting JSON string from URL
        JSONObject json = jParser.getJSONFromUrl(urlServer);
        //
        List<VoizeeVideo> youtubeVideos = new ArrayList<VoizeeVideo>();

        if (json != null) {

            JSONObject result = json.getJSONObject("result");

            // using JSONArray to grab the menuitems from under popop
            JSONArray videosArray = result.getJSONArray("videos");

            // lets loop through the JSONArray and get all the items
            for (int i = 0; i < videosArray.length(); i++) {
                VoizeeVideo youtubeVideo = new VoizeeVideo();

                youtubeVideo.setAutor(videosArray.getJSONObject(i)
                        .getString("autor").toString());
                youtubeVideo.setViews(videosArray.getJSONObject(i)
                        .getString("views").toString());
                youtubeVideo.setId(videosArray.getJSONObject(i)
                        .getString("youtube_id").toString());
                youtubeVideo.setIdRecording(videosArray.getJSONObject(i)
                        .getString("id_recording").toString());
                youtubeVideo.setTitulo(videosArray.getJSONObject(i)
                        .getString("title").toString());
                youtubeVideo.setUrlThumbnail(videosArray.getJSONObject(i)
                        .getString("thumbnail").toString());
                youtubeVideo.setVolume(Integer.parseInt(videosArray
                        .getJSONObject(i).getString("volumen").toString()));
                youtubeVideo.setUrlVideo("http://www.youtube.com/watch?v="
                        + videosArray.getJSONObject(i).getString("youtube_id")
                        .toString());
                youtubeVideo.setUrlAudio(videosArray.getJSONObject(i)
                        .getString("audio_file").toString());

                youtubeVideo.setUrlOnda(VoizeeConstants.URL_HOST_SERVER_VOIZEE
                        + "/ondas/560x40_"
                        + videosArray.getJSONObject(i).getString("dibujo_onda")
                        .toString());

                youtubeVideo.setUrlLink(VoizeeConstants.URL_HOST_SERVER_VOIZEE
                        + "/ini/video/"
                        + videosArray.getJSONObject(i).getString("url")
                        .toString());
                // printing the values to the logcat
                // Log.v(videosArray.getJSONObject(i).getString("onclick").toString());

                youtubeVideo.setBookmark(videosArray.getJSONObject(i)
                        .getBoolean("is_bookmark"));
                youtubeVideo.setLike(videosArray.getJSONObject(i).getBoolean(
                        "is_like"));
                youtubeVideo.setDislike(videosArray.getJSONObject(i)
                        .getBoolean("is_dislike"));
                youtubeVideo.setReport(videosArray.getJSONObject(i).getBoolean(
                        "is_report"));

                youtubeVideos.add(youtubeVideo);
            }
        }

        return youtubeVideos;
    }

    @Override
    public boolean addNewViewToVideo(String idVoizee) throws JSONException {

        // // Creating JSON Parser instance
        JSONParser jParser = new JSONParser();
        //
        // // getting JSON string from URL
        JSONObject json = jParser
                .getJSONFromUrl(VoizeeConstants.URL_HOST_SERVER_VOIZEE
                        + VoizeeConstants.URL_API_SERVER_VOIZEE
                        + "anyadir_view?id_video=" + idVoizee);

        if (json != null) {

            boolean status = json.getBoolean("status");

            if (status)
                return true;
        } else
            return false;
        return false;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.waiyaki.voizee.dao.VoizeeApiDao#bookmarkVideo(java.lang.String,
     * java.lang.String, java.lang.String)
     */
    @Override
    public String userOperationVideoPlayer(String operation, String idUser,
                                           String idVideo, String token) throws JSONException {

        String urlServer = VoizeeConstants.URL_HOST_SERVER_VOIZEE
                + VoizeeConstants.URL_API_SERVER_VOIZEE + operation;

        Map<String, String> mapValues = new HashMap<String, String>();
        mapValues.put("id_usuario", idUser);
        mapValues.put("id_video", idVideo);
        mapValues.put("token", token);

        JSONObject json;
        try {
            json = JSONParser.getJSONFromPost("", null, mapValues, urlServer);
            if (json.getString("status").equals("true"))
                return "true";
            else
                return json.getString("status");
        } catch (IOException e) {
            e.printStackTrace();
            return "false";
        } catch (JSONException e) {
            e.printStackTrace();
            return "false";
        }

    }

}
