/**
 * 
 */
package com.waiyaki.voizee.dao.model;

import java.io.Serializable;

/**
 * @author Francisco Javier Morant Moya
 * 
 */
public class VoizeeVideo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String urlThumbnail;
	private String urlVideo;
	private String urlVideoHQ;
	private String urlAudio;
	private String urlLink;
	private String descripcion;
	private String titulo;
	private String id;
	private String autor;
	private String categoria;
	private String idUsuario;
	private String idRecording;
	private String views;
	private String urlOnda;
	private int volume;
	private boolean isLike;
	private boolean isDislike;
	private boolean isBookmark;
	private boolean isReport;

	public String getUrlThumbnail() {
		return urlThumbnail;
	}

	public void setUrlThumbnail(String urlThumbnail) {
		this.urlThumbnail = urlThumbnail;
	}

	public String getUrlVideo() {
		return urlVideo;
	}

	public void setUrlVideo(String urlVideo) {
		this.urlVideo = urlVideo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getUrlAudio() {
		return urlAudio;
	}

	public void setUrlAudio(String urlAudio) {
		this.urlAudio = urlAudio;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getIdRecording() {
		return idRecording;
	}

	public void setIdRecording(String idRecording) {
		this.idRecording = idRecording;
	}

	public boolean isLike() {
		return isLike;
	}

	public void setLike(boolean isLike) {
		this.isLike = isLike;
	}

	public boolean isDislike() {
		return isDislike;
	}

	public void setDislike(boolean isDislike) {
		this.isDislike = isDislike;
	}

	public boolean isBookmark() {
		return isBookmark;
	}

	public void setBookmark(boolean isBookmark) {
		this.isBookmark = isBookmark;
	}

	public boolean isReport() {
		return isReport;
	}

	public void setReport(boolean isReport) {
		this.isReport = isReport;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getUrlLink() {
		return urlLink;
	}

	public void setUrlLink(String urlLink) {
		this.urlLink = urlLink;
	}

	public String getViews() {
		return views;
	}

	public void setViews(String views) {
		this.views = views;
	}

	public String getUrlVideoHQ() {
		return urlVideoHQ;
	}

	public void setUrlVideoHQ(String urlVideoHQ) {
		this.urlVideoHQ = urlVideoHQ;
	}

	public String getUrlOnda() {
		return urlOnda;
	}

	public void setUrlOnda(String urlOnda) {
		this.urlOnda = urlOnda;
	}

}
