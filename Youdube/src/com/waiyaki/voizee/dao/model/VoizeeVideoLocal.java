/**
 * 
 */
package com.waiyaki.voizee.dao.model;

import java.io.Serializable;

/**
 * @author Francisco Javier Morant Moya
 * 
 */
public class VoizeeVideoLocal implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6250018470928017326L;
	String id;
	String urlYoudube;
	String name;
	String autor;
	String fecha;
	String urlThumbnail;
	String urlVideo;
	String audioPath;
	private int volume = 0;

	public String getUrlVideo() {
		return urlVideo;
	}

	public void setUrlVideo(String urlRstp) {
		this.urlVideo = urlRstp;
	}

	public VoizeeVideoLocal(String id, String url, String name, String autor,
			String fecha, String urlThumbnail, String urlRstp, String audio,
			int volume) {
		this.id = id;
		this.urlYoudube = url;
		this.name = name;
		this.autor = autor;
		this.fecha = fecha;
		this.urlThumbnail = urlThumbnail;
		this.urlVideo = urlRstp;
		this.audioPath = audio;
		this.volume = volume;
	}

	public String getUrlYoudube() {
		return urlYoudube;
	}

	public void setUrlYoudube(String url) {
		this.urlYoudube = url;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUrlThumbnail() {
		return urlThumbnail;
	}

	public void setUrlThumbnail(String urlThumbnail) {
		this.urlThumbnail = urlThumbnail;
	}

	public String getAudio() {
		return audioPath;
	}

	public void setAudio(String audio) {
		this.audioPath = audio;
	}

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

}
