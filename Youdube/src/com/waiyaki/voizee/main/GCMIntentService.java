/**
 * 
 */
package com.waiyaki.voizee.main;

import android.content.Context;
import android.content.Intent;

import com.google.android.gcm.GCMBaseIntentService;

/**
 * @author Francisco Javier Morant Moya
 * 
 */
public class GCMIntentService extends GCMBaseIntentService {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.android.gcm.GCMBaseIntentService#onError(android.content.Context
	 * , java.lang.String)
	 */
	@Override
	protected void onError(Context arg0, String arg1) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.android.gcm.GCMBaseIntentService#onMessage(android.content
	 * .Context, android.content.Intent)
	 */
	@Override
	protected void onMessage(Context arg0, Intent arg1) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.android.gcm.GCMBaseIntentService#onRegistered(android.content
	 * .Context, java.lang.String)
	 */
	@Override
	protected void onRegistered(Context arg0, String arg1) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.android.gcm.GCMBaseIntentService#onUnregistered(android.content
	 * .Context, java.lang.String)
	 */
	@Override
	protected void onUnregistered(Context arg0, String arg1) {
		// TODO Auto-generated method stub

	}

}
