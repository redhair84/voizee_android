package com.waiyaki.voizee.main;


import android.app.Application;
import android.content.SharedPreferences;
import android.util.Log;
import com.waiyaki.voizee.dao.model.VoizeeVideo;
import com.waiyaki.voizee.manager.LoginManager;
import com.waiyaki.voizee.manager.LoginManagerImpl;
import com.waiyaki.voizee.manager.VoizeeApiManager;
import com.waiyaki.voizee.manager.VoizeeApiManagerImpl;
import com.waiyaki.voizee.util.StatusLogin;
import com.waiyaki.voizee.util.VoizeeConstants;

import java.util.Locale;

public class VoizeeApplication extends Application {

	private StatusLogin statusLogin = new StatusLogin();

	private String idUser;
	private String tokenSession;
	private String languageUser;
	private boolean isDisconnected = false;
	private boolean isOpeningFacebook = false;
    private boolean isShownInitialDialog = true;
	//private static List<Fragment> fragments;

	private LoginManager managerLogin = new LoginManagerImpl();
	private VoizeeApiManager managerVideoPlayer = new VoizeeApiManagerImpl();
	private VoizeeVideo selectedVideo;

	public VoizeeApplication() {

	}

	@Override
	public void onCreate() {
		super.onCreate();

		// Set the status login at the beginning of the app
		statusLogin.setCurrentStatus(StatusLogin.STATUS_LOGIN_NONE);

		// Set the id of the user
		idUser = "";

		// Set the language of the user
		// Get the country Iso where you are
		// TelephonyManager tm = (TelephonyManager)
		// getSystemService(TELEPHONY_SERVICE);

		// LocationManager lm = (LocationManager)
		// getSystemService(LOCATION_SERVICE);

		languageUser = Locale.getDefault().getLanguage();

		if (VoizeeConstants.DEVELOPER_MODE) {
			Log.d("Voizee", "Language of the user: " + languageUser);

            //Change the url of voizee to have something related to test
			VoizeeConstants.URL_HOST_SERVER_VOIZEE = VoizeeConstants.URL_HOST_SERVER_VOIZEE_TEST;
		}


        //Set the version number
        SharedPreferences settings = getSharedPreferences(VoizeeConstants.PREFS_IS_SHOWN_INITIAL_MESSAGE, 0);

        String versionNumberSaved = settings.getString(VoizeeConstants.PREFS_VERSION_NUMBER,"");

        if(!versionNumberSaved.equals(VoizeeConstants.VERSION_NUMBER)){
            if (VoizeeConstants.DEVELOPER_MODE) {
                Log.d("Voizee", "Se habilita el mensaje inicial porque es una nueva version.");
                Log.d("Voizee", "versionNumberSaved : "+versionNumberSaved+" VoizeeConstants.VERSION_NUMBER : "+VoizeeConstants.VERSION_NUMBER);
            }

            isShownInitialDialog=false;

            SharedPreferences.Editor editor = settings.edit();
            editor.putString(VoizeeConstants.PREFS_VERSION_NUMBER,VoizeeConstants.VERSION_NUMBER);

            // Commit the edits!
            editor.commit();
        }else{
            isShownInitialDialog=true;
        }

	}

	public void logOutVoizee() {
		// Set null to idUser
		setIdUser(null);

		// Reset token
		setTokenSession(null);
		getStatusLogin().setCurrentStatus(StatusLogin.STATUS_LOGIN_NONE);
	}

	@Override
	public void onTerminate() {
		super.onTerminate();
	}

	public StatusLogin getStatusLogin() {
		return statusLogin;
	}

	public void setStatusLogin(StatusLogin statusLogin) {
		this.statusLogin = statusLogin;
	}

	public String getIdUser() {
		return idUser;
	}

	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}

	public LoginManager getManagerLogin() {
		return managerLogin;
	}

	public void setManagerLogin(LoginManager managerLogin) {
		this.managerLogin = managerLogin;
	}

	public VoizeeApiManager getManagerVideoPlayer() {
		return managerVideoPlayer;
	}

	public void setManagerVideoPlayer(VoizeeApiManager managerVideoPlayer) {
		this.managerVideoPlayer = managerVideoPlayer;
	}

	public String getTokenSession() {
		return tokenSession;
	}

	public void setTokenSession(String tokenSession) {
		this.tokenSession = tokenSession;
	}

	public String getLanguageUser() {
		return languageUser;
	}

	public void setLanguageUser(String languageUser) {
		this.languageUser = languageUser;
	}

	public boolean isDisconnected() {
		return isDisconnected;
	}

	public void setDisconnected(boolean isDisconnected) {
		this.isDisconnected = isDisconnected;
	}

	public VoizeeVideo getSelectedVideo() {
		return selectedVideo;
	}

	public void setSelectedVideo(VoizeeVideo selectedVideo) {
		this.selectedVideo = selectedVideo;
	}

//	public static List<Fragment> getFragments() {
//		return fragments;
//	}
//
//	public static void setFragments(List<Fragment> fragments) {
//		VoizeeApplication.fragments = fragments;
//	}

	public boolean isOpeningFacebook() {
		return isOpeningFacebook;
	}

	public void setOpeningFacebook(boolean isOpeningFacebook) {
		this.isOpeningFacebook = isOpeningFacebook;
	}

    public boolean isShownInitialDialog() {
        return isShownInitialDialog;
    }

    public void setShownInitialDialog(boolean shownInitialDialog) {
        isShownInitialDialog = shownInitialDialog;
    }

}
