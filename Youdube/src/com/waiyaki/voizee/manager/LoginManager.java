/**
 * 
 */
package com.waiyaki.voizee.manager;

import java.util.List;

import org.json.JSONException;

import com.waiyaki.voizee.dao.model.BookMark;
import com.waiyaki.voizee.dao.model.ResultLogin;

/**
 * @author Francisco Javier Morant Moya
 * 
 */
public interface LoginManager {
	public ResultLogin loginFB(String correo, String username,
			String id_facebook, String nombre) throws JSONException;

	public ResultLogin login(String usuario, String password)
			throws JSONException;

	public ResultLogin register(String correo, String usuario, String password)
			throws JSONException;

	public List<BookMark> getBookMarks(String id_usuario);
}
