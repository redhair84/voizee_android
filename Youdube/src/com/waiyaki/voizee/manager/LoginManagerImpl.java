/**
 * 
 */
package com.waiyaki.voizee.manager;

import java.util.List;

import org.json.JSONException;

import com.waiyaki.voizee.dao.LoginDao;
import com.waiyaki.voizee.dao.LoginDaoImpl;
import com.waiyaki.voizee.dao.model.BookMark;
import com.waiyaki.voizee.dao.model.ResultLogin;

/**
 * @author Francisco Javier Morant Moya
 * 
 */
public class LoginManagerImpl implements LoginManager {

	LoginDao loginDao = new LoginDaoImpl();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.waiyaki.videolate.manager.LoginManager#loginFB(java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public ResultLogin loginFB(String correo, String username,
			String id_facebook, String nombre) throws JSONException {
		// TODO Auto-generated method stub
		return loginDao.loginFB(correo, username, id_facebook, nombre);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.waiyaki.videolate.manager.LoginManager#login(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public ResultLogin login(String usuario, String password)
			throws JSONException {
		return loginDao.loginVoizee(usuario, password);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.waiyaki.videolate.manager.LoginManager#register(java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public ResultLogin register(String correo, String usuario, String password)
			throws JSONException {
		return loginDao.register(correo, usuario, password);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.waiyaki.videolate.manager.LoginManager#getBookMarks(java.lang.String)
	 */
	@Override
	public List<BookMark> getBookMarks(String id_usuario) {
		// TODO Auto-generated method stub
		return loginDao.getBookMarks(id_usuario);
	}

}
