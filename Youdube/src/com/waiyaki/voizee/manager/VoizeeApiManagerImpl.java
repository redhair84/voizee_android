/**
 * 
 */
package com.waiyaki.voizee.manager;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import org.json.JSONException;

import com.google.gdata.util.ServiceException;
import com.waiyaki.voizee.dao.VoizeeApiDao;
import com.waiyaki.voizee.dao.VoizeeApiDaoImpl;
import com.waiyaki.voizee.dao.model.VoizeeVideo;

/**
 * @author Francisco Javier Morant Moya
 * 
 */
public class VoizeeApiManagerImpl implements VoizeeApiManager {

	private VoizeeApiDao videoPlayerDao = new VoizeeApiDaoImpl();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.waiyaki.videolate.manager.VideoPlayerManager#getVideos(java.lang.
	 * String)
	 */
	@Override
	public List<VoizeeVideo> searchYoutubeVideos(String query, String language)
			throws MalformedURLException {
		// TODO Auto-generated method stub
		return videoPlayerDao.searchYoutubeVideos(query, language);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.waiyaki.videolate.manager.VideoPlayerManager#getVideo(java.lang.String
	 * )
	 */
	@Override
	public VoizeeVideo getVideo(String id) throws MalformedURLException,
			IOException, ServiceException {
		// TODO Auto-generated method stub
		return videoPlayerDao.getVideo(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.waiyaki.videolate.manager.VideoPlayerManager#uploadVideo(java.lang
	 * .String)
	 */
	@Override
	public String uploadVideo(String path, VoizeeVideo youtubeVideo,
			String token,boolean isPrivate) {
		return videoPlayerDao.uploadVideo(path, youtubeVideo, token,isPrivate);
	}

    public VoizeeVideo getVoizeeVideo(String idVoizee) throws JSONException{
        return videoPlayerDao.getVoizeeVideo(idVoizee);
    }


	/*
	 * (non-Javadoc)
	 * 
	 * @see com.waiyaki.videolate.manager.VideoPlayerManager#getPopularsVideos()
	 */
	@Override
	public List<VoizeeVideo> getPopularsVideos(String idUser)
			throws JSONException {

		return videoPlayerDao.getPopularVideos(idUser);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.waiyaki.videolate.manager.VideoPlayerManager#getSuggestedVideo()
	 */
	// @Override
	// public List<YouTubeVideoRequest> getSuggestedVideos() throws
	// JSONException,
	// MalformedURLException, IOException, ServiceException {
	// // TODO Auto-generated method stub
	// return videoPlayerDao.getSuggestedVideos();
	// }

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.waiyaki.videolate.manager.VideoPlayerManager#getLastVideos()
	 */
	@Override
	public List<VoizeeVideo> getLastVideos(String idUser) throws JSONException {
		// TODO Auto-generated method stub
		return videoPlayerDao.getLastVideos(idUser);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.waiyaki.videolate.manager.VideoPlayerManager#getMostDubbedVideos()
	 */
	@Override
	public List<VoizeeVideo> getMainMostDubbedVideos() throws JSONException {
		return videoPlayerDao.getMainMostDubbedVideos();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.waiyaki.videolate.manager.VideoPlayerManager#getMostDubbedVideos()
	 */
	@Override
	public List<VoizeeVideo> getDetailMostDubbedVideos(String youtubeId,
			String idUser) throws JSONException {
		return videoPlayerDao.getDetailMostDubbedVideos(youtubeId, idUser);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.waiyaki.videolate.manager.VideoPlayerManager#getMostViewedVideos()
	 */
	@Override
	public List<VoizeeVideo> getMostViewedVideos(String idUser)
			throws JSONException {
		return videoPlayerDao.getMostViewedVideos(idUser);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.waiyaki.voizee.manager.VideoPlayerManager#getMyVideos(java.lang.String
	 * )
	 */
	@Override
	public List<VoizeeVideo> getMyVideos(String idUser) throws JSONException {
		// TODO Auto-generated method stub
		return videoPlayerDao.getMyVideos(idUser);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.waiyaki.voizee.manager.VideoPlayerManager#getMyFavouritesVideos(java
	 * .lang.String)
	 */
	@Override
	public List<VoizeeVideo> getMyFavouritesVideos(String idUser)
			throws JSONException {
		// TODO Auto-generated method stub
		return videoPlayerDao.getMyFavouritesVideos(idUser);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.waiyaki.voizee.manager.VideoPlayerManager#getSearchedVideos()
	 */
	@Override
	public List<VoizeeVideo> getSearchedVideos(String searchValueEntered,
			String idUser) throws JSONException {
		// TODO Auto-generated method stub
		return videoPlayerDao.getSearchedVideos(searchValueEntered, idUser);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.waiyaki.voizee.manager.VideoPlayerManager#addNewViewToVideo(java.
	 * lang.String)
	 */
	@Override
	public boolean addNewViewToVideo(String idVoizee) throws JSONException {
		return videoPlayerDao.addNewViewToVideo(idVoizee);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.waiyaki.voizee.manager.VoizeeApiManager#userOperationVideoPlayer(
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public String userOperationVideoPlayer(String operation, String idUser,
			String idVideo, String token) throws JSONException {
		// TODO Auto-generated method stub
		return videoPlayerDao.userOperationVideoPlayer(operation, idUser,
				idVideo, token);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.waiyaki.videolate.manager.VideoPlayerManager#getVideoYoutube(java
	 * .lang.String)
	 */
	// @Override
	// public YouTubeVideoRequest getVideoYoutube(String videoYoutubeId)
	// throws MalformedURLException, IOException, ServiceException,
	// JSONException {
	// return videoPlayerDao.getVideoYoutube(videoYoutubeId);
	// }

}
