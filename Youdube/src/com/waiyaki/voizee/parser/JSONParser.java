/**
 *
 */
package com.waiyaki.voizee.parser;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.waiyaki.voizee.util.VoizeeConstants;

public class JSONParser {

    static InputStream is = null;
    static JSONObject jObj = null;
    static String json = "";

    // constructor
    public JSONParser() {

    }

    public JSONObject getJSONFromUrl(String url) {

        boolean thereIsError = false;

        // Making HTTP request
        try {
            HttpClient client = new DefaultHttpClient();
            HttpGet request = new HttpGet();

            request.setURI(new URI(url));

            HttpResponse response = client.execute(request);
            HttpEntity httpEntity = response.getEntity();
            is = httpEntity.getContent();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            if (VoizeeConstants.DEVELOPER_MODE)
                Log.e("Voizee", "Error getting response " + e.getMessage());
            thereIsError = true;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            if (VoizeeConstants.DEVELOPER_MODE)
                Log.e("Voizee", "Error getting response " + e.getMessage());
            thereIsError = true;
        } catch (IOException e) {
            e.printStackTrace();
            if (VoizeeConstants.DEVELOPER_MODE)
                Log.e("Voizee", "Error getting response " + e.getMessage());
            thereIsError = true;
        } catch (Exception e) {
            e.printStackTrace();
            if (VoizeeConstants.DEVELOPER_MODE)
                Log.e("Voizee", "Error getting response " + e.getMessage());
            thereIsError = true;
        }

        if (!thereIsError) {

            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                json = sb.toString();

                if (VoizeeConstants.DEVELOPER_MODE)
                    Log.d("Voizee", json);
            } catch (Exception e) {
                if (VoizeeConstants.DEVELOPER_MODE)
                    Log.e("Voizee", "Error converting result " + e.toString());
            }

            // try parse the string to a JSON object
            try {
                jObj = new JSONObject(json);
            } catch (JSONException e) {
                Log.e("Voizee", "Error parsing data " + e.toString());
            }
        } else {
            return null;
        }

        // return JSON String
        return jObj;

    }

    public JSONObject getJSONFromString(String string) {

        // try parse the string to a JSON object
        try {
            jObj = new JSONObject(string);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        // return JSON String
        return jObj;

    }

    public static JSONObject getJSONFromPost(String paramFile, String pathFile,
                                             Map<String, String> paramValues, String urlPost) throws IOException {
        FileInputStream fileInputStream = null;
        HttpURLConnection connection = null;
        DataOutputStream outputStream = null;

        String pathToOurFile = pathFile;
        String urlServer = urlPost;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";

        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;

        URL url = new URL(urlServer);
        connection = (HttpURLConnection) url.openConnection();

        // Allow Inputs & Outputs
        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setUseCaches(false);

        // Enable POST method
        connection.setRequestMethod("POST");

        connection.setRequestProperty("Connection", "Keep-Alive");
        connection.setRequestProperty("Content-Type",
                "multipart/form-data;boundary=" + boundary);

        outputStream = new DataOutputStream(connection.getOutputStream());

        // //////////POST filename ///////////////////
        // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        if (pathToOurFile != null) {

            fileInputStream = new FileInputStream(new File(pathToOurFile));

            // twoHyphens
            outputStream.writeBytes(twoHyphens + boundary + lineEnd);
            outputStream.writeBytes("Content-Disposition: form-data; name=\""
                    + paramFile + "\";filename=\"" + pathToOurFile + "\""
                    + lineEnd);
            // lineEnd
            outputStream.writeBytes(lineEnd);

            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            while (bytesRead > 0) {
                outputStream.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }

            // lineEnd
            outputStream.writeBytes(lineEnd);

        }

        if (paramValues != null) {
            // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            Set<String> setParams = paramValues.keySet();

            Iterator<String> iter = setParams.iterator();

            while (iter.hasNext()) {
                String keyParam = iter.next();

                if (paramValues.get(keyParam) != null) {
                    // twoHyphens
                    outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                    outputStream
                            .writeBytes("Content-Disposition: form-data; name="
                                    + keyParam + lineEnd);

                    outputStream.writeBytes(lineEnd);
                    outputStream.writeBytes(paramValues.get(keyParam));

                    // linesEnd
                    outputStream.writeBytes(lineEnd);
                }
            }
        }

        // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // Responses from the server (code and message)
        String responsePHP = IOUtils.toString(connection.getInputStream());

        //if (VoizeeConstants.DEVELOPER_MODE)
            Log.d("Voizee", responsePHP);

        if (pathToOurFile != null) {
            // Closes the connection
            fileInputStream.close();
            outputStream.flush();
            outputStream.close();
        }

        // Creating JSON Parser instance
        JSONParser jParser = new JSONParser();

        // getting JSON string from URL
        JSONObject json = jParser.getJSONFromString(responsePHP);
        return json;

    }
}
