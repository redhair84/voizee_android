/**
 * 
 */
package com.waiyaki.voizee.util;

import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;

import com.waiyaki.voizee.activity.ui.UploadDialogActivity;

/**
 * @author Francisco Javier Morant Moya
 * 
 */
public class CategoryOnItemSelectedListener implements OnItemSelectedListener {

	/*
	 * provide local instances of the mLocalAdapter and the mLocalContext
	 */

	ArrayAdapter<CharSequence> mLocalAdapter;
	Activity mLocalContext;
	int count = 0;

	/**
	 * Constructor
	 * 
	 * @param c
	 *            - The activity that displays the Spinner.
	 * @param ad
	 *            - The Adapter view that controls the Spinner. Instantiate a
	 *            new listener object.
	 */
	public CategoryOnItemSelectedListener(Activity c,
			ArrayAdapter<CharSequence> ad) {

		this.mLocalContext = c;
		this.mLocalAdapter = ad;

	}

	/**
	 * When the user selects an item in the spinner, this method is invoked by
	 * the callback chain. Android calls the item selected listener for the
	 * spinner, which invokes the onItemSelected method.
	 * 
	 * @see android.widget.AdapterView.OnItemSelectedListener#onItemSelected(android.widget.AdapterView,
	 *      android.view.View, int, long)
	 * @param parent
	 *            - the AdapterView for this listener
	 * @param v
	 *            - the View for this listener
	 * @param pos
	 *            - the 0-based position of the selection in the mLocalAdapter
	 * @param row
	 *            - the 0-based row number of the selection in the View
	 */

	@Override
	public void onItemSelected(AdapterView<?> parent, View v, int pos, long row) {

		if (((UploadDialogActivity) this.mLocalContext).getSpinnerCategory()
				.equals(parent)) {
			((UploadDialogActivity) this.mLocalContext).setmPosCategory(pos);
			((UploadDialogActivity) this.mLocalContext)
					.setmSelectionCategory(parent.getItemAtPosition(pos)
							.toString());

		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.AdapterView.OnItemSelectedListener#onNothingSelected
	 * (android.widget.AdapterView)
	 */
	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}

}
