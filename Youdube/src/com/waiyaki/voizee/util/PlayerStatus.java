/**
 * 
 */
package com.waiyaki.voizee.util;

/**
 * @author Francisco Javier Morant Moya
 * 
 */
public class PlayerStatus {

	public static final int STATUS_STOPPED = 0;
	public static final int STATUS_PAUSED = 1;
	public static final int STATUS_PLAYING = 3;
	public static final int STATUS_RECORDING = 4;

	private int currentStatus = 0;

	public int getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(int currentStatus) {
		this.currentStatus = currentStatus;
	}
}
