/**
 * 
 */
package com.waiyaki.voizee.util;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.VideoView;

/**
 * @author Francisco Javier Morant Moya
 * 
 */
public class PlayerVideoView extends VideoView {

	/**
	 * @param context
	 */
	public PlayerVideoView(Context context) {
		super(context);
	}

	public PlayerVideoView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public PlayerVideoView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	// @Override
	// protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
	// int width = 713;
	// int height = 440;
	//
	// setMeasuredDimension(width, height);
	// }
}
