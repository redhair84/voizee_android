/**
 * 
 */
package com.waiyaki.voizee.util;

/**
 * @author Francisco Javier Morant Moya
 * 
 */
public class StatusLogin {
	public static final int STATUS_LOGIN_NONE = 0;
	public static final int STATUS_LOGIN_FB = 1;
	public static final int STATUS_LOGIN_VZ = 2;

	private int currentStatus = 0;

	public int getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(int currentStatus) {
		this.currentStatus = currentStatus;
	}
}
