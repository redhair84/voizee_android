/**
 * 
 */
package com.waiyaki.voizee.util;

/**
 * @author Francisco Javier Morant Moya
 * 
 */
public class VoizeeConstants {

	//public static String URL_YOUTUBE_JSON = "https://gdata.youtube.com/feeds/api/videos?";

	public static final int POS_CATEGORY_RISA = 1;
	public static final int POS_CATEGORY_TRADUCCION = 2;
	public static final int POS_CATEGORY_COMENTARIOS = 3;
	public static final int POS_CATEGORY_DOBLAJE = 4;
	public static final int POS_CATEGORY_MUSICA = 5;
	public static final int POS_CATEGORY_OTROS = 9;

	public static final boolean DEVELOPER_MODE = false;

	public static String URL_HOST_SERVER_VOIZEE = "http://voizee.tv";
	public static final String URL_API_SERVER_VOIZEE = "/api_V1.1/";
	public static final String URL_HOST_SERVER_VOIZEE_TEST = "http://test.voizee.tv";

	public static final String URL_VIDEO_YOUTUBE = "http://www.youtube.com/watch?v=";

	public static final int START_ACTIVITY_LOGIN = 0;
	public static final int START_SUGGESTED_VIDEO_RECORDED = 1;
	public static final int START_SHARE_VIDEO_UPLOADED = 2;

	public static final String VIDEO_VALUE_REQUESTED = "videoBD";


    public static final String PREFS_IS_SHOWN_INITIAL_MESSAGE = "prefInitialMessage";
    public static final String PREFS_VERSION_NUMBER = "versionNumber";
    public static final String VERSION_NUMBER = "1.1.3";

}
