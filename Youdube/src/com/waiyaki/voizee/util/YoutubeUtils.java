/**
 * 
 */
package com.waiyaki.voizee.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;

/**
 * @author Francisco Javier Morant Moya
 * 
 */
public class YoutubeUtils {

	public static Bitmap getBitmapFromURL(String src) {
		try {
			URL url = new URL(src);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setDoInput(true);
			connection.connect();
			InputStream input = connection.getInputStream();
			Bitmap myBitmap = BitmapFactory.decodeStream(input);
			return myBitmap;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Drawable LoadImageFromWebOperations(String url) {
		Drawable d = null;
		InputStream is;
		try {
			is = (InputStream) new URL(url).getContent();
			d = Drawable.createFromStream(is, "src name");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return d;
	}

	public static boolean isOnline(Activity executedActivity) {
		ConnectivityManager cm = (ConnectivityManager) executedActivity
				.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(cm==null)
            return false;

		if (cm!=null && cm.getActiveNetworkInfo() == null)
			return false;

		return cm.getActiveNetworkInfo().isConnectedOrConnecting();

	}

}
